package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Store Screen</h1>
 * Transactions will be processed here
 * which required a customer account to
 * be linked and movies added to the queue
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.Color;
import java.awt.event.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Store extends javax.swing.JFrame
{

    //Creates new form Store
    public Store()
    {
        initComponents();
        setSystemInformation();
        setStaffInformation();
        storeBinding();
        
        tblQueue.setModel(dtm);
        
        if (f.getStaffFullName().equals("Admin"))
        {
            btnAddMovie.setEnabled(false);
            btnMRemove.setEnabled(false);
            btnLinkAccount.setEnabled(false);
            btnUnlinkAccount.setEnabled(false);
            btnProcessRental.setEnabled(false);
        }
        else mnuAdmin.setVisible(false);
        
        lblNoMovies.setText(items + "");
        lblPrice.setText(price + "");
    }
    
    int items = 0;
    double price = 0.0;
    
    Functions f = new Functions();
    
    Connection c = Functions.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    /**
     * Method used to set the date and time
     * from the system clock and runs
     * a timer to update the time
     */
    final void setSystemInformation()
    {
        new Timer(1, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy"),
                                       sdfTime = new SimpleDateFormat("HH:mm:ss");
                Calendar now = Calendar.getInstance();
                lblDate.setText(" " + sdfDate.format(now.getTime()));
                lblTime.setText(" " + sdfTime.format(now.getTime()));
            }
        }).start();
    }
    
    /**
     * This method gets the staff information
     * from the functions class and sets
     * the relevant labels to show
     * which staff member is logged in
     */
    final void setStaffInformation()
    {
        lblStaffFullName.setText(" " + f.getStaffFullName());
        lblStaffID.setText(" " + f.getStaffID());
    }
    
    /**
     * This method retrieves the relevant
     * information from the database
     * [the movie ID and title
     *  for cmbMovies and
     *  the customer ID and email
     *  for cmbCustomers]
     * and binds the UI with
     * the acquired information
     */
    private void storeBinding()
    {
        //Clears the combo boxes
        cmbMovies.removeAllItems();
        cmbCustomers.removeAllItems();
        
        try
        {
            ps = c.prepareStatement
            (
                "SELECT MOVIE_ID, TITLE " +
                "FROM movies " +
                "WHERE NO_OF_COPIES > 0"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                cmbMovies.addItem(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
            
            ps = c.prepareStatement
            (
                "SELECT CUSTOMER_ID, EMAIL " +
                "FROM customers"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                cmbCustomers.addItem(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); } 
    }

    /**
     * This method is used clear
     * the fields related to a
     * customer which is called
     * by the unlink button
     * and the clear method.
     * This method also recalculates
     * the price when it is called
     */
    private void unlinkAccount()
    {
        lblAccount.setText("");
        lblRName.setText("");
        lblRSurname.setText("");
        lblRDOB.setText("");
        lblRNumber.setText("");
        lblRCreation.setText("");
        lblRLevel.setText("");
        lblRDiscount.setText("");
        lblRRented.setText("");
        
        cmbCustomers.setEnabled(true);
        btnLinkAccount.setEnabled(true);
        
        price = 0.0;
        
        for (int i = 0; i < dtm.getRowCount(); i++) price += (float) tblQueue.getValueAt(i, 2);
        
        price  = Math.round(price * 100);
        price /= 100;
        
        lblPrice.setText(price + "");
    }
    
    //Refreshes the UI
    private void clear()
    {
        while (dtm.getRowCount() > 0) { dtm.removeRow(0); }

        items = 0;
        price = 0.0;

        lblNoMovies.setText(items + "");
        lblPrice.setText(price + "");

        unlinkAccount();
    }
    
    //Column names for tblQueue
    private Object headers[] =
    {
        "MOVIE ID",
        "TITLE",
        "PRICE"
    };
    
    /**
     * Creates a model for tblQueue
     * that overrides the ability
     * to edit cells of the table
     */
    DefaultTableModel dtm = new DefaultTableModel(headers, 0)
    {
        @Override
        public boolean isCellEditable(int row, int column)
        {
            return false;
        }
    };
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cmbMovies = new javax.swing.JComboBox<>();
        btnAddMovie = new javax.swing.JButton();
        btnMRemove = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblQueue = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cmbCustomers = new javax.swing.JComboBox<>();
        btnLinkAccount = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblRRented = new javax.swing.JLabel();
        lblRName = new javax.swing.JLabel();
        lblRSurname = new javax.swing.JLabel();
        lblRDOB = new javax.swing.JLabel();
        lblRNumber = new javax.swing.JLabel();
        lblRCreation = new javax.swing.JLabel();
        lblRLevel = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        lblRDiscount = new javax.swing.JLabel();
        btnUnlinkAccount = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        btnProcessRental = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblNoMovies = new javax.swing.JLabel();
        lblPrice = new javax.swing.JLabel();
        lblAccount = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        lblStaffID = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblStaffFullName = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnuItemExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnuItemCustomerSignUp = new javax.swing.JMenuItem();
        mnuItemCustomerCRUD = new javax.swing.JMenuItem();
        mnuItemReturns = new javax.swing.JMenuItem();
        mnuAdmin = new javax.swing.JMenu();
        mnuLogOut = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Movie Shack");
        setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage());
        setMinimumSize(new java.awt.Dimension(1280, 960));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(1280, 960));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("STORE");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(570, 25, 138, 52);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Movie", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel2.setLayout(null);

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Add Title to Queue:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(40, 40, 130, 16);

        cmbMovies.setBackground(new java.awt.Color(255, 255, 255));
        cmbMovies.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(cmbMovies);
        cmbMovies.setBounds(40, 60, 360, 26);

        btnAddMovie.setBackground(new java.awt.Color(255, 255, 255));
        btnAddMovie.setForeground(new java.awt.Color(0, 0, 0));
        btnAddMovie.setText("ADD MOVIE");
        btnAddMovie.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddMovieMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddMovieMouseExited(evt);
            }
        });
        btnAddMovie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMovieActionPerformed(evt);
            }
        });
        jPanel2.add(btnAddMovie);
        btnAddMovie.setBounds(410, 56, 160, 32);

        btnMRemove.setBackground(new java.awt.Color(255, 255, 255));
        btnMRemove.setForeground(new java.awt.Color(0, 0, 0));
        btnMRemove.setText("REMOVE MOVIE");
        btnMRemove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMRemoveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMRemoveMouseExited(evt);
            }
        });
        btnMRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMRemoveActionPerformed(evt);
            }
        });
        jPanel2.add(btnMRemove);
        btnMRemove.setBounds(225, 655, 160, 32);

        tblQueue.setBackground(new java.awt.Color(255, 255, 255));
        tblQueue.setForeground(new java.awt.Color(0, 0, 0));
        tblQueue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblQueue.setFillsViewportHeight(true);
        jScrollPane2.setViewportView(tblQueue);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(40, 100, 530, 533);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(20, 170, 610, 710);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Customer", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel3.setLayout(null);

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Customer Account:");
        jPanel3.add(jLabel3);
        jLabel3.setBounds(40, 40, 120, 16);

        cmbCustomers.setBackground(new java.awt.Color(255, 255, 255));
        cmbCustomers.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(cmbCustomers);
        cmbCustomers.setBounds(40, 60, 360, 26);

        btnLinkAccount.setBackground(new java.awt.Color(255, 255, 255));
        btnLinkAccount.setForeground(new java.awt.Color(0, 0, 0));
        btnLinkAccount.setText("LINK ACCOUNT");
        btnLinkAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLinkAccountMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLinkAccountMouseExited(evt);
            }
        });
        btnLinkAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLinkAccountActionPerformed(evt);
            }
        });
        jPanel3.add(btnLinkAccount);
        btnLinkAccount.setBounds(410, 56, 160, 32);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Customer Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel4.setLayout(null);

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Name:");
        jPanel4.add(jLabel9);
        jLabel9.setBounds(50, 50, 50, 16);

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Surname:");
        jPanel4.add(jLabel11);
        jLabel11.setBounds(50, 110, 70, 16);

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("Date of Birth:");
        jPanel4.add(jLabel12);
        jLabel12.setBounds(50, 170, 80, 16);

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Contact Number:");
        jPanel4.add(jLabel13);
        jLabel13.setBounds(50, 230, 110, 16);

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Account Creation Date:");
        jPanel4.add(jLabel14);
        jLabel14.setBounds(50, 290, 140, 16);

        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Membership Level:");
        jPanel4.add(jLabel15);
        jLabel15.setBounds(50, 350, 120, 16);

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Number of Movies Rented:");
        jPanel4.add(jLabel16);
        jLabel16.setBounds(50, 470, 160, 16);

        lblRRented.setForeground(new java.awt.Color(0, 0, 0));
        lblRRented.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRRented);
        lblRRented.setBounds(260, 469, 220, 18);

        lblRName.setForeground(new java.awt.Color(0, 0, 0));
        lblRName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRName);
        lblRName.setBounds(260, 49, 220, 18);

        lblRSurname.setForeground(new java.awt.Color(0, 0, 0));
        lblRSurname.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRSurname);
        lblRSurname.setBounds(260, 109, 220, 18);

        lblRDOB.setForeground(new java.awt.Color(0, 0, 0));
        lblRDOB.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRDOB);
        lblRDOB.setBounds(260, 169, 220, 18);

        lblRNumber.setForeground(new java.awt.Color(0, 0, 0));
        lblRNumber.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRNumber);
        lblRNumber.setBounds(260, 229, 220, 18);

        lblRCreation.setForeground(new java.awt.Color(0, 0, 0));
        lblRCreation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRCreation);
        lblRCreation.setBounds(260, 289, 220, 18);

        lblRLevel.setForeground(new java.awt.Color(0, 0, 0));
        lblRLevel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRLevel);
        lblRLevel.setBounds(260, 349, 220, 18);

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Discount Percentage:");
        jPanel4.add(jLabel18);
        jLabel18.setBounds(50, 410, 130, 16);

        lblRDiscount.setForeground(new java.awt.Color(0, 0, 0));
        lblRDiscount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblRDiscount);
        lblRDiscount.setBounds(260, 409, 220, 18);

        jPanel3.add(jPanel4);
        jPanel4.setBounds(40, 95, 530, 538);

        btnUnlinkAccount.setBackground(new java.awt.Color(255, 255, 255));
        btnUnlinkAccount.setForeground(new java.awt.Color(0, 0, 0));
        btnUnlinkAccount.setText("UNLINK ACCOUNT");
        btnUnlinkAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUnlinkAccountMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUnlinkAccountMouseExited(evt);
            }
        });
        btnUnlinkAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnlinkAccountActionPerformed(evt);
            }
        });
        jPanel3.add(btnUnlinkAccount);
        btnUnlinkAccount.setBounds(225, 655, 160, 32);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(650, 170, 610, 710);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Rental Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel5.setLayout(null);

        btnProcessRental.setBackground(new java.awt.Color(255, 255, 255));
        btnProcessRental.setForeground(new java.awt.Color(0, 0, 0));
        btnProcessRental.setText("PROCESS RENTAL");
        btnProcessRental.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnProcessRentalMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnProcessRentalMouseExited(evt);
            }
        });
        btnProcessRental.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessRentalActionPerformed(evt);
            }
        });
        jPanel5.add(btnProcessRental);
        btnProcessRental.setBounds(1040, 18, 180, 32);

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Number of Movies:");
        jPanel5.add(jLabel17);
        jLabel17.setBounds(20, 27, 110, 16);

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Total Price:");
        jPanel5.add(jLabel4);
        jLabel4.setBounds(290, 27, 70, 16);

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Customer Account:");
        jPanel5.add(jLabel5);
        jLabel5.setBounds(520, 27, 110, 16);

        lblNoMovies.setForeground(new java.awt.Color(0, 0, 0));
        lblNoMovies.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblNoMovies);
        lblNoMovies.setBounds(140, 26, 140, 18);

        lblPrice.setForeground(new java.awt.Color(0, 0, 0));
        lblPrice.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblPrice);
        lblPrice.setBounds(370, 26, 140, 18);

        lblAccount.setForeground(new java.awt.Color(0, 0, 0));
        lblAccount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblAccount);
        lblAccount.setBounds(640, 26, 390, 18);

        jPanel1.add(jPanel5);
        jPanel5.setBounds(20, 890, 1240, 60);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "System Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel6.setLayout(null);

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Date:");
        jPanel6.add(jLabel6);
        jLabel6.setBounds(20, 27, 30, 16);

        lblDate.setForeground(new java.awt.Color(0, 0, 0));
        lblDate.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblDate);
        lblDate.setBounds(60, 26, 240, 18);

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Time:");
        jPanel6.add(jLabel8);
        jLabel8.setBounds(310, 27, 40, 16);

        lblTime.setForeground(new java.awt.Color(0, 0, 0));
        lblTime.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblTime);
        lblTime.setBounds(350, 26, 240, 18);

        jPanel1.add(jPanel6);
        jPanel6.setBounds(20, 100, 610, 60);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Staff Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel7.setLayout(null);

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("Full Name:");
        jPanel7.add(jLabel10);
        jLabel10.setBounds(40, 27, 60, 16);

        lblStaffID.setForeground(new java.awt.Color(0, 0, 0));
        lblStaffID.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.add(lblStaffID);
        lblStaffID.setBounds(360, 26, 210, 18);

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("ID:");
        jPanel7.add(jLabel7);
        jLabel7.setBounds(335, 27, 20, 16);

        lblStaffFullName.setForeground(new java.awt.Color(0, 0, 0));
        lblStaffFullName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.add(lblStaffFullName);
        lblStaffFullName.setBounds(110, 26, 210, 18);

        jPanel1.add(jPanel7);
        jPanel7.setBounds(650, 100, 610, 60);

        jMenuBar1.setBackground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setForeground(new java.awt.Color(0, 0, 0));

        jMenu1.setBackground(new java.awt.Color(255, 255, 255));
        jMenu1.setForeground(new java.awt.Color(0, 0, 0));
        jMenu1.setText("File");

        mnuItemExit.setBackground(new java.awt.Color(255, 255, 255));
        mnuItemExit.setForeground(new java.awt.Color(0, 0, 0));
        mnuItemExit.setText("Exit");
        mnuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemExitActionPerformed(evt);
            }
        });
        jMenu1.add(mnuItemExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setBackground(new java.awt.Color(255, 255, 255));
        jMenu2.setForeground(new java.awt.Color(0, 0, 0));
        jMenu2.setText("Customers");

        mnuItemCustomerSignUp.setBackground(new java.awt.Color(255, 255, 255));
        mnuItemCustomerSignUp.setForeground(new java.awt.Color(0, 0, 0));
        mnuItemCustomerSignUp.setText("Create New Customer Account");
        mnuItemCustomerSignUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemCustomerSignUpActionPerformed(evt);
            }
        });
        jMenu2.add(mnuItemCustomerSignUp);

        mnuItemCustomerCRUD.setBackground(new java.awt.Color(255, 255, 255));
        mnuItemCustomerCRUD.setForeground(new java.awt.Color(0, 0, 0));
        mnuItemCustomerCRUD.setText("Read, Update And Delete Customer Accounts");
        mnuItemCustomerCRUD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemCustomerCRUDActionPerformed(evt);
            }
        });
        jMenu2.add(mnuItemCustomerCRUD);

        mnuItemReturns.setBackground(new java.awt.Color(255, 255, 255));
        mnuItemReturns.setForeground(new java.awt.Color(0, 0, 0));
        mnuItemReturns.setText("Process Returns");
        mnuItemReturns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemReturnsActionPerformed(evt);
            }
        });
        jMenu2.add(mnuItemReturns);

        jMenuBar1.add(jMenu2);

        mnuAdmin.setBackground(new java.awt.Color(255, 255, 255));
        mnuAdmin.setForeground(new java.awt.Color(0, 0, 0));
        mnuAdmin.setLabel("Return to Admin");
        mnuAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnuAdminMouseClicked(evt);
            }
        });
        jMenuBar1.add(mnuAdmin);

        mnuLogOut.setBackground(new java.awt.Color(255, 255, 255));
        mnuLogOut.setForeground(new java.awt.Color(0, 0, 0));
        mnuLogOut.setText("Log out");
        mnuLogOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnuLogOutMouseClicked(evt);
            }
        });
        jMenuBar1.add(mnuLogOut);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1280, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 960, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddMovieMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMovieMouseEntered
        btnAddMovie.setForeground(Color.GREEN);
    }//GEN-LAST:event_btnAddMovieMouseEntered

    private void btnAddMovieMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMovieMouseExited
        btnAddMovie.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnAddMovieMouseExited

    private void btnLinkAccountMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLinkAccountMouseEntered
        btnLinkAccount.setForeground(Color.GREEN);
    }//GEN-LAST:event_btnLinkAccountMouseEntered

    private void btnLinkAccountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLinkAccountMouseExited
        btnLinkAccount.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnLinkAccountMouseExited

    private void btnMRemoveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRemoveMouseEntered
        btnMRemove.setForeground(Color.RED);
    }//GEN-LAST:event_btnMRemoveMouseEntered

    private void btnMRemoveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRemoveMouseExited
        btnMRemove.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMRemoveMouseExited

    private void btnUnlinkAccountMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUnlinkAccountMouseEntered
        btnUnlinkAccount.setForeground(Color.RED);
    }//GEN-LAST:event_btnUnlinkAccountMouseEntered

    private void btnUnlinkAccountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUnlinkAccountMouseExited
        btnUnlinkAccount.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnUnlinkAccountMouseExited

    private void btnProcessRentalMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProcessRentalMouseEntered
        btnProcessRental.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnProcessRentalMouseEntered

    private void btnProcessRentalMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProcessRentalMouseExited
        btnProcessRental.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnProcessRentalMouseExited

    private void mnuItemCustomerCRUDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemCustomerCRUDActionPerformed
        this.dispose();
        new CustomerCRUD().setVisible(true);
    }//GEN-LAST:event_mnuItemCustomerCRUDActionPerformed

    private void mnuItemCustomerSignUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemCustomerSignUpActionPerformed
        this.dispose();
        new CustomerSignUp();
    }//GEN-LAST:event_mnuItemCustomerSignUpActionPerformed

    private void mnuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemExitActionPerformed
        if (f.logout(true) == 0) { /*System will exit*/ }
    }//GEN-LAST:event_mnuItemExitActionPerformed

    private void mnuLogOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuLogOutMouseClicked
        if (f.logout(false) == 0) this.dispose();
    }//GEN-LAST:event_mnuLogOutMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (f.logout(true) == 0) { /*System will exit*/ }
    }//GEN-LAST:event_formWindowClosing

    private void btnAddMovieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMovieActionPerformed
        //Get the selected ID from cmbMovies
        String selectedID = (String) cmbMovies.getSelectedItem();
        selectedID = selectedID != null ? selectedID.substring(0, selectedID.indexOf(" - ")): null;
        int movieID = selectedID != null ? Integer.parseInt(selectedID) : -1;
        
        //Calculate discount
        String disc = lblRDiscount.getText();
        double discount = !disc.isEmpty() ? Double.parseDouble(disc) : 0.0;
        
        if (movieID != -1)
        {
            /**
             * Extract the relevant information
             * from the database and add it to
             * the table
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT TITLE, RENTAL_PRICE " +
                    "FROM movies " +
                    "WHERE MOVIE_ID = ?"
                );
                ps.setInt(1, movieID);

                rs = ps.executeQuery();

                if (rs.next())
                {
                    Object tblRow[] =
                    {
                        movieID,
                        rs.getObject(1),
                        rs.getObject(2)
                    };
                    
                    dtm.addRow(tblRow);
                    ++items;
                    
                    //Calculate the price
                    price = 0.0;
                    
                    for (int i = 0; i < dtm.getRowCount(); i++) price += (float) tblQueue.getValueAt(i, 2);
                    
                    price -= price * discount;
                    price  = Math.round(price * 100);
                    price /= 100;
                }
                
                rs.close();
                ps.close();
                
                lblNoMovies.setText(items + "");
                lblPrice.setText(price + "");
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_btnAddMovieActionPerformed

    private void btnMRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMRemoveActionPerformed
        //Get the selected ID from tblQueue
        int getRow = tblQueue.getSelectedRow() >= 0 ? tblQueue.getSelectedRow() : -1;
        if (getRow != -1)
        {
            dtm.removeRow(getRow); //Remove selected row
            
            //Recalculate price
            --items;
            price = 0.0;
            
            for (int i = 0; i < dtm.getRowCount(); i++) price += (float) tblQueue.getValueAt(i, 2);
            
            String disc = lblRDiscount.getText();
            double discount = !disc.isEmpty() ? Double.parseDouble(disc) : 0.0;
            
            price -= price * discount;
            price  = Math.round(price * 100);
            price /= 100;
            
            lblNoMovies.setText(items + "");
            lblPrice.setText(price + "");
        }
    }//GEN-LAST:event_btnMRemoveActionPerformed

    private void btnLinkAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLinkAccountActionPerformed
        //Get the selected ID from cmbCustomers
        String selectedID = (String) cmbCustomers.getSelectedItem();
               selectedID = selectedID != null ? selectedID.substring(0, selectedID.indexOf(" - ")) : null;
        int customerID = selectedID != null ? Integer.parseInt(selectedID) : -1;
        
        if (customerID != -1)
        {
            /**
             * Extract the relevant information
             * from the database and bind the UI
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT c.EMAIL, c.NAME, c.SURNAME, c.DOB, c.CONTACT_NUMBER, " +
                    "c.ACCOUNT_CREATION_DATE, mem.NAME, mem.DISCOUNT_PERCENT, c.NO_OF_RENTALS " +
                    "FROM customers c, membership_levels mem " +
                    "WHERE CUSTOMER_ID = ? AND mem.LEVEL_ID = c.FK_LEVEL_ID"
                );
                ps.setInt(1, customerID);

                rs = ps.executeQuery();

                if (rs.next())
                {
                    lblAccount.setText((String) rs.getObject(1));
                    lblRName.setText((String) rs.getObject(2));
                    lblRSurname.setText((String) rs.getObject(3));
                    lblRDOB.setText((String) rs.getObject(4));
                    lblRNumber.setText((String) rs.getObject(5));
                    lblRCreation.setText((String) rs.getObject(6));
                    lblRLevel.setText((String) rs.getObject(7));
                    lblRDiscount.setText(rs.getObject(8) + "");
                    lblRRented.setText(rs.getObject(9) + "");
                }
                
                rs.close();
                ps.close();
                
                //Recalculate the price [because of discounts]
                price = 0.0;
                
                for (int i = 0; i < dtm.getRowCount(); i++) price += (float) tblQueue.getValueAt(i, 2);
                
                price -= price * Double.parseDouble(lblRDiscount.getText());
                price  = Math.round(price * 100);
                price /= 100;
                
                lblPrice.setText(price + "");

                cmbCustomers.setEnabled(false);
                btnLinkAccount.setEnabled(false);
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_btnLinkAccountActionPerformed

    private void btnUnlinkAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnlinkAccountActionPerformed
        unlinkAccount();
    }//GEN-LAST:event_btnUnlinkAccountActionPerformed

    private void btnProcessRentalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessRentalActionPerformed
        //Get the required information to process a transaction
        String customerEmail = lblAccount.getText(),
             transactionDate = lblDate.getText().trim(),
             transactionTime = lblTime.getText().trim();
        
        String cID = (String) cmbCustomers.getSelectedItem();
               cID = cID.substring(0, cID.indexOf(" - "));
                       
        int customerID = Integer.parseInt(cID),
               staffID = Integer.parseInt(lblStaffID.getText().trim());
        
        ArrayList movies = new ArrayList();
        
        for (int i = 0; i < tblQueue.getRowCount(); i++)
        {
            movies.add(tblQueue.getValueAt(i, 0));
        }
        
        /**
         * Processes a transaction only if
         * the required information is collected
         * otherwise an error message would appear
         * to inform the user of the issue
         */
        if (!customerEmail.isEmpty() && !movies.isEmpty())
        {
            if (f.processTransaction(movies, price, transactionDate, transactionTime, customerID, customerEmail, staffID) == 0)
            {
                clear();
                storeBinding();
            }
        }
        else JOptionPane.showMessageDialog(null, "Transaction Invalid\r\n\r\nMake Sure an Account is Linked\r\nand Movies are in the Queue", "Error", JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_btnProcessRentalActionPerformed

    private void mnuItemReturnsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemReturnsActionPerformed
        this.dispose();
        new Returns();
    }//GEN-LAST:event_mnuItemReturnsActionPerformed

    private void mnuAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuAdminMouseClicked
        this.dispose();
        new Admin().setVisible(true);
    }//GEN-LAST:event_mnuAdminMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Store.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Store().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMovie;
    private javax.swing.JButton btnLinkAccount;
    private javax.swing.JButton btnMRemove;
    private javax.swing.JButton btnProcessRental;
    private javax.swing.JButton btnUnlinkAccount;
    private javax.swing.JComboBox<String> cmbCustomers;
    private javax.swing.JComboBox<String> cmbMovies;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAccount;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblNoMovies;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblRCreation;
    private javax.swing.JLabel lblRDOB;
    private javax.swing.JLabel lblRDiscount;
    private javax.swing.JLabel lblRLevel;
    private javax.swing.JLabel lblRName;
    private javax.swing.JLabel lblRNumber;
    private javax.swing.JLabel lblRRented;
    private javax.swing.JLabel lblRSurname;
    private javax.swing.JLabel lblStaffFullName;
    private javax.swing.JLabel lblStaffID;
    private javax.swing.JLabel lblTime;
    private javax.swing.JMenu mnuAdmin;
    private javax.swing.JMenuItem mnuItemCustomerCRUD;
    private javax.swing.JMenuItem mnuItemCustomerSignUp;
    private javax.swing.JMenuItem mnuItemExit;
    private javax.swing.JMenuItem mnuItemReturns;
    private javax.swing.JMenu mnuLogOut;
    private javax.swing.JTable tblQueue;
    // End of variables declaration//GEN-END:variables
}
