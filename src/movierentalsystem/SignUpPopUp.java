package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Staff Sign Up Screen</h1>
 * Staff members who wish to sign up
 * immediately when the system starts
 * up will be redirected from the login
 * screen to create their account here
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SignUpPopUp
{
    
    public SignUpPopUp()
    {
        //Creates new frame
        JFrame f = new JFrame("Movie Shack");
        f.setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage()); //Sets the icon for frame
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 640); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we) { new Login(); }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("SIGN UP"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for name
        JLabel lblName = new JLabel("Name:");
        lblName.setSize(lblName.getPreferredSize());
        
        //Textfield for name to be inputted
        JTextField txfName = new JTextField(15);
        txfName.setSize(txfName.getPreferredSize());
        
        //Label for surname
        JLabel lblSurname = new JLabel("Surname:");
        lblSurname.setSize(lblSurname.getPreferredSize());
        
        //Textfield for surname to be inputted
        JTextField txfSurname = new JTextField(15);
        txfSurname.setSize(txfSurname.getPreferredSize());
        
        //Label for date of birth
        JLabel lblDOB = new JLabel("Date of Birth:");
        lblDOB.setSize(lblDOB.getPreferredSize());
        
        //Textfield for date of birth to be inputted
        JFormattedTextField ftxfDOB = new JFormattedTextField();
        ftxfDOB.setSize(txfName.getPreferredSize());
        ftxfDOB.setToolTipText("DD/MM/YYYY");
        
        //Label for gender
        JLabel lblGender = new JLabel("Gender:");
        lblGender.setSize(lblGender.getPreferredSize());
        
        //Combobox for gender to be selected
        Object o[] = { "Male", "Female" };
        JComboBox cmbGender = new JComboBox(o);
        cmbGender.setSize(txfName.getPreferredSize());
        cmbGender.setSelectedIndex(0);
        cmbGender.setBackground(Color.WHITE);
        
        //Label for physical address
        JLabel lblPhysical = new JLabel("Physical Address:");
        lblPhysical.setSize(lblPhysical.getPreferredSize());
        
        //Textfield for physical address to be inputted
        JTextField txfPhysical = new JTextField(15);
        txfPhysical.setSize(txfPhysical.getPreferredSize());
        
        //Label for contact number
        JLabel lblNumber = new JLabel("Contact Number:");
        lblNumber.setSize(lblNumber.getPreferredSize());
        
        //Textfield for contact number to be inputted
        JTextField txfNumber = new JTextField(15);
        txfNumber.setSize(txfNumber.getPreferredSize());
        
        //Label for email address
        JLabel lblEmail = new JLabel("Email Address");
        lblEmail.setSize(lblEmail.getPreferredSize());
        
        //Textfield for email address to be inputted
        JFormattedTextField ftxfEmail = new JFormattedTextField();
        ftxfEmail.setSize(txfName.getPreferredSize());
        
        //Label for password
        JLabel lblPassword = new JLabel("Enter New Password:");
        lblPassword.setSize(lblPassword.getPreferredSize());
        
        //Password field for a new password to be inputted
        JPasswordField pwfPassword = new JPasswordField(15);
        pwfPassword.setSize(pwfPassword.getPreferredSize());
        
        //Label for confirm password
        JLabel lblConfirm = new JLabel("Confirm New Password:");
        lblConfirm.setSize(lblConfirm.getPreferredSize());
        
        //Password field for a new password to be confirmed
        JPasswordField pwfConfirm = new JPasswordField(15);
        pwfConfirm.setSize(pwfConfirm.getPreferredSize());
        
        //Button to sign up a new staff member
        JButton btnSignUp = new JButton("SIGN UP");
        btnSignUp.setSize(btnSignUp.getPreferredSize());
        btnSignUp.setBackground(Color.WHITE);
        btnSignUp.setForeground(Color.BLACK);
        btnSignUp.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String name = txfName.getText(),
                    surname = txfSurname.getText().toLowerCase(),
                        DOB = ftxfDOB.getText(),
                     gender = cmbGender.getSelectedItem().toString(),
                   physical = txfPhysical.getText(),
                     number = txfNumber.getText(),
                      email = ftxfEmail.getText(),
                   password = "",
                    confirm = "";
                
                char passwordArr[] = pwfPassword.getPassword(),
                      confirmArr[] = pwfConfirm.getPassword();
                
                //Iterating through the char arrays to represent the passwords as Strings
                for (char c : passwordArr) password += c;
                for (char c : confirmArr) confirm += c;
                
                Functions f1 = new Functions();
                
                if (f1.staff(name, surname, DOB, gender, physical, number, email, password, confirm, 0, -1) == 0)
                {
                    txfName.setText("");
                    txfSurname.setText("");
                    ftxfDOB.setText("");
                    cmbGender.setSelectedIndex(0);
                    txfPhysical.setText("");
                    txfNumber.setText("");
                    ftxfEmail.setText("");
                    pwfPassword.setText("");
                    pwfConfirm.setText("");
                }
            }

            @Override
            public void mouseEntered(MouseEvent me) { btnSignUp.setForeground(Color.GREEN); }

            @Override
            public void mouseExited(MouseEvent me) { btnSignUp.setForeground(Color.BLACK); }
        });
        
        //Button to cancel operation
        JButton btnCancel = new JButton("CANCEL");
        btnCancel.setSize(btnSignUp.getPreferredSize());
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new Login();
            }

            @Override
            public void mouseEntered(MouseEvent me) { btnCancel.setForeground(Color.RED); }

            @Override
            public void mouseExited(MouseEvent me) { btnCancel.setForeground(Color.BLACK); }
        });
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 80);
        lblName.setLocation(((f.getWidth()-txfName.getWidth())/2) - (txfName.getWidth()/2) - 20, 180);
        txfName.setLocation(((f.getWidth()-txfName.getWidth())/2) - (txfName.getWidth()/2) - 20, 200);
        lblSurname.setLocation(((f.getWidth()-txfSurname.getWidth())/2) - (txfSurname.getWidth()/2) - 20, 240);
        txfSurname.setLocation(((f.getWidth()-txfSurname.getWidth())/2) - (txfSurname.getWidth()/2) - 20, 260);
        lblDOB.setLocation(((f.getWidth()-ftxfDOB.getWidth())/2) - (ftxfDOB.getWidth()/2) - 20, 300);
        ftxfDOB.setLocation(((f.getWidth()-ftxfDOB.getWidth())/2) - (ftxfDOB.getWidth()/2) - 20, 320);
        lblGender.setLocation(((f.getWidth()-cmbGender.getWidth())/2) - (cmbGender.getWidth()/2) - 20, 360);
        cmbGender.setLocation(((f.getWidth()-cmbGender.getWidth())/2) - (cmbGender.getWidth()/2) - 20, 380);
        lblPhysical.setLocation(((f.getWidth()-txfPhysical.getWidth())/2) - (txfPhysical.getWidth()/2) - 20, 420);
        txfPhysical.setLocation(((f.getWidth()-txfPhysical.getWidth())/2) - (txfPhysical.getWidth()/2) - 20, 440);
        lblNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2) + (txfNumber.getWidth()/2) + 20, 180);
        txfNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2) + (txfNumber.getWidth()/2) + 20, 200);
        lblEmail.setLocation(((f.getWidth()-ftxfEmail.getWidth())/2) + (ftxfEmail.getWidth()/2) + 20, 240);
        ftxfEmail.setLocation(((f.getWidth()-ftxfEmail.getWidth())/2) + (ftxfEmail.getWidth()/2) + 20, 260);
        lblPassword.setLocation(((f.getWidth()-pwfPassword.getWidth())/2) + (pwfPassword.getWidth()/2) + 20, 300);
        pwfPassword.setLocation(((f.getWidth()-pwfPassword.getWidth())/2) + (pwfPassword.getWidth()/2) + 20, 320);
        lblConfirm.setLocation(((f.getWidth()-pwfConfirm.getWidth())/2) + (pwfConfirm.getWidth()/2) + 20, 360);
        pwfConfirm.setLocation(((f.getWidth()-pwfConfirm.getWidth())/2) + (pwfConfirm.getWidth()/2) + 20, 380);
        btnSignUp.setLocation(((f.getWidth()-btnSignUp.getWidth())/2) - (btnSignUp.getWidth()/2) - 10, 520);
        btnCancel.setLocation(((f.getWidth()-btnCancel.getWidth())/2) + (btnCancel.getWidth()/2) + 10, 520);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblName);
        f.getContentPane().add(txfName);
        f.getContentPane().add(lblSurname);
        f.getContentPane().add(txfSurname);
        f.getContentPane().add(lblDOB);
        f.getContentPane().add(ftxfDOB);
        f.getContentPane().add(lblGender);
        f.getContentPane().add(cmbGender);
        f.getContentPane().add(lblPhysical);
        f.getContentPane().add(txfPhysical);
        f.getContentPane().add(lblNumber);
        f.getContentPane().add(txfNumber);
        f.getContentPane().add(lblEmail);
        f.getContentPane().add(ftxfEmail);
        f.getContentPane().add(lblPassword);
        f.getContentPane().add(pwfPassword);
        f.getContentPane().add(lblConfirm);
        f.getContentPane().add(pwfConfirm);
        f.getContentPane().add(btnSignUp);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
        
    }
    
}
