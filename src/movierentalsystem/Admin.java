package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Admin Screen</h1>
 * This screen is dedicated to admin
 * and the administrative tasks of
 * creating, reading, updating
 * and deleting staff and movies
 * from the database. The database
 * itself is viewable in this screen
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.Color;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Admin extends javax.swing.JFrame
{

    //Creates new form Admin
    public Admin()
    {
        initComponents();
        
        bindStaffCRUD();
        bindMovieCRUD();
        
        bindtblStaff();
        bindtblSessions();
        bindtblMovies();
        bindtblLevels();
        bindtblCustomers();
        bindtblTransactions();
        bindtblRentals();
    }
    
    Functions f = new Functions();
    
    Connection c = Functions.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    private void bindStaffCRUD()
    {
        //Clear text fields and labels from staff CRUD
        txfSCName.setText("");
        txfSCSurname.setText("");
        ftxfSCDOB.setText("");
        cmbSCGender.setSelectedIndex(0);
        txfSCPhysical.setText("");
        txfSCNumber.setText("");
        ftxfSCEmail.setText("");
        pwfSCPassword.setText("");
        pwfSCConfirm.setText("");
        
        lblSRName.setText("");
        lblSRSurname.setText("");
        lblSRDOB.setText("");
        lblSRGender.setText("");
        lblSRPhysical.setText("");
        lblSRNumber.setText("");
        lblSREmail.setText("");
        lblSRStartDate.setText("");
        
        txfSUName.setText("");
        txfSUSurname.setText("");
        ftxfSUDOB.setText("");
        cmbSUGender.setSelectedIndex(0);
        txfSUPhysical.setText("");
        txfSUNumber.setText("");
        ftxfSUEmail.setText("");
        
        lblSDName.setText("");
        lblSDSurname.setText("");
        lblSDDOB.setText("");
        lblSDGender.setText("");
        lblSDPhysical.setText("");
        lblSDNumber.setText("");
        lblSDEmail.setText("");
        lblSDStartDate.setText("");
        
        //Remove selection from all the lists in staff CRUD
        lstSRead.clearSelection();
        lstSUpdate.clearSelection();
        lstSDelete.clearSelection();
        
        //Create a model to be used to bind the lists in staff CRUD
        DefaultListModel dlm = new DefaultListModel();
        
        /**
         * Extract staffID and email from the database
         * and combine it to form an element for all
         * the lists
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT STAFF_ID, EMAIL " +
                "FROM staff"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                dlm.addElement(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dlm for all lists in staff CRUD
        lstSRead.setModel(dlm);
        lstSUpdate.setModel(dlm);
        lstSDelete.setModel(dlm);
    }
    
    private void bindMovieCRUD()
    {
        //Clear text fields and labels from movie CRUD
        txfMCTitle.setText("");
        txfMCSynopsis.setText("");
        txfMCRunTime.setText("");
        cmbMCRated.setSelectedIndex(0);
        txfMCProducer.setText("");
        txfMCDirector.setText("");
        ftxfMCRelease.setText("");
        txfMCPrice.setText("");
        txfMCCopies.setText("");
        
        lblMRTitle.setText("");
        lblMRSynopsis.setText("");
        lblMRRunTime.setText("");
        lblMRRated.setText("");
        lblMRProducer.setText("");
        lblMRDirector.setText("");
        lblMRRelease.setText("");
        lblMRPrice.setText("");
        lblMRCopies.setText("");
        
        txfMUTitle.setText("");
        txfMUSynopsis.setText("");
        txfMURunTime.setText("");
        cmbMURated.setSelectedIndex(0);
        txfMUProducer.setText("");
        txfMUDirector.setText("");
        ftxfMURelease.setText("");
        txfMUPrice.setText("");
        txfMUCopies.setText("");
        
        lblMDTitle.setText("");
        lblMDSynopsis.setText("");
        lblMDRunTime.setText("");
        lblMDRated.setText("");
        lblMDProducer.setText("");
        lblMDDirector.setText("");
        lblMDRelease.setText("");
        lblMDPrice.setText("");
        lblMDCopies.setText("");
        
        //Remove selection from all the lists in movie CRUD
        lstMRead.clearSelection();
        lstMUpdate.clearSelection();
        lstMDelete.clearSelection();
        
        //Create a model to be used to bind the lists in movie CRUD
        DefaultListModel dlm = new DefaultListModel();
        
        /**
         * Extract movieID and title from the database
         * and combine it to form an element for all
         * the lists
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT MOVIE_ID, TITLE " +
                "FROM movies"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                dlm.addElement(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dlm for all lists in movie CRUD
        lstMRead.setModel(dlm);
        lstMUpdate.setModel(dlm);
        lstMDelete.setModel(dlm);
    }
    
    private void bindtblStaff()
    {
        //Column names for the table
        Object headers[] = 
        {
            "NAME",
            "SURNAME",
            "DOB",
            "GENDER",
            "PHYSICAL ADDRESS",
            "CONTACT NUMBER",
            "EMAIL ADDRESS",
            "START DATE"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL, START_DATE " +
                "FROM staff"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(6),
                    rs.getObject(7),
                    rs.getObject(8)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblStaff.setModel(dtm);
    }
    
    private void bindtblSessions()
    {
        //Column names for the table
        Object headers[] = 
        {
            "SESSION DATE",
            "START TIME",
            "END TIME",
            "STAFF MEMBER"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT se.SESSION_DATE, se.START_TIME, se.END_TIME, " +
                "s.NAME, s.SURNAME " +
                "FROM sessions se, staff s " +
                "WHERE s.STAFF_ID = se.FK_STAFF_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4) + " " + rs.getObject(5)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblSessions.setModel(dtm);
    }
    
    private void bindtblMovies()
    {
        //Column names for the table
        Object headers[] = 
        {
            "TITLE",
            "SYNOPSIS",
            "RUNTIME",
            "RATED",
            "PRODUCER",
            "DIRECTOR",
            "RELEASE DATE",
            "RENTAL PRICE",
            "NUMBER OF COPIES"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE, RENTAL_PRICE, NO_OF_COPIES " +
                "FROM movies"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(6),
                    rs.getObject(7),
                    rs.getObject(8),
                    rs.getObject(9)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblMovies.setModel(dtm);
    }
    
    private void bindtblLevels()
    {
        //Column names for the table
        Object headers[] =
        {
            "NAME",
            "DISCOUNT PERCENTAGE",
            "TOTAL CUSTOMERS PER LEVEL"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT NAME, DISCOUNT_PERCENT, " +
                    "(SELECT COUNT(FK_LEVEL_ID) AS level " +
                    "FROM customers " +
                    "WHERE membership_levels.LEVEL_ID = customers.FK_LEVEL_ID) " +
                "FROM membership_levels"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    Math.round(rs.getDouble(2) * 100),
                    rs.getObject(3)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblLevels.setModel(dtm);
    }
    
    private void bindtblCustomers()
    {
        //Column names for the table
        Object headers[] = 
        {
            "NAME",
            "SURNAME",
            "DOB",
            "GENDER",
            "PHYSICAL ADDRESS",
            "CONTACT NUMBER",
            "EMAIL ADDRESS",
            "ACCOUNT CREATION DATE",
            "MEMBERSHIP LEVEL",
            "NUMBER OF RENTALS"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT c.NAME, c.SURNAME, c.DOB, c.GENDER, c.PHYSICAL_ADDRESS, c.CONTACT_NUMBER, c.EMAIL, " +
                "c.ACCOUNT_CREATION_DATE, mem.NAME, c.NO_OF_RENTALS " +
                "FROM customers c, membership_levels mem " +
                "WHERE mem.LEVEL_ID = c.FK_LEVEL_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(6),
                    rs.getObject(7),
                    rs.getObject(8),
                    rs.getObject(9),
                    rs.getObject(10)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblCustomers.setModel(dtm);
    }
    
    private void bindtblTransactions()
    {
        //Column names for the table
        Object headers[] = 
        {
            "TRANSACTION ID",
            "DATE",
            "TIME",
            "TOTAL RENTALS",
            "TOTAL PRICE",
            "PROCESSED BY",
            "CUSTOMER"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT t.TRANSACTION_ID, t.DATE, t.TIME, t.TOTAL_RENTALS, t.TOTAL_PRICE, " +
                "s.NAME, s.SURNAME, c.NAME, c.SURNAME " +
                "FROM transactions t, staff s, customers c " +
                "WHERE s.STAFF_ID = t.FK_STAFF_ID AND c.CUSTOMER_ID = t.FK_CUSTOMER_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(6) + " " + rs.getObject(7),
                    rs.getObject(8) + " " + rs.getObject(9)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblTransactions.setModel(dtm);
    }
    
    private void bindtblRentals()
    {
        //Column names for the table
        Object headers[] =
        {
            "MOVIE TITLE",
            "DUE DATE",
            "RETURN DATE",
            "TRANSACTION ID"
        };
        
        /**
         * Create a model for the table
         * that overrides the ability
         * to edit cells of the table
         */
        DefaultTableModel dtm = new DefaultTableModel(headers, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        
        /**
         * Extract the relevant information
         * from the database and add it to
         * the table
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT m.TITLE, r.DUE_DATE, r.RETURN_DATE, r.FK_TRANSACTION_ID " +
                "FROM movies m, rentals_per_transaction r " +
                "WHERE m.MOVIE_ID = r.FK_MOVIE_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4)
                };
                
                dtm.addRow(row);
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dtm for the table
        tblRentals.setModel(dtm);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel11 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txfSCName = new javax.swing.JTextField();
        txfSCSurname = new javax.swing.JTextField();
        ftxfSCDOB = new javax.swing.JFormattedTextField();
        txfSCPhysical = new javax.swing.JTextField();
        txfSCNumber = new javax.swing.JTextField();
        ftxfSCEmail = new javax.swing.JFormattedTextField();
        btnSCreate = new javax.swing.JButton();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        pwfSCPassword = new javax.swing.JPasswordField();
        pwfSCConfirm = new javax.swing.JPasswordField();
        jLabel66 = new javax.swing.JLabel();
        cmbSCGender = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lblSRName = new javax.swing.JLabel();
        lblSRSurname = new javax.swing.JLabel();
        lblSRDOB = new javax.swing.JLabel();
        lblSRPhysical = new javax.swing.JLabel();
        lblSRNumber = new javax.swing.JLabel();
        lblSRStartDate = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstSRead = new javax.swing.JList<>();
        btnSRFirst = new javax.swing.JButton();
        btnSRPrevious = new javax.swing.JButton();
        btnSRNext = new javax.swing.JButton();
        btnSRLast = new javax.swing.JButton();
        jLabel70 = new javax.swing.JLabel();
        lblSRGender = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        lblSREmail = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txfSUName = new javax.swing.JTextField();
        txfSUSurname = new javax.swing.JTextField();
        ftxfSUDOB = new javax.swing.JFormattedTextField();
        txfSUPhysical = new javax.swing.JTextField();
        txfSUNumber = new javax.swing.JTextField();
        ftxfSUEmail = new javax.swing.JFormattedTextField();
        btnSUFirst = new javax.swing.JButton();
        btnSUPrevious = new javax.swing.JButton();
        btnSUNext = new javax.swing.JButton();
        btnSULast = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstSUpdate = new javax.swing.JList<>();
        btnSUpdate = new javax.swing.JButton();
        jLabel67 = new javax.swing.JLabel();
        cmbSUGender = new javax.swing.JComboBox<>();
        jPanel6 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        btnSDFirst = new javax.swing.JButton();
        btnSDPrevious = new javax.swing.JButton();
        btnSDNext = new javax.swing.JButton();
        btnSDLast = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstSDelete = new javax.swing.JList<>();
        btnSDelete = new javax.swing.JButton();
        jLabel68 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        lblSDName = new javax.swing.JLabel();
        lblSDSurname = new javax.swing.JLabel();
        lblSDDOB = new javax.swing.JLabel();
        lblSDGender = new javax.swing.JLabel();
        lblSDPhysical = new javax.swing.JLabel();
        lblSDNumber = new javax.swing.JLabel();
        lblSDEmail = new javax.swing.JLabel();
        lblSDStartDate = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel7 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        txfMCTitle = new javax.swing.JTextField();
        txfMCSynopsis = new javax.swing.JTextField();
        txfMCRunTime = new javax.swing.JTextField();
        txfMCProducer = new javax.swing.JTextField();
        txfMCDirector = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        ftxfMCRelease = new javax.swing.JFormattedTextField();
        txfMCPrice = new javax.swing.JTextField();
        txfMCCopies = new javax.swing.JTextField();
        btnMCreate = new javax.swing.JButton();
        cmbMCRated = new javax.swing.JComboBox<>();
        jPanel8 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        lblMRTitle = new javax.swing.JLabel();
        lblMRSynopsis = new javax.swing.JLabel();
        lblMRRunTime = new javax.swing.JLabel();
        lblMRRated = new javax.swing.JLabel();
        lblMRProducer = new javax.swing.JLabel();
        lblMRDirector = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        lblMRRelease = new javax.swing.JLabel();
        lblMRPrice = new javax.swing.JLabel();
        lblMRCopies = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        lstMRead = new javax.swing.JList<>();
        btnMRFirst = new javax.swing.JButton();
        btnMRPrevious = new javax.swing.JButton();
        btnMRNext = new javax.swing.JButton();
        btnMRLast = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        txfMUTitle = new javax.swing.JTextField();
        txfMUSynopsis = new javax.swing.JTextField();
        txfMURunTime = new javax.swing.JTextField();
        txfMUProducer = new javax.swing.JTextField();
        txfMUDirector = new javax.swing.JTextField();
        ftxfMURelease = new javax.swing.JFormattedTextField();
        txfMUPrice = new javax.swing.JTextField();
        txfMUCopies = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        lstMUpdate = new javax.swing.JList<>();
        btnMUpdate = new javax.swing.JButton();
        btnMUFirst = new javax.swing.JButton();
        btnMUPrevious = new javax.swing.JButton();
        btnMUNext = new javax.swing.JButton();
        btnMULast = new javax.swing.JButton();
        cmbMURated = new javax.swing.JComboBox<>();
        jPanel10 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        lstMDelete = new javax.swing.JList<>();
        btnMDelete = new javax.swing.JButton();
        btnMDFirst = new javax.swing.JButton();
        btnMDPrevious = new javax.swing.JButton();
        btnMDNext = new javax.swing.JButton();
        btnMDLast = new javax.swing.JButton();
        lblMDTitle = new javax.swing.JLabel();
        lblMDSynopsis = new javax.swing.JLabel();
        lblMDRunTime = new javax.swing.JLabel();
        lblMDRated = new javax.swing.JLabel();
        lblMDProducer = new javax.swing.JLabel();
        lblMDDirector = new javax.swing.JLabel();
        lblMDRelease = new javax.swing.JLabel();
        lblMDPrice = new javax.swing.JLabel();
        lblMDCopies = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblStaff = new javax.swing.JTable();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        tblSessions = new javax.swing.JTable();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblMovies = new javax.swing.JTable();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane13 = new javax.swing.JScrollPane();
        tblLevels = new javax.swing.JTable();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tblCustomers = new javax.swing.JTable();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tblTransactions = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        tblRentals = new javax.swing.JTable();
        btnStore = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnuItemExit = new javax.swing.JMenuItem();
        mnuLogOut = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Movie Shack");
        setBackground(new java.awt.Color(255, 255, 255));
        setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage());
        setLocation(new java.awt.Point(0, 0));
        setMinimumSize(new java.awt.Dimension(1280, 960));
        setResizable(false);
        setSize(new java.awt.Dimension(1280, 960));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ADMIN");
        jPanel11.add(jLabel1);
        jLabel1.setBounds(565, 40, 130, 52);

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setForeground(new java.awt.Color(0, 0, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setForeground(new java.awt.Color(0, 0, 0));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Name:");
        jPanel3.add(jLabel4);
        jLabel4.setBounds(100, 90, 50, 16);

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Surname:");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(100, 160, 60, 16);

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Date of Birth:");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(100, 230, 80, 16);

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Physical Address:");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(100, 370, 110, 16);

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Contact Number:");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(100, 440, 100, 16);

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Email Address:");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(530, 90, 90, 16);

        txfSCName.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(txfSCName);
        txfSCName.setBounds(250, 85, 180, 25);

        txfSCSurname.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(txfSCSurname);
        txfSCSurname.setBounds(250, 155, 180, 25);

        ftxfSCDOB.setForeground(new java.awt.Color(0, 0, 0));
        ftxfSCDOB.setToolTipText("DD/MM/YYYY");
        jPanel3.add(ftxfSCDOB);
        ftxfSCDOB.setBounds(250, 225, 180, 25);

        txfSCPhysical.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(txfSCPhysical);
        txfSCPhysical.setBounds(250, 365, 180, 25);

        txfSCNumber.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(txfSCNumber);
        txfSCNumber.setBounds(250, 435, 180, 25);

        ftxfSCEmail.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(ftxfSCEmail);
        ftxfSCEmail.setBounds(700, 85, 180, 25);

        btnSCreate.setBackground(new java.awt.Color(255, 255, 255));
        btnSCreate.setForeground(new java.awt.Color(0, 0, 0));
        btnSCreate.setText("CREATE NEW STAFF ACCOUNT");
        btnSCreate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSCreateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSCreateMouseExited(evt);
            }
        });
        btnSCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSCreateActionPerformed(evt);
            }
        });
        jPanel3.add(btnSCreate);
        btnSCreate.setBounds(980, 450, 210, 32);

        jLabel28.setForeground(new java.awt.Color(0, 0, 0));
        jLabel28.setText("Password:");
        jPanel3.add(jLabel28);
        jLabel28.setBounds(530, 160, 70, 16);

        jLabel29.setForeground(new java.awt.Color(0, 0, 0));
        jLabel29.setText("Confirm Password:");
        jPanel3.add(jLabel29);
        jLabel29.setBounds(530, 230, 120, 16);

        pwfSCPassword.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(pwfSCPassword);
        pwfSCPassword.setBounds(700, 155, 180, 25);

        pwfSCConfirm.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(pwfSCConfirm);
        pwfSCConfirm.setBounds(700, 225, 180, 25);

        jLabel66.setForeground(new java.awt.Color(0, 0, 0));
        jLabel66.setText("Gender:");
        jPanel3.add(jLabel66);
        jLabel66.setBounds(100, 300, 44, 16);

        cmbSCGender.setBackground(new java.awt.Color(255, 255, 255));
        cmbSCGender.setForeground(new java.awt.Color(0, 0, 0));
        cmbSCGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        jPanel3.add(cmbSCGender);
        cmbSCGender.setBounds(250, 295, 180, 25);

        jTabbedPane2.addTab("CREATE", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(null);

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("Name:");
        jPanel4.add(jLabel10);
        jLabel10.setBounds(100, 90, 50, 16);

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Surname:");
        jPanel4.add(jLabel11);
        jLabel11.setBounds(100, 160, 60, 16);

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("Date of Birth:");
        jPanel4.add(jLabel12);
        jLabel12.setBounds(100, 230, 80, 16);

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Physical Address:");
        jPanel4.add(jLabel13);
        jLabel13.setBounds(100, 370, 110, 16);

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Contact Number:");
        jPanel4.add(jLabel14);
        jLabel14.setBounds(100, 440, 100, 16);

        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Email Address:");
        jPanel4.add(jLabel15);
        jLabel15.setBounds(530, 90, 90, 16);

        lblSRName.setForeground(new java.awt.Color(0, 0, 0));
        lblSRName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRName);
        lblSRName.setBounds(250, 89, 180, 18);

        lblSRSurname.setForeground(new java.awt.Color(0, 0, 0));
        lblSRSurname.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRSurname);
        lblSRSurname.setBounds(250, 159, 180, 18);

        lblSRDOB.setForeground(new java.awt.Color(0, 0, 0));
        lblSRDOB.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRDOB);
        lblSRDOB.setBounds(250, 229, 180, 18);

        lblSRPhysical.setForeground(new java.awt.Color(0, 0, 0));
        lblSRPhysical.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRPhysical);
        lblSRPhysical.setBounds(250, 369, 180, 18);

        lblSRNumber.setForeground(new java.awt.Color(0, 0, 0));
        lblSRNumber.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRNumber);
        lblSRNumber.setBounds(250, 439, 180, 18);

        lblSRStartDate.setForeground(new java.awt.Color(0, 0, 0));
        lblSRStartDate.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRStartDate);
        lblSRStartDate.setBounds(700, 159, 180, 18);

        lstSRead.setForeground(new java.awt.Color(0, 0, 0));
        lstSRead.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstSRead.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstSReadValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstSRead);

        jPanel4.add(jScrollPane1);
        jScrollPane1.setBounds(940, 80, 240, 300);

        btnSRFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnSRFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnSRFirst.setText("FIRST");
        btnSRFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSRFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSRFirstMouseExited(evt);
            }
        });
        btnSRFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSRFirstActionPerformed(evt);
            }
        });
        jPanel4.add(btnSRFirst);
        btnSRFirst.setBounds(940, 400, 110, 32);

        btnSRPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnSRPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnSRPrevious.setText("PREVIOUS");
        btnSRPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSRPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSRPreviousMouseExited(evt);
            }
        });
        btnSRPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSRPreviousActionPerformed(evt);
            }
        });
        jPanel4.add(btnSRPrevious);
        btnSRPrevious.setBounds(1070, 400, 110, 32);

        btnSRNext.setBackground(new java.awt.Color(255, 255, 255));
        btnSRNext.setForeground(new java.awt.Color(0, 0, 0));
        btnSRNext.setText("NEXT");
        btnSRNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSRNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSRNextMouseExited(evt);
            }
        });
        btnSRNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSRNextActionPerformed(evt);
            }
        });
        jPanel4.add(btnSRNext);
        btnSRNext.setBounds(940, 450, 110, 32);

        btnSRLast.setBackground(new java.awt.Color(255, 255, 255));
        btnSRLast.setForeground(new java.awt.Color(0, 0, 0));
        btnSRLast.setText("LAST");
        btnSRLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSRLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSRLastMouseExited(evt);
            }
        });
        btnSRLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSRLastActionPerformed(evt);
            }
        });
        jPanel4.add(btnSRLast);
        btnSRLast.setBounds(1070, 450, 110, 32);

        jLabel70.setForeground(new java.awt.Color(0, 0, 0));
        jLabel70.setText("Gender:");
        jPanel4.add(jLabel70);
        jLabel70.setBounds(100, 300, 44, 16);

        lblSRGender.setForeground(new java.awt.Color(0, 0, 0));
        lblSRGender.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSRGender);
        lblSRGender.setBounds(250, 299, 180, 18);

        jLabel71.setForeground(new java.awt.Color(0, 0, 0));
        jLabel71.setText("Start Date:");
        jPanel4.add(jLabel71);
        jLabel71.setBounds(530, 160, 70, 16);

        lblSREmail.setForeground(new java.awt.Color(0, 0, 0));
        lblSREmail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.add(lblSREmail);
        lblSREmail.setBounds(700, 89, 180, 18);

        jTabbedPane2.addTab("READ", jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Name:");
        jPanel5.add(jLabel16);
        jLabel16.setBounds(100, 90, 50, 16);

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Surname:");
        jPanel5.add(jLabel17);
        jLabel17.setBounds(100, 160, 60, 16);

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Date of Birth:");
        jPanel5.add(jLabel18);
        jLabel18.setBounds(100, 230, 80, 16);

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("Physical Address:");
        jPanel5.add(jLabel19);
        jLabel19.setBounds(100, 370, 110, 16);

        jLabel20.setForeground(new java.awt.Color(0, 0, 0));
        jLabel20.setText("Contact Number:");
        jPanel5.add(jLabel20);
        jLabel20.setBounds(100, 440, 100, 16);

        jLabel21.setForeground(new java.awt.Color(0, 0, 0));
        jLabel21.setText("Email Address:");
        jPanel5.add(jLabel21);
        jLabel21.setBounds(530, 90, 90, 16);

        txfSUName.setForeground(new java.awt.Color(0, 0, 0));
        jPanel5.add(txfSUName);
        txfSUName.setBounds(250, 85, 180, 25);

        txfSUSurname.setForeground(new java.awt.Color(0, 0, 0));
        jPanel5.add(txfSUSurname);
        txfSUSurname.setBounds(250, 155, 180, 25);

        ftxfSUDOB.setForeground(new java.awt.Color(0, 0, 0));
        ftxfSUDOB.setToolTipText("DD/MM/YYYY");
        jPanel5.add(ftxfSUDOB);
        ftxfSUDOB.setBounds(250, 225, 180, 25);

        txfSUPhysical.setForeground(new java.awt.Color(0, 0, 0));
        jPanel5.add(txfSUPhysical);
        txfSUPhysical.setBounds(250, 365, 180, 25);

        txfSUNumber.setForeground(new java.awt.Color(0, 0, 0));
        jPanel5.add(txfSUNumber);
        txfSUNumber.setBounds(250, 435, 180, 25);

        ftxfSUEmail.setForeground(new java.awt.Color(0, 0, 0));
        jPanel5.add(ftxfSUEmail);
        ftxfSUEmail.setBounds(700, 85, 180, 25);

        btnSUFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnSUFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnSUFirst.setText("FIRST");
        btnSUFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSUFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSUFirstMouseExited(evt);
            }
        });
        btnSUFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSUFirstActionPerformed(evt);
            }
        });
        jPanel5.add(btnSUFirst);
        btnSUFirst.setBounds(940, 400, 110, 32);

        btnSUPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnSUPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnSUPrevious.setText("PREVIOUS");
        btnSUPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSUPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSUPreviousMouseExited(evt);
            }
        });
        btnSUPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSUPreviousActionPerformed(evt);
            }
        });
        jPanel5.add(btnSUPrevious);
        btnSUPrevious.setBounds(1070, 400, 110, 32);

        btnSUNext.setBackground(new java.awt.Color(255, 255, 255));
        btnSUNext.setForeground(new java.awt.Color(0, 0, 0));
        btnSUNext.setText("NEXT");
        btnSUNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSUNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSUNextMouseExited(evt);
            }
        });
        btnSUNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSUNextActionPerformed(evt);
            }
        });
        jPanel5.add(btnSUNext);
        btnSUNext.setBounds(940, 450, 110, 32);

        btnSULast.setBackground(new java.awt.Color(255, 255, 255));
        btnSULast.setForeground(new java.awt.Color(0, 0, 0));
        btnSULast.setText("LAST");
        btnSULast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSULastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSULastMouseExited(evt);
            }
        });
        btnSULast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSULastActionPerformed(evt);
            }
        });
        jPanel5.add(btnSULast);
        btnSULast.setBounds(1070, 450, 110, 32);

        lstSUpdate.setForeground(new java.awt.Color(0, 0, 0));
        lstSUpdate.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstSUpdateValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(lstSUpdate);

        jPanel5.add(jScrollPane2);
        jScrollPane2.setBounds(940, 80, 240, 300);

        btnSUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnSUpdate.setForeground(new java.awt.Color(0, 0, 0));
        btnSUpdate.setText("UPDATE STAFF ACCOUNT");
        btnSUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSUpdateMouseExited(evt);
            }
        });
        btnSUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSUpdateActionPerformed(evt);
            }
        });
        jPanel5.add(btnSUpdate);
        btnSUpdate.setBounds(600, 510, 210, 32);

        jLabel67.setForeground(new java.awt.Color(0, 0, 0));
        jLabel67.setText("Gender:");
        jPanel5.add(jLabel67);
        jLabel67.setBounds(100, 300, 44, 16);

        cmbSUGender.setBackground(new java.awt.Color(255, 255, 255));
        cmbSUGender.setForeground(new java.awt.Color(0, 0, 0));
        cmbSUGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        jPanel5.add(cmbSUGender);
        cmbSUGender.setBounds(250, 295, 180, 26);

        jTabbedPane2.addTab("UPDATE", jPanel5);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(null);

        jLabel22.setForeground(new java.awt.Color(0, 0, 0));
        jLabel22.setText("Name:");
        jPanel6.add(jLabel22);
        jLabel22.setBounds(100, 90, 50, 16);

        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("Surname:");
        jPanel6.add(jLabel23);
        jLabel23.setBounds(100, 160, 60, 16);

        jLabel24.setForeground(new java.awt.Color(0, 0, 0));
        jLabel24.setText("Date of Birth:");
        jPanel6.add(jLabel24);
        jLabel24.setBounds(100, 230, 80, 16);

        jLabel25.setForeground(new java.awt.Color(0, 0, 0));
        jLabel25.setText("Physical Address:");
        jPanel6.add(jLabel25);
        jLabel25.setBounds(100, 370, 110, 16);

        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("Contact Number:");
        jPanel6.add(jLabel26);
        jLabel26.setBounds(100, 440, 100, 16);

        jLabel27.setForeground(new java.awt.Color(0, 0, 0));
        jLabel27.setText("Email Address:");
        jPanel6.add(jLabel27);
        jLabel27.setBounds(530, 90, 90, 16);

        btnSDFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnSDFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnSDFirst.setText("FIRST");
        btnSDFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSDFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSDFirstMouseExited(evt);
            }
        });
        btnSDFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSDFirstActionPerformed(evt);
            }
        });
        jPanel6.add(btnSDFirst);
        btnSDFirst.setBounds(940, 400, 110, 32);

        btnSDPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnSDPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnSDPrevious.setText("PREVIOUS");
        btnSDPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSDPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSDPreviousMouseExited(evt);
            }
        });
        btnSDPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSDPreviousActionPerformed(evt);
            }
        });
        jPanel6.add(btnSDPrevious);
        btnSDPrevious.setBounds(1070, 400, 110, 32);

        btnSDNext.setBackground(new java.awt.Color(255, 255, 255));
        btnSDNext.setForeground(new java.awt.Color(0, 0, 0));
        btnSDNext.setText("NEXT");
        btnSDNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSDNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSDNextMouseExited(evt);
            }
        });
        btnSDNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSDNextActionPerformed(evt);
            }
        });
        jPanel6.add(btnSDNext);
        btnSDNext.setBounds(940, 450, 110, 32);

        btnSDLast.setBackground(new java.awt.Color(255, 255, 255));
        btnSDLast.setForeground(new java.awt.Color(0, 0, 0));
        btnSDLast.setText("LAST");
        btnSDLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSDLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSDLastMouseExited(evt);
            }
        });
        btnSDLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSDLastActionPerformed(evt);
            }
        });
        jPanel6.add(btnSDLast);
        btnSDLast.setBounds(1070, 450, 110, 32);

        lstSDelete.setForeground(new java.awt.Color(0, 0, 0));
        lstSDelete.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstSDeleteValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(lstSDelete);

        jPanel6.add(jScrollPane3);
        jScrollPane3.setBounds(940, 80, 240, 300);

        btnSDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnSDelete.setForeground(new java.awt.Color(0, 0, 0));
        btnSDelete.setText("DELETE STAFF ACCOUNT");
        btnSDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSDeleteMouseExited(evt);
            }
        });
        btnSDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSDeleteActionPerformed(evt);
            }
        });
        jPanel6.add(btnSDelete);
        btnSDelete.setBounds(600, 510, 210, 32);

        jLabel68.setForeground(new java.awt.Color(0, 0, 0));
        jLabel68.setText("Gender:");
        jPanel6.add(jLabel68);
        jLabel68.setBounds(100, 300, 44, 16);

        jLabel72.setForeground(new java.awt.Color(0, 0, 0));
        jLabel72.setText("Start Date:");
        jPanel6.add(jLabel72);
        jLabel72.setBounds(530, 160, 70, 16);

        lblSDName.setForeground(new java.awt.Color(0, 0, 0));
        lblSDName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDName);
        lblSDName.setBounds(250, 89, 180, 18);

        lblSDSurname.setForeground(new java.awt.Color(0, 0, 0));
        lblSDSurname.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDSurname);
        lblSDSurname.setBounds(250, 159, 180, 18);

        lblSDDOB.setForeground(new java.awt.Color(0, 0, 0));
        lblSDDOB.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDDOB);
        lblSDDOB.setBounds(250, 229, 180, 18);

        lblSDGender.setForeground(new java.awt.Color(0, 0, 0));
        lblSDGender.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDGender);
        lblSDGender.setBounds(250, 299, 180, 18);

        lblSDPhysical.setForeground(new java.awt.Color(0, 0, 0));
        lblSDPhysical.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDPhysical);
        lblSDPhysical.setBounds(250, 369, 180, 18);

        lblSDNumber.setForeground(new java.awt.Color(0, 0, 0));
        lblSDNumber.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDNumber);
        lblSDNumber.setBounds(250, 439, 180, 18);

        lblSDEmail.setForeground(new java.awt.Color(0, 0, 0));
        lblSDEmail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDEmail);
        lblSDEmail.setBounds(700, 89, 180, 18);

        lblSDStartDate.setForeground(new java.awt.Color(0, 0, 0));
        lblSDStartDate.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(lblSDStartDate);
        lblSDStartDate.setBounds(700, 159, 180, 18);

        jTabbedPane2.addTab("DELETE", jPanel6);

        jPanel1.add(jTabbedPane2);
        jTabbedPane2.setBounds(0, 110, 1280, 590);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("STAFF");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(565, 40, 126, 52);

        jTabbedPane1.addTab("STAFF", jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jTabbedPane3.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane3.setForeground(new java.awt.Color(0, 0, 0));
        jTabbedPane3.setToolTipText("");

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(null);

        jLabel30.setForeground(new java.awt.Color(0, 0, 0));
        jLabel30.setText("Title:");
        jPanel7.add(jLabel30);
        jLabel30.setBounds(100, 90, 50, 16);

        jLabel31.setForeground(new java.awt.Color(0, 0, 0));
        jLabel31.setText("Synopsis:");
        jPanel7.add(jLabel31);
        jLabel31.setBounds(100, 160, 60, 16);

        jLabel32.setForeground(new java.awt.Color(0, 0, 0));
        jLabel32.setText("RunTime:");
        jPanel7.add(jLabel32);
        jLabel32.setBounds(100, 230, 60, 16);

        jLabel33.setForeground(new java.awt.Color(0, 0, 0));
        jLabel33.setText("Rated:");
        jPanel7.add(jLabel33);
        jLabel33.setBounds(100, 300, 40, 16);

        jLabel34.setForeground(new java.awt.Color(0, 0, 0));
        jLabel34.setText("Producer:");
        jPanel7.add(jLabel34);
        jLabel34.setBounds(100, 370, 60, 16);

        jLabel35.setForeground(new java.awt.Color(0, 0, 0));
        jLabel35.setText("Director:");
        jPanel7.add(jLabel35);
        jLabel35.setBounds(100, 440, 60, 16);

        txfMCTitle.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCTitle);
        txfMCTitle.setBounds(250, 85, 180, 25);

        txfMCSynopsis.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCSynopsis);
        txfMCSynopsis.setBounds(250, 155, 180, 25);

        txfMCRunTime.setForeground(new java.awt.Color(0, 0, 0));
        txfMCRunTime.setToolTipText("");
        jPanel7.add(txfMCRunTime);
        txfMCRunTime.setBounds(250, 225, 180, 25);

        txfMCProducer.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCProducer);
        txfMCProducer.setBounds(250, 365, 180, 25);

        txfMCDirector.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCDirector);
        txfMCDirector.setBounds(250, 435, 180, 25);

        jLabel36.setForeground(new java.awt.Color(0, 0, 0));
        jLabel36.setText("Release Date:");
        jPanel7.add(jLabel36);
        jLabel36.setBounds(530, 90, 90, 16);

        jLabel37.setForeground(new java.awt.Color(0, 0, 0));
        jLabel37.setText("Rental Price:");
        jPanel7.add(jLabel37);
        jLabel37.setBounds(530, 160, 80, 16);

        jLabel38.setForeground(new java.awt.Color(0, 0, 0));
        jLabel38.setText("Number of Copies:");
        jPanel7.add(jLabel38);
        jLabel38.setBounds(530, 230, 110, 16);

        ftxfMCRelease.setForeground(new java.awt.Color(0, 0, 0));
        ftxfMCRelease.setToolTipText("YYYY");
        jPanel7.add(ftxfMCRelease);
        ftxfMCRelease.setBounds(700, 85, 180, 25);

        txfMCPrice.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCPrice);
        txfMCPrice.setBounds(700, 155, 180, 25);

        txfMCCopies.setForeground(new java.awt.Color(0, 0, 0));
        jPanel7.add(txfMCCopies);
        txfMCCopies.setBounds(700, 225, 180, 25);

        btnMCreate.setBackground(new java.awt.Color(255, 255, 255));
        btnMCreate.setForeground(new java.awt.Color(0, 0, 0));
        btnMCreate.setText("CREATE NEW MOVIE ENTRY");
        btnMCreate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMCreateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMCreateMouseExited(evt);
            }
        });
        btnMCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMCreateActionPerformed(evt);
            }
        });
        jPanel7.add(btnMCreate);
        btnMCreate.setBounds(980, 450, 210, 32);

        cmbMCRated.setBackground(new java.awt.Color(255, 255, 255));
        cmbMCRated.setForeground(new java.awt.Color(0, 0, 0));
        cmbMCRated.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "G [General Audiences]", "PG [Parental Guidance Suggested]", "PG-13 [Parents Strongly Cautioned]", "R [Restricted]", "NC-17 [Adults Only]", "UR [Unrated]", "This Film Is Not Yet Rated" }));
        jPanel7.add(cmbMCRated);
        cmbMCRated.setBounds(250, 295, 180, 26);

        jTabbedPane3.addTab("CREATE", jPanel7);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(null);

        jLabel39.setForeground(new java.awt.Color(0, 0, 0));
        jLabel39.setText("Title:");
        jPanel8.add(jLabel39);
        jLabel39.setBounds(100, 90, 50, 16);

        jLabel40.setForeground(new java.awt.Color(0, 0, 0));
        jLabel40.setText("Synopsis:");
        jPanel8.add(jLabel40);
        jLabel40.setBounds(100, 160, 60, 16);

        jLabel41.setForeground(new java.awt.Color(0, 0, 0));
        jLabel41.setText("RunTime:");
        jPanel8.add(jLabel41);
        jLabel41.setBounds(100, 230, 60, 16);

        jLabel42.setForeground(new java.awt.Color(0, 0, 0));
        jLabel42.setText("Rated:");
        jPanel8.add(jLabel42);
        jLabel42.setBounds(100, 300, 40, 16);

        jLabel43.setForeground(new java.awt.Color(0, 0, 0));
        jLabel43.setText("Producer:");
        jPanel8.add(jLabel43);
        jLabel43.setBounds(100, 370, 60, 16);

        jLabel44.setForeground(new java.awt.Color(0, 0, 0));
        jLabel44.setText("Director:");
        jPanel8.add(jLabel44);
        jLabel44.setBounds(100, 440, 60, 16);

        lblMRTitle.setForeground(new java.awt.Color(0, 0, 0));
        lblMRTitle.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRTitle);
        lblMRTitle.setBounds(250, 89, 180, 18);

        lblMRSynopsis.setForeground(new java.awt.Color(0, 0, 0));
        lblMRSynopsis.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRSynopsis);
        lblMRSynopsis.setBounds(250, 159, 180, 18);

        lblMRRunTime.setForeground(new java.awt.Color(0, 0, 0));
        lblMRRunTime.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRRunTime);
        lblMRRunTime.setBounds(250, 229, 180, 18);

        lblMRRated.setForeground(new java.awt.Color(0, 0, 0));
        lblMRRated.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRRated);
        lblMRRated.setBounds(250, 299, 180, 18);

        lblMRProducer.setForeground(new java.awt.Color(0, 0, 0));
        lblMRProducer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRProducer);
        lblMRProducer.setBounds(250, 369, 180, 18);

        lblMRDirector.setForeground(new java.awt.Color(0, 0, 0));
        lblMRDirector.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRDirector);
        lblMRDirector.setBounds(250, 439, 180, 18);

        jLabel45.setForeground(new java.awt.Color(0, 0, 0));
        jLabel45.setText("Release Date:");
        jPanel8.add(jLabel45);
        jLabel45.setBounds(530, 90, 80, 16);

        jLabel46.setForeground(new java.awt.Color(0, 0, 0));
        jLabel46.setText("Rental Price:");
        jPanel8.add(jLabel46);
        jLabel46.setBounds(530, 160, 80, 16);

        jLabel47.setForeground(new java.awt.Color(0, 0, 0));
        jLabel47.setText("Number of Copies:");
        jPanel8.add(jLabel47);
        jLabel47.setBounds(530, 230, 110, 16);

        lblMRRelease.setForeground(new java.awt.Color(0, 0, 0));
        lblMRRelease.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRRelease);
        lblMRRelease.setBounds(700, 89, 180, 18);

        lblMRPrice.setForeground(new java.awt.Color(0, 0, 0));
        lblMRPrice.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRPrice);
        lblMRPrice.setBounds(700, 159, 180, 18);

        lblMRCopies.setForeground(new java.awt.Color(0, 0, 0));
        lblMRCopies.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.add(lblMRCopies);
        lblMRCopies.setBounds(700, 229, 180, 18);

        lstMRead.setForeground(new java.awt.Color(0, 0, 0));
        lstMRead.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstMReadValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(lstMRead);

        jPanel8.add(jScrollPane4);
        jScrollPane4.setBounds(940, 80, 240, 300);

        btnMRFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnMRFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnMRFirst.setText("FIRST");
        btnMRFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMRFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMRFirstMouseExited(evt);
            }
        });
        btnMRFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMRFirstActionPerformed(evt);
            }
        });
        jPanel8.add(btnMRFirst);
        btnMRFirst.setBounds(940, 400, 110, 32);

        btnMRPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnMRPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnMRPrevious.setText("PREVIOUS");
        btnMRPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMRPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMRPreviousMouseExited(evt);
            }
        });
        btnMRPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMRPreviousActionPerformed(evt);
            }
        });
        jPanel8.add(btnMRPrevious);
        btnMRPrevious.setBounds(1070, 400, 110, 32);

        btnMRNext.setBackground(new java.awt.Color(255, 255, 255));
        btnMRNext.setForeground(new java.awt.Color(0, 0, 0));
        btnMRNext.setText("NEXT");
        btnMRNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMRNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMRNextMouseExited(evt);
            }
        });
        btnMRNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMRNextActionPerformed(evt);
            }
        });
        jPanel8.add(btnMRNext);
        btnMRNext.setBounds(940, 450, 110, 32);

        btnMRLast.setBackground(new java.awt.Color(255, 255, 255));
        btnMRLast.setForeground(new java.awt.Color(0, 0, 0));
        btnMRLast.setText("LAST");
        btnMRLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMRLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMRLastMouseExited(evt);
            }
        });
        btnMRLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMRLastActionPerformed(evt);
            }
        });
        jPanel8.add(btnMRLast);
        btnMRLast.setBounds(1070, 450, 110, 32);

        jTabbedPane3.addTab("READ", jPanel8);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(null);

        jLabel48.setForeground(new java.awt.Color(0, 0, 0));
        jLabel48.setText("Title:");
        jPanel9.add(jLabel48);
        jLabel48.setBounds(100, 90, 50, 16);

        jLabel49.setForeground(new java.awt.Color(0, 0, 0));
        jLabel49.setText("Synopsis:");
        jPanel9.add(jLabel49);
        jLabel49.setBounds(100, 160, 60, 16);

        jLabel50.setForeground(new java.awt.Color(0, 0, 0));
        jLabel50.setText("RunTime:");
        jPanel9.add(jLabel50);
        jLabel50.setBounds(100, 230, 60, 16);

        jLabel51.setForeground(new java.awt.Color(0, 0, 0));
        jLabel51.setText("Rated:");
        jPanel9.add(jLabel51);
        jLabel51.setBounds(100, 300, 40, 16);

        jLabel52.setForeground(new java.awt.Color(0, 0, 0));
        jLabel52.setText("Producer:");
        jPanel9.add(jLabel52);
        jLabel52.setBounds(100, 370, 60, 16);

        jLabel53.setForeground(new java.awt.Color(0, 0, 0));
        jLabel53.setText("Director:");
        jPanel9.add(jLabel53);
        jLabel53.setBounds(100, 440, 60, 16);

        jLabel54.setForeground(new java.awt.Color(0, 0, 0));
        jLabel54.setText("Release Date:");
        jPanel9.add(jLabel54);
        jLabel54.setBounds(530, 90, 80, 16);

        jLabel55.setForeground(new java.awt.Color(0, 0, 0));
        jLabel55.setText("Rental Price:");
        jPanel9.add(jLabel55);
        jLabel55.setBounds(530, 160, 80, 16);

        jLabel56.setForeground(new java.awt.Color(0, 0, 0));
        jLabel56.setText("Number of Copies:");
        jPanel9.add(jLabel56);
        jLabel56.setBounds(530, 230, 110, 16);

        txfMUTitle.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUTitle);
        txfMUTitle.setBounds(250, 85, 180, 25);

        txfMUSynopsis.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUSynopsis);
        txfMUSynopsis.setBounds(250, 155, 180, 25);

        txfMURunTime.setForeground(new java.awt.Color(0, 0, 0));
        txfMURunTime.setToolTipText("");
        jPanel9.add(txfMURunTime);
        txfMURunTime.setBounds(250, 225, 180, 25);

        txfMUProducer.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUProducer);
        txfMUProducer.setBounds(250, 365, 180, 25);

        txfMUDirector.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUDirector);
        txfMUDirector.setBounds(250, 435, 180, 25);

        ftxfMURelease.setForeground(new java.awt.Color(0, 0, 0));
        ftxfMURelease.setToolTipText("YYYY");
        jPanel9.add(ftxfMURelease);
        ftxfMURelease.setBounds(700, 85, 180, 25);

        txfMUPrice.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUPrice);
        txfMUPrice.setBounds(700, 155, 180, 25);

        txfMUCopies.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(txfMUCopies);
        txfMUCopies.setBounds(700, 225, 180, 25);

        lstMUpdate.setForeground(new java.awt.Color(0, 0, 0));
        lstMUpdate.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstMUpdateValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(lstMUpdate);

        jPanel9.add(jScrollPane5);
        jScrollPane5.setBounds(940, 80, 240, 300);

        btnMUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnMUpdate.setForeground(new java.awt.Color(0, 0, 0));
        btnMUpdate.setText("UPDATE MOVIE ENTRY");
        btnMUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMUpdateMouseExited(evt);
            }
        });
        btnMUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMUpdateActionPerformed(evt);
            }
        });
        jPanel9.add(btnMUpdate);
        btnMUpdate.setBounds(600, 510, 210, 32);

        btnMUFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnMUFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnMUFirst.setText("FIRST");
        btnMUFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMUFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMUFirstMouseExited(evt);
            }
        });
        btnMUFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMUFirstActionPerformed(evt);
            }
        });
        jPanel9.add(btnMUFirst);
        btnMUFirst.setBounds(940, 400, 110, 32);

        btnMUPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnMUPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnMUPrevious.setText("PREVIOUS");
        btnMUPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMUPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMUPreviousMouseExited(evt);
            }
        });
        btnMUPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMUPreviousActionPerformed(evt);
            }
        });
        jPanel9.add(btnMUPrevious);
        btnMUPrevious.setBounds(1070, 400, 110, 32);

        btnMUNext.setBackground(new java.awt.Color(255, 255, 255));
        btnMUNext.setForeground(new java.awt.Color(0, 0, 0));
        btnMUNext.setText("NEXT");
        btnMUNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMUNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMUNextMouseExited(evt);
            }
        });
        btnMUNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMUNextActionPerformed(evt);
            }
        });
        jPanel9.add(btnMUNext);
        btnMUNext.setBounds(940, 450, 110, 32);

        btnMULast.setBackground(new java.awt.Color(255, 255, 255));
        btnMULast.setForeground(new java.awt.Color(0, 0, 0));
        btnMULast.setText("LAST");
        btnMULast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMULastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMULastMouseExited(evt);
            }
        });
        btnMULast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMULastActionPerformed(evt);
            }
        });
        jPanel9.add(btnMULast);
        btnMULast.setBounds(1070, 450, 110, 32);

        cmbMURated.setBackground(new java.awt.Color(255, 255, 255));
        cmbMURated.setForeground(new java.awt.Color(0, 0, 0));
        cmbMURated.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "G [General Audiences]", "PG [Parental Guidance Suggested]", "PG-13 [Parents Strongly Cautioned]", "R [Restricted]", "NC-17 [Adults Only]", "UR [Unrated]", "This Film Is Not Yet Rated" }));
        jPanel9.add(cmbMURated);
        cmbMURated.setBounds(250, 295, 180, 26);

        jTabbedPane3.addTab("UPDATE", jPanel9);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setLayout(null);

        jLabel57.setForeground(new java.awt.Color(0, 0, 0));
        jLabel57.setText("Title:");
        jPanel10.add(jLabel57);
        jLabel57.setBounds(100, 90, 50, 16);

        jLabel58.setForeground(new java.awt.Color(0, 0, 0));
        jLabel58.setText("Synopsis:");
        jPanel10.add(jLabel58);
        jLabel58.setBounds(100, 160, 60, 16);

        jLabel59.setForeground(new java.awt.Color(0, 0, 0));
        jLabel59.setText("RunTime:");
        jPanel10.add(jLabel59);
        jLabel59.setBounds(100, 230, 60, 16);

        jLabel60.setForeground(new java.awt.Color(0, 0, 0));
        jLabel60.setText("Rated:");
        jPanel10.add(jLabel60);
        jLabel60.setBounds(100, 300, 40, 16);

        jLabel61.setForeground(new java.awt.Color(0, 0, 0));
        jLabel61.setText("Producer:");
        jPanel10.add(jLabel61);
        jLabel61.setBounds(100, 370, 60, 16);

        jLabel62.setForeground(new java.awt.Color(0, 0, 0));
        jLabel62.setText("Director:");
        jPanel10.add(jLabel62);
        jLabel62.setBounds(100, 440, 60, 16);

        jLabel63.setForeground(new java.awt.Color(0, 0, 0));
        jLabel63.setText("Release Date:");
        jPanel10.add(jLabel63);
        jLabel63.setBounds(530, 90, 80, 16);

        jLabel64.setForeground(new java.awt.Color(0, 0, 0));
        jLabel64.setText("Rental Price:");
        jPanel10.add(jLabel64);
        jLabel64.setBounds(530, 160, 80, 16);

        jLabel65.setForeground(new java.awt.Color(0, 0, 0));
        jLabel65.setText("Number of Copies:");
        jPanel10.add(jLabel65);
        jLabel65.setBounds(530, 230, 110, 16);

        lstMDelete.setForeground(new java.awt.Color(0, 0, 0));
        lstMDelete.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstMDeleteValueChanged(evt);
            }
        });
        jScrollPane6.setViewportView(lstMDelete);

        jPanel10.add(jScrollPane6);
        jScrollPane6.setBounds(940, 80, 240, 300);

        btnMDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnMDelete.setForeground(new java.awt.Color(0, 0, 0));
        btnMDelete.setText("DELETE MOVIE ENTRY");
        btnMDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMDeleteMouseExited(evt);
            }
        });
        btnMDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMDeleteActionPerformed(evt);
            }
        });
        jPanel10.add(btnMDelete);
        btnMDelete.setBounds(600, 510, 210, 32);

        btnMDFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnMDFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnMDFirst.setText("FIRST");
        btnMDFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMDFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMDFirstMouseExited(evt);
            }
        });
        btnMDFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMDFirstActionPerformed(evt);
            }
        });
        jPanel10.add(btnMDFirst);
        btnMDFirst.setBounds(940, 400, 110, 32);

        btnMDPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnMDPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnMDPrevious.setText("PREVIOUS");
        btnMDPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMDPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMDPreviousMouseExited(evt);
            }
        });
        btnMDPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMDPreviousActionPerformed(evt);
            }
        });
        jPanel10.add(btnMDPrevious);
        btnMDPrevious.setBounds(1070, 400, 110, 32);

        btnMDNext.setBackground(new java.awt.Color(255, 255, 255));
        btnMDNext.setForeground(new java.awt.Color(0, 0, 0));
        btnMDNext.setText("NEXT");
        btnMDNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMDNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMDNextMouseExited(evt);
            }
        });
        btnMDNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMDNextActionPerformed(evt);
            }
        });
        jPanel10.add(btnMDNext);
        btnMDNext.setBounds(940, 450, 110, 32);

        btnMDLast.setBackground(new java.awt.Color(255, 255, 255));
        btnMDLast.setForeground(new java.awt.Color(0, 0, 0));
        btnMDLast.setText("LAST");
        btnMDLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMDLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMDLastMouseExited(evt);
            }
        });
        btnMDLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMDLastActionPerformed(evt);
            }
        });
        jPanel10.add(btnMDLast);
        btnMDLast.setBounds(1070, 450, 110, 32);

        lblMDTitle.setForeground(new java.awt.Color(0, 0, 0));
        lblMDTitle.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDTitle);
        lblMDTitle.setBounds(250, 89, 180, 18);

        lblMDSynopsis.setForeground(new java.awt.Color(0, 0, 0));
        lblMDSynopsis.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDSynopsis);
        lblMDSynopsis.setBounds(250, 159, 180, 18);

        lblMDRunTime.setForeground(new java.awt.Color(0, 0, 0));
        lblMDRunTime.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDRunTime);
        lblMDRunTime.setBounds(250, 229, 180, 18);

        lblMDRated.setForeground(new java.awt.Color(0, 0, 0));
        lblMDRated.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDRated);
        lblMDRated.setBounds(250, 299, 180, 18);

        lblMDProducer.setForeground(new java.awt.Color(0, 0, 0));
        lblMDProducer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDProducer);
        lblMDProducer.setBounds(250, 369, 180, 18);

        lblMDDirector.setForeground(new java.awt.Color(0, 0, 0));
        lblMDDirector.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDDirector);
        lblMDDirector.setBounds(250, 439, 180, 18);

        lblMDRelease.setForeground(new java.awt.Color(0, 0, 0));
        lblMDRelease.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDRelease);
        lblMDRelease.setBounds(700, 89, 180, 18);

        lblMDPrice.setForeground(new java.awt.Color(0, 0, 0));
        lblMDPrice.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDPrice);
        lblMDPrice.setBounds(700, 159, 180, 18);

        lblMDCopies.setForeground(new java.awt.Color(0, 0, 0));
        lblMDCopies.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.add(lblMDCopies);
        lblMDCopies.setBounds(700, 229, 180, 18);

        jTabbedPane3.addTab("DELETE", jPanel10);

        jPanel2.add(jTabbedPane3);
        jTabbedPane3.setBounds(0, 110, 1280, 590);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("MOVIE");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(565, 40, 128, 52);

        jTabbedPane1.addTab("MOVIE", jPanel2);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(null);

        jLabel69.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel69.setForeground(new java.awt.Color(0, 0, 0));
        jLabel69.setText("DATABASE");
        jPanel12.add(jLabel69);
        jLabel69.setBounds(520, 40, 220, 52);

        jTabbedPane4.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane4.setForeground(new java.awt.Color(0, 0, 0));

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(null);

        jScrollPane7.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane7.setForeground(new java.awt.Color(0, 0, 0));

        tblStaff.setBackground(new java.awt.Color(255, 255, 255));
        tblStaff.setForeground(new java.awt.Color(0, 0, 0));
        tblStaff.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblStaff.setFillsViewportHeight(true);
        jScrollPane7.setViewportView(tblStaff);

        jPanel13.add(jScrollPane7);
        jScrollPane7.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("STAFF", jPanel13);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setLayout(null);

        jScrollPane11.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane11.setForeground(new java.awt.Color(0, 0, 0));

        tblSessions.setBackground(new java.awt.Color(255, 255, 255));
        tblSessions.setForeground(new java.awt.Color(0, 0, 0));
        tblSessions.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblSessions.setFillsViewportHeight(true);
        jScrollPane11.setViewportView(tblSessions);

        jPanel17.add(jScrollPane11);
        jScrollPane11.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("SESSIONS", jPanel17);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setLayout(null);

        jScrollPane8.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane8.setForeground(new java.awt.Color(0, 0, 0));

        tblMovies.setBackground(new java.awt.Color(255, 255, 255));
        tblMovies.setForeground(new java.awt.Color(0, 0, 0));
        tblMovies.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblMovies.setFillsViewportHeight(true);
        jScrollPane8.setViewportView(tblMovies);

        jPanel14.add(jScrollPane8);
        jScrollPane8.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("MOVIES", jPanel14);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setLayout(null);

        jScrollPane13.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane13.setForeground(new java.awt.Color(0, 0, 0));

        tblLevels.setBackground(new java.awt.Color(255, 255, 255));
        tblLevels.setForeground(new java.awt.Color(0, 0, 0));
        tblLevels.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblLevels.setFillsViewportHeight(true);
        jScrollPane13.setViewportView(tblLevels);

        jPanel19.add(jScrollPane13);
        jScrollPane13.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("MEMBERSHIP LEVELS", jPanel19);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setLayout(null);

        jScrollPane9.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane9.setForeground(new java.awt.Color(0, 0, 0));

        tblCustomers.setBackground(new java.awt.Color(255, 255, 255));
        tblCustomers.setForeground(new java.awt.Color(0, 0, 0));
        tblCustomers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblCustomers.setFillsViewportHeight(true);
        jScrollPane9.setViewportView(tblCustomers);

        jPanel15.add(jScrollPane9);
        jScrollPane9.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("CUSTOMERS", jPanel15);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setLayout(null);

        jScrollPane10.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane10.setForeground(new java.awt.Color(0, 0, 0));

        tblTransactions.setBackground(new java.awt.Color(255, 255, 255));
        tblTransactions.setForeground(new java.awt.Color(0, 0, 0));
        tblTransactions.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblTransactions.setFillsViewportHeight(true);
        jScrollPane10.setViewportView(tblTransactions);

        jPanel16.add(jScrollPane10);
        jScrollPane10.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("TRANSACTIONS", jPanel16);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setLayout(null);

        jScrollPane12.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane12.setForeground(new java.awt.Color(0, 0, 0));

        tblRentals.setBackground(new java.awt.Color(255, 255, 255));
        tblRentals.setForeground(new java.awt.Color(0, 0, 0));
        tblRentals.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblRentals.setFillsViewportHeight(true);
        jScrollPane12.setViewportView(tblRentals);

        jPanel18.add(jScrollPane12);
        jScrollPane12.setBounds(25, 20, 1220, 520);

        jTabbedPane4.addTab("RENTALS PER TRANSACTION", jPanel18);

        jPanel12.add(jTabbedPane4);
        jTabbedPane4.setBounds(0, 110, 1280, 590);

        jTabbedPane1.addTab("DATABASE", jPanel12);

        jPanel11.add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 120, 1280, 720);

        btnStore.setBackground(new java.awt.Color(255, 255, 255));
        btnStore.setForeground(new java.awt.Color(0, 0, 0));
        btnStore.setText("GO TO STORE");
        btnStore.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnStoreMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnStoreMouseExited(evt);
            }
        });
        btnStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStoreActionPerformed(evt);
            }
        });
        jPanel11.add(btnStore);
        btnStore.setBounds(1070, 880, 130, 32);

        getContentPane().add(jPanel11);
        jPanel11.setBounds(0, 0, 1280, 950);

        jMenuBar1.setBackground(new java.awt.Color(255, 255, 255));

        jMenu1.setBackground(new java.awt.Color(255, 255, 255));
        jMenu1.setForeground(new java.awt.Color(0, 0, 0));
        jMenu1.setText("File");

        mnuItemExit.setBackground(new java.awt.Color(255, 255, 255));
        mnuItemExit.setForeground(new java.awt.Color(0, 0, 0));
        mnuItemExit.setText("Exit");
        mnuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemExitActionPerformed(evt);
            }
        });
        jMenu1.add(mnuItemExit);

        jMenuBar1.add(jMenu1);

        mnuLogOut.setBackground(new java.awt.Color(255, 255, 255));
        mnuLogOut.setForeground(new java.awt.Color(0, 0, 0));
        mnuLogOut.setText("Log out");
        mnuLogOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnuLogOutMouseClicked(evt);
            }
        });
        jMenuBar1.add(mnuLogOut);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(1280, 999));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnStoreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStoreMouseEntered
        btnStore.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnStoreMouseEntered

    private void btnStoreMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStoreMouseExited
        btnStore.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnStoreMouseExited

    private void btnSCreateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSCreateMouseEntered
        btnSCreate.setForeground(Color.GREEN);
    }//GEN-LAST:event_btnSCreateMouseEntered

    private void btnSCreateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSCreateMouseExited
        btnSCreate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSCreateMouseExited

    private void btnSUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUpdateMouseEntered
        btnSUpdate.setForeground(Color.YELLOW);
    }//GEN-LAST:event_btnSUpdateMouseEntered

    private void btnSUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUpdateMouseExited
        btnSUpdate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSUpdateMouseExited

    private void btnSDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDeleteMouseEntered
        btnSDelete.setForeground(Color.RED);
    }//GEN-LAST:event_btnSDeleteMouseEntered

    private void btnSDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDeleteMouseExited
        btnSDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSDeleteMouseExited

    private void btnSRFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRFirstMouseEntered
        btnSRFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSRFirstMouseEntered

    private void btnSRFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRFirstMouseExited
        btnSRFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSRFirstMouseExited

    private void btnSRPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRPreviousMouseEntered
        btnSRPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSRPreviousMouseEntered

    private void btnSRPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRPreviousMouseExited
        btnSRPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSRPreviousMouseExited

    private void btnSRNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRNextMouseEntered
        btnSRNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSRNextMouseEntered

    private void btnSRNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRNextMouseExited
        btnSRNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSRNextMouseExited

    private void btnSRLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRLastMouseEntered
        btnSRLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSRLastMouseEntered

    private void btnSRLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSRLastMouseExited
        btnSRLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSRLastMouseExited

    private void btnSUFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUFirstMouseEntered
        btnSUFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSUFirstMouseEntered

    private void btnSUFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUFirstMouseExited
        btnSUFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSUFirstMouseExited

    private void btnSUPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUPreviousMouseEntered
        btnSUPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSUPreviousMouseEntered

    private void btnSUPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUPreviousMouseExited
        btnSUPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSUPreviousMouseExited

    private void btnSUNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUNextMouseEntered
        btnSUNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSUNextMouseEntered

    private void btnSUNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSUNextMouseExited
        btnSUNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSUNextMouseExited

    private void btnSULastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSULastMouseEntered
        btnSULast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSULastMouseEntered

    private void btnSULastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSULastMouseExited
        btnSULast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSULastMouseExited

    private void btnSDFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDFirstMouseEntered
        btnSDFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSDFirstMouseEntered

    private void btnSDFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDFirstMouseExited
        btnSDFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSDFirstMouseExited

    private void btnSDPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDPreviousMouseEntered
        btnSDPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSDPreviousMouseEntered

    private void btnSDPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDPreviousMouseExited
        btnSDPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSDPreviousMouseExited

    private void btnSDNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDNextMouseEntered
        btnSDNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSDNextMouseEntered

    private void btnSDNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDNextMouseExited
        btnSDNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSDNextMouseExited

    private void btnSDLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDLastMouseEntered
        btnSDLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnSDLastMouseEntered

    private void btnSDLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSDLastMouseExited
        btnSDLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnSDLastMouseExited

    private void btnMCreateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMCreateMouseEntered
        btnMCreate.setForeground(Color.GREEN);
    }//GEN-LAST:event_btnMCreateMouseEntered

    private void btnMCreateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMCreateMouseExited
        btnMCreate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMCreateMouseExited

    private void btnMUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUpdateMouseEntered
        btnMUpdate.setForeground(Color.YELLOW);
    }//GEN-LAST:event_btnMUpdateMouseEntered

    private void btnMUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUpdateMouseExited
        btnMUpdate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMUpdateMouseExited

    private void btnMDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDeleteMouseEntered
        btnMDelete.setForeground(Color.RED);
    }//GEN-LAST:event_btnMDeleteMouseEntered

    private void btnMDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDeleteMouseExited
        btnMDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMDeleteMouseExited

    private void btnMRFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRFirstMouseEntered
        btnMRFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMRFirstMouseEntered

    private void btnMRFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRFirstMouseExited
        btnMRFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMRFirstMouseExited

    private void btnMRPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRPreviousMouseEntered
        btnMRPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMRPreviousMouseEntered

    private void btnMRPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRPreviousMouseExited
        btnMRPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMRPreviousMouseExited

    private void btnMRNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRNextMouseEntered
        btnMRNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMRNextMouseEntered

    private void btnMRNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRNextMouseExited
        btnMRNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMRNextMouseExited

    private void btnMRLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRLastMouseEntered
        btnMRLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMRLastMouseEntered

    private void btnMRLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMRLastMouseExited
        btnMRLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMRLastMouseExited

    private void btnMUFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUFirstMouseEntered
        btnMUFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMUFirstMouseEntered

    private void btnMUFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUFirstMouseExited
        btnMUFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMUFirstMouseExited

    private void btnMUPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUPreviousMouseEntered
        btnMUPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMUPreviousMouseEntered

    private void btnMUPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUPreviousMouseExited
        btnMUPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMUPreviousMouseExited

    private void btnMUNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUNextMouseEntered
        btnMUNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMUNextMouseEntered

    private void btnMUNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMUNextMouseExited
        btnMUNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMUNextMouseExited

    private void btnMULastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMULastMouseEntered
        btnMULast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMULastMouseEntered

    private void btnMULastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMULastMouseExited
        btnMULast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMULastMouseExited

    private void btnMDFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDFirstMouseEntered
        btnMDFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMDFirstMouseEntered

    private void btnMDFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDFirstMouseExited
        btnMDFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMDFirstMouseExited

    private void btnMDPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDPreviousMouseEntered
        btnMDPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMDPreviousMouseEntered

    private void btnMDPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDPreviousMouseExited
        btnMDPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMDPreviousMouseExited

    private void btnMDNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDNextMouseEntered
        btnMDNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMDNextMouseEntered

    private void btnMDNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDNextMouseExited
        btnMDNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMDNextMouseExited

    private void btnMDLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDLastMouseEntered
        btnMDLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnMDLastMouseEntered

    private void btnMDLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMDLastMouseExited
        btnMDLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnMDLastMouseExited

    private void btnSCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSCreateActionPerformed
        //Extracting data from the components
        String name = txfSCName.getText(),
            surname = txfSCSurname.getText().toLowerCase(),
                DOB = ftxfSCDOB.getText(),
             gender = cmbSCGender.getSelectedItem().toString(),
           physical = txfSCPhysical.getText(),
             number = txfSCNumber.getText(),
              email = ftxfSCEmail.getText(),
           password = "",
            confirm = "";
        
        char passwordArr[] = pwfSCPassword.getPassword(),
              confirmArr[] = pwfSCConfirm.getPassword();
        
        //Iterating through the char arrays to represent the passwords as Strings
        for (char c : passwordArr) password += c;
        for (char c : confirmArr) confirm += c;
        
        /**
         * Call the staff method and pass
         * all the data for error handling
         * and database insertion.
         * If it is successful,
         * the UI will update
         */
        if (f.staff(name, surname, DOB, gender, physical, number, email, password, confirm, 0, -1) == 0)
        {
            bindStaffCRUD();
            bindtblStaff();
        }
    }//GEN-LAST:event_btnSCreateActionPerformed

    private void btnSUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSUpdateActionPerformed
        //Extracting data from the components
        String name = txfSUName.getText(),
            surname = txfSUSurname.getText().toLowerCase(),
                DOB = ftxfSUDOB.getText(),
             gender = cmbSUGender.getSelectedItem().toString(),
           physical = txfSUPhysical.getText(),
             number = txfSUNumber.getText(),
              email = ftxfSUEmail.getText();
        
        //Get the selected ID from the list
        String staffID = lstSUpdate.getSelectedIndex() >= 0 ? lstSUpdate.getSelectedValue() : null;
        staffID = staffID != null ? staffID.substring(0, staffID.indexOf(" - ")) : null;
        int updateID = staffID != null ? Integer.parseInt(staffID) : -1;
        
        if (updateID != -1)
        {
            /**
             * Call the staff method and pass
             * all the data for error handling
             * and to update the database.
             * If it is successful,
             * the UI will update
             */
            if (f.staff(name, surname, DOB, gender, physical, number, email, "", "", updateID, -2) == 0)
            {
                bindStaffCRUD();
                bindtblStaff();
            }
        }
    }//GEN-LAST:event_btnSUpdateActionPerformed

    private void btnSDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSDeleteActionPerformed
        //Get the selected ID from the list
        String staffID = lstSDelete.getSelectedIndex() >= 0 ? lstSDelete.getSelectedValue() : null;
        staffID = staffID != null ? staffID.substring(0, staffID.indexOf(" - ")) : null;
        int deleteID = staffID != null ? Integer.parseInt(staffID) : -1;
        
        if (deleteID != -1)
        {
            /**
             * Call the delete method and pass
             * the ID and operation type.
             * If it is successful,
             * the UI will update
             */
            if (f.delete(deleteID, -3) == 0)
            {
                bindStaffCRUD();
                bindtblStaff();
            }
        }
    }//GEN-LAST:event_btnSDeleteActionPerformed

    private void btnMCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMCreateActionPerformed
        String title = txfMCTitle.getText(),
            synopsis = txfMCSynopsis.getText(),
             runtime = txfMCRunTime.getText(),
               rated = cmbMCRated.getSelectedItem().toString(),
            producer = txfMCProducer.getText(),
            director = txfMCDirector.getText(),
         releaseDate = ftxfMCRelease.getText(),
         rentalPrice = txfMCPrice.getText(),
          noOfCopies = txfMCCopies.getText();
        
        /**
         * Call the movie method and pass
         * all the data for error handling
         * and database insertion.
         * If it is successful,
         * the UI will update
         */
        if (f.movie(title, synopsis, runtime, rated, producer, director, releaseDate, rentalPrice, noOfCopies, 0, 11) == 0)
        {
            bindMovieCRUD();
            bindtblMovies();
        }
    }//GEN-LAST:event_btnMCreateActionPerformed

    private void btnMUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMUpdateActionPerformed
        String title = txfMUTitle.getText(),
            synopsis = txfMUSynopsis.getText(),
             runtime = txfMURunTime.getText(),
               rated = cmbMURated.getSelectedItem().toString(),
            producer = txfMUProducer.getText(),
            director = txfMUDirector.getText(),
         releaseDate = ftxfMURelease.getText(),
         rentalPrice = txfMUPrice.getText(),
          noOfCopies = txfMUCopies.getText();
        
        //Get the selected ID from the list
        String movieID = lstMUpdate.getSelectedIndex() >= 0 ? lstMUpdate.getSelectedValue() : null;
        movieID = movieID != null ? movieID.substring(0, movieID.indexOf(" - ")) : null;
        int updateID = movieID != null ? Integer.parseInt(movieID) : -1;
        
        if (updateID != -1)
        {
            /**
             * Call the movie method and pass
             * all the data for error handling
             * and to update the database.
             * If it is successful,
             * the UI will update
             */
            if (f.movie(title, synopsis, runtime, rated, producer, director, releaseDate, rentalPrice, noOfCopies, updateID, 12) == 0)
            {
                bindMovieCRUD();
                bindtblMovies();
            }
        }
    }//GEN-LAST:event_btnMUpdateActionPerformed

    private void btnMDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMDeleteActionPerformed
        //Get the selected ID from the list
        String movieID = lstMDelete.getSelectedIndex() >= 0 ? lstMDelete.getSelectedValue() : null;
        movieID = movieID != null ? movieID.substring(0, movieID.indexOf(" - ")) : null;
        int deleteID = movieID != null ? Integer.parseInt(movieID) : -1;
        
        if (deleteID != -1)
        {
            /**
             * Call the delete method and pass
             * the ID and operation type.
             * If it is successful,
             * the UI will update
             */
            if (f.delete(deleteID, 13) == 0)
            {
                bindMovieCRUD();
                bindtblMovies();
            }
        }
    }//GEN-LAST:event_btnMDeleteActionPerformed

    private void btnStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStoreActionPerformed
        this.setVisible(false);
        new Store().setVisible(true);
    }//GEN-LAST:event_btnStoreActionPerformed

    private void mnuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemExitActionPerformed
        if (f.logout(true) == 0) { /*System will exit*/ }
    }//GEN-LAST:event_mnuItemExitActionPerformed

    private void mnuLogOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuLogOutMouseClicked
        if (f.logout(false) == 0) this.dispose();
    }//GEN-LAST:event_mnuLogOutMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (f.logout(true) == 0) { /*System will exit*/ }
    }//GEN-LAST:event_formWindowClosing

    private void btnSRFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSRFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstSRead.setSelectedIndex(0);
    }//GEN-LAST:event_btnSRFirstActionPerformed

    private void btnSRPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSRPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstSRead.getSelectedIndex();
        if (idx > 0) lstSRead.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnSRPreviousActionPerformed

    private void btnSRNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSRNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstSRead.getSelectedIndex();
        if (idx != lstSRead.getModel().getSize() - 1) lstSRead.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnSRNextActionPerformed

    private void btnSRLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSRLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstSRead.setSelectedIndex(lstSRead.getModel().getSize() - 1);
    }//GEN-LAST:event_btnSRLastActionPerformed

    private void btnSUFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSUFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstSUpdate.setSelectedIndex(0);
    }//GEN-LAST:event_btnSUFirstActionPerformed

    private void btnSUPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSUPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstSUpdate.getSelectedIndex();
        if (idx > 0) lstSUpdate.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnSUPreviousActionPerformed

    private void btnSUNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSUNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstSUpdate.getSelectedIndex();
        if (idx != lstSUpdate.getModel().getSize() - 1) lstSUpdate.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnSUNextActionPerformed

    private void btnSULastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSULastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstSUpdate.setSelectedIndex(lstSUpdate.getModel().getSize() - 1);
    }//GEN-LAST:event_btnSULastActionPerformed

    private void btnSDFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSDFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstSDelete.setSelectedIndex(0);
    }//GEN-LAST:event_btnSDFirstActionPerformed

    private void btnSDPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSDPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstSDelete.getSelectedIndex();
        if (idx > 0) lstSDelete.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnSDPreviousActionPerformed

    private void btnSDNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSDNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstSDelete.getSelectedIndex();
        if (idx != lstSDelete.getModel().getSize() - 1) lstSDelete.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnSDNextActionPerformed

    private void btnSDLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSDLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstSDelete.setSelectedIndex(lstSDelete.getModel().getSize() - 1);
    }//GEN-LAST:event_btnSDLastActionPerformed

    private void lstSReadValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstSReadValueChanged
        //Get the selected ID from the list
        String readID = lstSRead.getSelectedIndex() >= 0 ? lstSRead.getSelectedValue() : null;
        readID = readID != null ? readID.substring(0, readID.indexOf(" - ")) : null;
        int staffID = readID != null ? Integer.parseInt(readID) : -1;
        
        if (staffID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the read labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL, START_DATE " +
                    "FROM staff " +
                    "WHERE STAFF_ID = ?"
                );
                ps.setInt(1, staffID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblSRName.setText((String) rs.getObject(1));
                    lblSRSurname.setText((String) rs.getObject(2));
                    lblSRDOB.setText((String) rs.getObject(3));
                    lblSRGender.setText((String) rs.getObject(4));
                    lblSRPhysical.setText((String) rs.getObject(5));
                    lblSRNumber.setText((String) rs.getObject(6));
                    lblSREmail.setText((String) rs.getObject(7));
                    lblSRStartDate.setText((String) rs.getObject(8));
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstSReadValueChanged

    private void lstSUpdateValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstSUpdateValueChanged
        //Get the selected ID from the list
        String updateID = lstSUpdate.getSelectedIndex() >= 0 ? lstSUpdate.getSelectedValue() : null;
        updateID = updateID != null ? updateID.substring(0, updateID.indexOf(" - ")) : null;
        int staffID = updateID != null ? Integer.parseInt(updateID) : -1;
        
        if (staffID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the update text fields with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL " +
                    "FROM staff " +
                    "WHERE STAFF_ID = ?"
                );
                ps.setInt(1, staffID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    txfSUName.setText((String) rs.getObject(1));
                    txfSUSurname.setText((String) rs.getObject(2));
                    ftxfSUDOB.setText((String) rs.getObject(3));
                    cmbSUGender.setSelectedItem((String) rs.getObject(4));
                    txfSUPhysical.setText((String) rs.getObject(5));
                    txfSUNumber.setText((String) rs.getObject(6));
                    ftxfSUEmail.setText((String) rs.getObject(7));
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstSUpdateValueChanged

    private void lstSDeleteValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstSDeleteValueChanged
        //Get the selected ID from the list
        String deleteID = lstSDelete.getSelectedIndex() >= 0 ? lstSDelete.getSelectedValue() : null;
        deleteID = deleteID != null ? deleteID.substring(0, deleteID.indexOf(" - ")) : null;
        int staffID = deleteID != null ? Integer.parseInt(deleteID) : -1;
        
        if (staffID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the delete labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL, START_DATE " +
                    "FROM staff " +
                    "WHERE STAFF_ID = ?"
                );
                ps.setInt(1, staffID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblSDName.setText((String) rs.getObject(1));
                    lblSDSurname.setText((String) rs.getObject(2));
                    lblSDDOB.setText((String) rs.getObject(3));
                    lblSDGender.setText((String) rs.getObject(4));
                    lblSDPhysical.setText((String) rs.getObject(5));
                    lblSDNumber.setText((String) rs.getObject(6));
                    lblSDEmail.setText((String) rs.getObject(7));
                    lblSDStartDate.setText((String) rs.getObject(8));
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstSDeleteValueChanged

    private void lstMReadValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstMReadValueChanged
        //Get the selected ID from the list
        String readID = lstMRead.getSelectedIndex() >= 0 ? lstMRead.getSelectedValue() : null;
        readID = readID != null ? readID.substring(0, readID.indexOf(" - ")) : null;
        int movieID = readID != null ? Integer.parseInt(readID) : -1;
        
        if (movieID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the read labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE, RENTAL_PRICE, NO_OF_COPIES " +
                    "FROM movies WHERE MOVIE_ID = ?"
                );
                ps.setInt(1, movieID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblMRTitle.setText((String) rs.getObject(1));
                    lblMRSynopsis.setText((String) rs.getObject(2));
                    lblMRRunTime.setText(rs.getObject(3) + "");
                    lblMRRated.setText((String) rs.getObject(4));
                    lblMRProducer.setText((String) rs.getObject(5));
                    lblMRDirector.setText((String) rs.getObject(6));
                    lblMRRelease.setText(rs.getObject(7) + "");
                    lblMRPrice.setText(rs.getObject(8) + "");
                    lblMRCopies.setText(rs.getObject(9) + "");
                }
                
                rs.close();
                ps.close();
            }
            catch(SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstMReadValueChanged

    private void lstMUpdateValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstMUpdateValueChanged
        //Get the selected ID from the list
        String updateID = lstMUpdate.getSelectedIndex() >= 0 ? lstMUpdate.getSelectedValue() : null;
        updateID = updateID != null ? updateID.substring(0, updateID.indexOf(" - ")) : null;
        int movieID = updateID != null ? Integer.parseInt(updateID) : -1;
        
        if (movieID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the update text fields with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE, RENTAL_PRICE, NO_OF_COPIES " +
                    "FROM movies WHERE MOVIE_ID = ?"
                );
                ps.setInt(1, movieID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    txfMUTitle.setText((String) rs.getObject(1));
                    txfMUSynopsis.setText((String) rs.getObject(2));
                    txfMURunTime.setText(rs.getObject(3) + "");
                    cmbMURated.setSelectedItem((String) rs.getObject(4));
                    txfMUProducer.setText((String) rs.getObject(5));
                    txfMUDirector.setText((String) rs.getObject(6));
                    ftxfMURelease.setText(rs.getObject(7) + "");
                    txfMUPrice.setText(rs.getObject(8) + "");
                    txfMUCopies.setText(rs.getObject(9) + "");
                }
                
                rs.close();
                ps.close();
            }
            catch(SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstMUpdateValueChanged

    private void lstMDeleteValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstMDeleteValueChanged
        //Get the selected ID from the list
        String deleteID = lstMDelete.getSelectedIndex() >= 0 ? lstMDelete.getSelectedValue() : null;
        deleteID = deleteID != null ? deleteID.substring(0, deleteID.indexOf(" - ")) : null;
        int movieID = deleteID != null ? Integer.parseInt(deleteID) : -1;
        
        if (movieID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the delete labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE, RENTAL_PRICE, NO_OF_COPIES " +
                    "FROM movies WHERE MOVIE_ID = ?"
                );
                ps.setInt(1, movieID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblMDTitle.setText((String) rs.getObject(1));
                    lblMDSynopsis.setText((String) rs.getObject(2));
                    lblMDRunTime.setText(rs.getObject(3) + "");
                    lblMDRated.setText((String) rs.getObject(4));
                    lblMDProducer.setText((String) rs.getObject(5));
                    lblMDDirector.setText((String) rs.getObject(6));
                    lblMDRelease.setText(rs.getObject(7) + "");
                    lblMDPrice.setText(rs.getObject(8) + "");
                    lblMDCopies.setText(rs.getObject(9) + "");
                }
                
                rs.close();
                ps.close();
            }
            catch(SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstMDeleteValueChanged

    private void btnMRFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMRFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstMRead.setSelectedIndex(0);
    }//GEN-LAST:event_btnMRFirstActionPerformed

    private void btnMRPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMRPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstMRead.getSelectedIndex();
        if (idx > 0) lstMRead.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnMRPreviousActionPerformed

    private void btnMRNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMRNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstMRead.getSelectedIndex();
        if (idx != lstMRead.getModel().getSize() - 1) lstMRead.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnMRNextActionPerformed

    private void btnMRLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMRLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstMRead.setSelectedIndex(lstMRead.getModel().getSize() - 1);
    }//GEN-LAST:event_btnMRLastActionPerformed

    private void btnMUFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMUFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstMUpdate.setSelectedIndex(0);
    }//GEN-LAST:event_btnMUFirstActionPerformed

    private void btnMUPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMUPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstMUpdate.getSelectedIndex();
        if (idx > 0) lstMUpdate.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnMUPreviousActionPerformed

    private void btnMUNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMUNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstMUpdate.getSelectedIndex();
        if (idx != lstMUpdate.getModel().getSize() - 1) lstMUpdate.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnMUNextActionPerformed

    private void btnMULastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMULastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstMUpdate.setSelectedIndex(lstMUpdate.getModel().getSize() - 1);
    }//GEN-LAST:event_btnMULastActionPerformed

    private void btnMDFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMDFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstMDelete.setSelectedIndex(0);
    }//GEN-LAST:event_btnMDFirstActionPerformed

    private void btnMDPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMDPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstMDelete.getSelectedIndex();
        if (idx > 0) lstMDelete.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnMDPreviousActionPerformed

    private void btnMDNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMDNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstMDelete.getSelectedIndex();
        if (idx != lstMDelete.getModel().getSize() - 1) lstMDelete.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnMDNextActionPerformed

    private void btnMDLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMDLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstMDelete.setSelectedIndex(lstMDelete.getModel().getSize() - 1);
    }//GEN-LAST:event_btnMDLastActionPerformed

    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() { new Admin().setVisible(true); }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnMCreate;
    private javax.swing.JButton btnMDFirst;
    private javax.swing.JButton btnMDLast;
    private javax.swing.JButton btnMDNext;
    private javax.swing.JButton btnMDPrevious;
    private javax.swing.JButton btnMDelete;
    private javax.swing.JButton btnMRFirst;
    private javax.swing.JButton btnMRLast;
    private javax.swing.JButton btnMRNext;
    private javax.swing.JButton btnMRPrevious;
    private javax.swing.JButton btnMUFirst;
    private javax.swing.JButton btnMULast;
    private javax.swing.JButton btnMUNext;
    private javax.swing.JButton btnMUPrevious;
    private javax.swing.JButton btnMUpdate;
    private javax.swing.JButton btnSCreate;
    private javax.swing.JButton btnSDFirst;
    private javax.swing.JButton btnSDLast;
    private javax.swing.JButton btnSDNext;
    private javax.swing.JButton btnSDPrevious;
    private javax.swing.JButton btnSDelete;
    private javax.swing.JButton btnSRFirst;
    private javax.swing.JButton btnSRLast;
    private javax.swing.JButton btnSRNext;
    private javax.swing.JButton btnSRPrevious;
    private javax.swing.JButton btnSUFirst;
    private javax.swing.JButton btnSULast;
    private javax.swing.JButton btnSUNext;
    private javax.swing.JButton btnSUPrevious;
    private javax.swing.JButton btnSUpdate;
    private javax.swing.JButton btnStore;
    private javax.swing.JComboBox<String> cmbMCRated;
    private javax.swing.JComboBox<String> cmbMURated;
    private javax.swing.JComboBox<String> cmbSCGender;
    private javax.swing.JComboBox<String> cmbSUGender;
    private javax.swing.JFormattedTextField ftxfMCRelease;
    private javax.swing.JFormattedTextField ftxfMURelease;
    private javax.swing.JFormattedTextField ftxfSCDOB;
    private javax.swing.JFormattedTextField ftxfSCEmail;
    private javax.swing.JFormattedTextField ftxfSUDOB;
    private javax.swing.JFormattedTextField ftxfSUEmail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JLabel lblMDCopies;
    private javax.swing.JLabel lblMDDirector;
    private javax.swing.JLabel lblMDPrice;
    private javax.swing.JLabel lblMDProducer;
    private javax.swing.JLabel lblMDRated;
    private javax.swing.JLabel lblMDRelease;
    private javax.swing.JLabel lblMDRunTime;
    private javax.swing.JLabel lblMDSynopsis;
    private javax.swing.JLabel lblMDTitle;
    private javax.swing.JLabel lblMRCopies;
    private javax.swing.JLabel lblMRDirector;
    private javax.swing.JLabel lblMRPrice;
    private javax.swing.JLabel lblMRProducer;
    private javax.swing.JLabel lblMRRated;
    private javax.swing.JLabel lblMRRelease;
    private javax.swing.JLabel lblMRRunTime;
    private javax.swing.JLabel lblMRSynopsis;
    private javax.swing.JLabel lblMRTitle;
    private javax.swing.JLabel lblSDDOB;
    private javax.swing.JLabel lblSDEmail;
    private javax.swing.JLabel lblSDGender;
    private javax.swing.JLabel lblSDName;
    private javax.swing.JLabel lblSDNumber;
    private javax.swing.JLabel lblSDPhysical;
    private javax.swing.JLabel lblSDStartDate;
    private javax.swing.JLabel lblSDSurname;
    private javax.swing.JLabel lblSRDOB;
    private javax.swing.JLabel lblSREmail;
    private javax.swing.JLabel lblSRGender;
    private javax.swing.JLabel lblSRName;
    private javax.swing.JLabel lblSRNumber;
    private javax.swing.JLabel lblSRPhysical;
    private javax.swing.JLabel lblSRStartDate;
    private javax.swing.JLabel lblSRSurname;
    private javax.swing.JList<String> lstMDelete;
    private javax.swing.JList<String> lstMRead;
    private javax.swing.JList<String> lstMUpdate;
    private javax.swing.JList<String> lstSDelete;
    private javax.swing.JList<String> lstSRead;
    private javax.swing.JList<String> lstSUpdate;
    private javax.swing.JMenuItem mnuItemExit;
    private javax.swing.JMenu mnuLogOut;
    private javax.swing.JPasswordField pwfSCConfirm;
    private javax.swing.JPasswordField pwfSCPassword;
    private javax.swing.JTable tblCustomers;
    private javax.swing.JTable tblLevels;
    private javax.swing.JTable tblMovies;
    private javax.swing.JTable tblRentals;
    private javax.swing.JTable tblSessions;
    private javax.swing.JTable tblStaff;
    private javax.swing.JTable tblTransactions;
    private javax.swing.JTextField txfMCCopies;
    private javax.swing.JTextField txfMCDirector;
    private javax.swing.JTextField txfMCPrice;
    private javax.swing.JTextField txfMCProducer;
    private javax.swing.JTextField txfMCRunTime;
    private javax.swing.JTextField txfMCSynopsis;
    private javax.swing.JTextField txfMCTitle;
    private javax.swing.JTextField txfMUCopies;
    private javax.swing.JTextField txfMUDirector;
    private javax.swing.JTextField txfMUPrice;
    private javax.swing.JTextField txfMUProducer;
    private javax.swing.JTextField txfMURunTime;
    private javax.swing.JTextField txfMUSynopsis;
    private javax.swing.JTextField txfMUTitle;
    private javax.swing.JTextField txfSCName;
    private javax.swing.JTextField txfSCNumber;
    private javax.swing.JTextField txfSCPhysical;
    private javax.swing.JTextField txfSCSurname;
    private javax.swing.JTextField txfSUName;
    private javax.swing.JTextField txfSUNumber;
    private javax.swing.JTextField txfSUPhysical;
    private javax.swing.JTextField txfSUSurname;
    // End of variables declaration//GEN-END:variables
}
