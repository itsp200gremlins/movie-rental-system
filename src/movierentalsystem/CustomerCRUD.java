package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>CustomerCRUD Screen</h1>
 * This screen handles the tasks
 * of creating, reading, updating
 * and deleting customer accounts
 * from the database
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.Color;
import java.sql.*;
import javax.swing.*;

public class CustomerCRUD extends javax.swing.JFrame
{

    //Creates new form CustomerCRUD
    public CustomerCRUD()
    {
        initComponents();
        bindCustomerCRUD();
    }
    
    Functions f = new Functions();
    
    Connection c = Functions.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    private void bindCustomerCRUD()
    {
        //Clear text fields and labels from customer CRUD
        txfCCName.setText("");
        txfCCSurname.setText("");
        ftxfCCDOB.setText("");
        cmbCCGender.setSelectedIndex(0);
        txfCCPhysical.setText("");
        txfCCNumber.setText("");
        ftxfCCEmail.setText("");
        
        lblCRName.setText("");
        lblCRSurname.setText("");
        lblCRDOB.setText("");
        lblCRGender.setText("");
        lblCRPhysical.setText("");
        lblCRNumber.setText("");
        lblCREmail.setText("");
        lblCRCreation.setText("");
        lblCRLevel.setText("");
        lblCRNoRented.setText("");
        
        txfCUName.setText("");
        txfCUSurname.setText("");
        ftxfCUDOB.setText("");
        cmbCUGender.setSelectedIndex(0);
        txfCUPhysical.setText("");
        txfCUNumber.setText("");
        ftxfCUEmail.setText("");
        
        lblCDName.setText("");
        lblCDSurname.setText("");
        lblCDDOB.setText("");
        lblCDGender.setText("");
        lblCDPhysical.setText("");
        lblCDNumber.setText("");
        lblCDEmail.setText("");
        lblCDCreation.setText("");
        lblCDLevel.setText("");
        lblCDNoRented.setText("");
        
        //Remove selection from all the lists in customer CRUD
        lstCRead.clearSelection();
        lstCUpdate.clearSelection();
        lstCDelete.clearSelection();
        
        //Create a model to be used to bind the lists in customer CRUD
        DefaultListModel dlm = new DefaultListModel();
        
        /**
         * Extract customerID and email from the database
         * and combine it to form an element for all
         * the lists
         */
        try
        {
            ps = c.prepareStatement
            (
                "SELECT CUSTOMER_ID, EMAIL " +
                "FROM customers"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                dlm.addElement(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Set the model to dlm for all lists in customer CRUD
        lstCRead.setModel(dlm);
        lstCUpdate.setModel(dlm);
        lstCDelete.setModel(dlm);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txfCCName = new javax.swing.JTextField();
        txfCCSurname = new javax.swing.JTextField();
        ftxfCCDOB = new javax.swing.JFormattedTextField();
        cmbCCGender = new javax.swing.JComboBox<>();
        txfCCPhysical = new javax.swing.JTextField();
        txfCCNumber = new javax.swing.JTextField();
        ftxfCCEmail = new javax.swing.JFormattedTextField();
        btnCCreate = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lblCRName = new javax.swing.JLabel();
        lblCRSurname = new javax.swing.JLabel();
        lblCRDOB = new javax.swing.JLabel();
        lblCRGender = new javax.swing.JLabel();
        lblCRPhysical = new javax.swing.JLabel();
        lblCRNumber = new javax.swing.JLabel();
        lblCRNoRented = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        lblCREmail = new javax.swing.JLabel();
        lblCRCreation = new javax.swing.JLabel();
        lblCRLevel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstCRead = new javax.swing.JList<>();
        btnCRFirst = new javax.swing.JButton();
        btnCRPrevious = new javax.swing.JButton();
        btnCRNext = new javax.swing.JButton();
        btnCRLast = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txfCUName = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txfCUSurname = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        ftxfCUDOB = new javax.swing.JFormattedTextField();
        jLabel19 = new javax.swing.JLabel();
        cmbCUGender = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        txfCUPhysical = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txfCUNumber = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        ftxfCUEmail = new javax.swing.JFormattedTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstCUpdate = new javax.swing.JList<>();
        btnCUFirst = new javax.swing.JButton();
        btnCUPrevious = new javax.swing.JButton();
        btnCUNext = new javax.swing.JButton();
        btnCULast = new javax.swing.JButton();
        btnCUpdate = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstCDelete = new javax.swing.JList<>();
        btnCDFirst = new javax.swing.JButton();
        btnCDPrevious = new javax.swing.JButton();
        btnCDNext = new javax.swing.JButton();
        btnCDLast = new javax.swing.JButton();
        btnCDelete = new javax.swing.JButton();
        lblCDNoRented = new javax.swing.JLabel();
        lblCDLevel = new javax.swing.JLabel();
        lblCDCreation = new javax.swing.JLabel();
        lblCDEmail = new javax.swing.JLabel();
        lblCDNumber = new javax.swing.JLabel();
        lblCDPhysical = new javax.swing.JLabel();
        lblCDGender = new javax.swing.JLabel();
        lblCDDOB = new javax.swing.JLabel();
        lblCDSurname = new javax.swing.JLabel();
        lblCDName = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Movie Shack");
        setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage());
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 40)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("CUSTOMERS");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(510, 80, 256, 52);

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setForeground(new java.awt.Color(0, 0, 0));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Name:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(100, 90, 50, 16);

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Surname:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(100, 160, 70, 16);

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Date of Birth:");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(100, 230, 80, 16);

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Gender:");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(100, 300, 50, 16);

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Physical Address:");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(100, 370, 110, 16);

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Contact Number:");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(100, 440, 100, 16);

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Email Address:");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(530, 90, 90, 16);

        txfCCName.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(txfCCName);
        txfCCName.setBounds(250, 85, 180, 25);

        txfCCSurname.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(txfCCSurname);
        txfCCSurname.setBounds(250, 155, 180, 25);

        ftxfCCDOB.setForeground(new java.awt.Color(0, 0, 0));
        ftxfCCDOB.setToolTipText("DD/MM/YYYY");
        jPanel2.add(ftxfCCDOB);
        ftxfCCDOB.setBounds(250, 225, 180, 25);

        cmbCCGender.setBackground(new java.awt.Color(255, 255, 255));
        cmbCCGender.setForeground(new java.awt.Color(0, 0, 0));
        cmbCCGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        jPanel2.add(cmbCCGender);
        cmbCCGender.setBounds(250, 295, 180, 25);

        txfCCPhysical.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(txfCCPhysical);
        txfCCPhysical.setBounds(250, 365, 180, 25);

        txfCCNumber.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(txfCCNumber);
        txfCCNumber.setBounds(250, 435, 180, 25);

        ftxfCCEmail.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(ftxfCCEmail);
        ftxfCCEmail.setBounds(700, 85, 180, 25);

        btnCCreate.setBackground(new java.awt.Color(255, 255, 255));
        btnCCreate.setForeground(new java.awt.Color(0, 0, 0));
        btnCCreate.setText("CREATE NEW CUSTOMER ACCOUNT");
        btnCCreate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCCreateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCCreateMouseExited(evt);
            }
        });
        btnCCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCCreateActionPerformed(evt);
            }
        });
        jPanel2.add(btnCCreate);
        btnCCreate.setBounds(950, 450, 240, 32);

        jTabbedPane1.addTab("CREATE", jPanel2);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Name:");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(100, 90, 50, 16);

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("Surname:");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(100, 160, 70, 16);

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Date of Birth:");
        jPanel3.add(jLabel11);
        jLabel11.setBounds(100, 230, 80, 16);

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("Gender:");
        jPanel3.add(jLabel12);
        jLabel12.setBounds(100, 300, 50, 16);

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Physical Address:");
        jPanel3.add(jLabel13);
        jLabel13.setBounds(100, 370, 110, 16);

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Contact Number:");
        jPanel3.add(jLabel14);
        jLabel14.setBounds(100, 440, 100, 16);

        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Email Address:");
        jPanel3.add(jLabel15);
        jLabel15.setBounds(530, 90, 90, 16);

        lblCRName.setForeground(new java.awt.Color(0, 0, 0));
        lblCRName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRName);
        lblCRName.setBounds(250, 89, 180, 18);

        lblCRSurname.setForeground(new java.awt.Color(0, 0, 0));
        lblCRSurname.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRSurname);
        lblCRSurname.setBounds(250, 159, 180, 18);

        lblCRDOB.setForeground(new java.awt.Color(0, 0, 0));
        lblCRDOB.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRDOB);
        lblCRDOB.setBounds(250, 229, 180, 18);

        lblCRGender.setForeground(new java.awt.Color(0, 0, 0));
        lblCRGender.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRGender);
        lblCRGender.setBounds(250, 299, 180, 18);

        lblCRPhysical.setForeground(new java.awt.Color(0, 0, 0));
        lblCRPhysical.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRPhysical);
        lblCRPhysical.setBounds(250, 369, 180, 18);

        lblCRNumber.setForeground(new java.awt.Color(0, 0, 0));
        lblCRNumber.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRNumber);
        lblCRNumber.setBounds(250, 439, 180, 18);

        lblCRNoRented.setForeground(new java.awt.Color(0, 0, 0));
        lblCRNoRented.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRNoRented);
        lblCRNoRented.setBounds(700, 299, 180, 18);

        jLabel29.setForeground(new java.awt.Color(0, 0, 0));
        jLabel29.setText("Account Creation Date:");
        jPanel3.add(jLabel29);
        jLabel29.setBounds(530, 160, 140, 16);

        jLabel31.setForeground(new java.awt.Color(0, 0, 0));
        jLabel31.setText("Membership Level:");
        jPanel3.add(jLabel31);
        jLabel31.setBounds(530, 230, 110, 16);

        jLabel32.setForeground(new java.awt.Color(0, 0, 0));
        jLabel32.setText("Number of Movies Rented:");
        jPanel3.add(jLabel32);
        jLabel32.setBounds(530, 300, 160, 16);

        lblCREmail.setForeground(new java.awt.Color(0, 0, 0));
        lblCREmail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCREmail);
        lblCREmail.setBounds(700, 89, 180, 18);

        lblCRCreation.setForeground(new java.awt.Color(0, 0, 0));
        lblCRCreation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRCreation);
        lblCRCreation.setBounds(700, 159, 180, 18);

        lblCRLevel.setForeground(new java.awt.Color(0, 0, 0));
        lblCRLevel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.add(lblCRLevel);
        lblCRLevel.setBounds(700, 229, 180, 18);

        lstCRead.setForeground(new java.awt.Color(0, 0, 0));
        lstCRead.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstCReadValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstCRead);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(940, 80, 240, 300);

        btnCRFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnCRFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnCRFirst.setText("FIRST");
        btnCRFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCRFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCRFirstMouseExited(evt);
            }
        });
        btnCRFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCRFirstActionPerformed(evt);
            }
        });
        jPanel3.add(btnCRFirst);
        btnCRFirst.setBounds(940, 400, 110, 32);

        btnCRPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnCRPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnCRPrevious.setText("PREVIOUS");
        btnCRPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCRPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCRPreviousMouseExited(evt);
            }
        });
        btnCRPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCRPreviousActionPerformed(evt);
            }
        });
        jPanel3.add(btnCRPrevious);
        btnCRPrevious.setBounds(1070, 400, 110, 32);

        btnCRNext.setBackground(new java.awt.Color(255, 255, 255));
        btnCRNext.setForeground(new java.awt.Color(0, 0, 0));
        btnCRNext.setText("NEXT");
        btnCRNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCRNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCRNextMouseExited(evt);
            }
        });
        btnCRNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCRNextActionPerformed(evt);
            }
        });
        jPanel3.add(btnCRNext);
        btnCRNext.setBounds(940, 450, 110, 32);

        btnCRLast.setBackground(new java.awt.Color(255, 255, 255));
        btnCRLast.setForeground(new java.awt.Color(0, 0, 0));
        btnCRLast.setText("LAST");
        btnCRLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCRLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCRLastMouseExited(evt);
            }
        });
        btnCRLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCRLastActionPerformed(evt);
            }
        });
        jPanel3.add(btnCRLast);
        btnCRLast.setBounds(1070, 450, 110, 32);

        jTabbedPane1.addTab("READ", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(null);

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Name:");
        jPanel4.add(jLabel16);
        jLabel16.setBounds(100, 90, 50, 16);

        txfCUName.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(txfCUName);
        txfCUName.setBounds(250, 85, 180, 25);

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Surname:");
        jPanel4.add(jLabel17);
        jLabel17.setBounds(100, 160, 70, 16);

        txfCUSurname.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(txfCUSurname);
        txfCUSurname.setBounds(250, 155, 180, 25);

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Date of Birth:");
        jPanel4.add(jLabel18);
        jLabel18.setBounds(100, 230, 80, 16);

        ftxfCUDOB.setForeground(new java.awt.Color(0, 0, 0));
        ftxfCUDOB.setToolTipText("DD/MM/YYYY");
        jPanel4.add(ftxfCUDOB);
        ftxfCUDOB.setBounds(250, 225, 180, 25);

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("Gender:");
        jPanel4.add(jLabel19);
        jLabel19.setBounds(100, 300, 50, 16);

        cmbCUGender.setBackground(new java.awt.Color(255, 255, 255));
        cmbCUGender.setForeground(new java.awt.Color(0, 0, 0));
        cmbCUGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        jPanel4.add(cmbCUGender);
        cmbCUGender.setBounds(250, 295, 180, 25);

        jLabel20.setForeground(new java.awt.Color(0, 0, 0));
        jLabel20.setText("Physical Address:");
        jPanel4.add(jLabel20);
        jLabel20.setBounds(100, 370, 110, 16);

        txfCUPhysical.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(txfCUPhysical);
        txfCUPhysical.setBounds(250, 365, 180, 25);

        jLabel21.setForeground(new java.awt.Color(0, 0, 0));
        jLabel21.setText("Contact Number:");
        jPanel4.add(jLabel21);
        jLabel21.setBounds(100, 440, 100, 16);

        txfCUNumber.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(txfCUNumber);
        txfCUNumber.setBounds(250, 435, 180, 25);

        jLabel22.setForeground(new java.awt.Color(0, 0, 0));
        jLabel22.setText("Email Address:");
        jPanel4.add(jLabel22);
        jLabel22.setBounds(530, 90, 90, 16);

        ftxfCUEmail.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(ftxfCUEmail);
        ftxfCUEmail.setBounds(700, 85, 180, 25);

        lstCUpdate.setForeground(new java.awt.Color(0, 0, 0));
        lstCUpdate.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstCUpdateValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(lstCUpdate);

        jPanel4.add(jScrollPane2);
        jScrollPane2.setBounds(940, 80, 240, 300);

        btnCUFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnCUFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnCUFirst.setText("FIRST");
        btnCUFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCUFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCUFirstMouseExited(evt);
            }
        });
        btnCUFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCUFirstActionPerformed(evt);
            }
        });
        jPanel4.add(btnCUFirst);
        btnCUFirst.setBounds(940, 400, 110, 32);

        btnCUPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnCUPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnCUPrevious.setText("PREVIOUS");
        btnCUPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCUPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCUPreviousMouseExited(evt);
            }
        });
        btnCUPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCUPreviousActionPerformed(evt);
            }
        });
        jPanel4.add(btnCUPrevious);
        btnCUPrevious.setBounds(1070, 400, 110, 32);

        btnCUNext.setBackground(new java.awt.Color(255, 255, 255));
        btnCUNext.setForeground(new java.awt.Color(0, 0, 0));
        btnCUNext.setText("NEXT");
        btnCUNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCUNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCUNextMouseExited(evt);
            }
        });
        btnCUNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCUNextActionPerformed(evt);
            }
        });
        jPanel4.add(btnCUNext);
        btnCUNext.setBounds(940, 450, 110, 32);

        btnCULast.setBackground(new java.awt.Color(255, 255, 255));
        btnCULast.setForeground(new java.awt.Color(0, 0, 0));
        btnCULast.setText("LAST");
        btnCULast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCULastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCULastMouseExited(evt);
            }
        });
        btnCULast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCULastActionPerformed(evt);
            }
        });
        jPanel4.add(btnCULast);
        btnCULast.setBounds(1070, 450, 110, 32);

        btnCUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnCUpdate.setForeground(new java.awt.Color(0, 0, 0));
        btnCUpdate.setText("UPDATE CUSTOMER ACCOUNT");
        btnCUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCUpdateMouseExited(evt);
            }
        });
        btnCUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCUpdateActionPerformed(evt);
            }
        });
        jPanel4.add(btnCUpdate);
        btnCUpdate.setBounds(590, 510, 240, 32);

        jTabbedPane1.addTab("UPDATE", jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("Name:");
        jPanel5.add(jLabel23);
        jLabel23.setBounds(100, 90, 50, 16);

        jLabel24.setForeground(new java.awt.Color(0, 0, 0));
        jLabel24.setText("Surname:");
        jPanel5.add(jLabel24);
        jLabel24.setBounds(100, 160, 60, 16);

        jLabel25.setForeground(new java.awt.Color(0, 0, 0));
        jLabel25.setText("Date of Birth:");
        jPanel5.add(jLabel25);
        jLabel25.setBounds(100, 230, 80, 16);

        jLabel68.setForeground(new java.awt.Color(0, 0, 0));
        jLabel68.setText("Gender:");
        jPanel5.add(jLabel68);
        jLabel68.setBounds(100, 300, 44, 16);

        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("Physical Address:");
        jPanel5.add(jLabel26);
        jLabel26.setBounds(100, 370, 110, 16);

        jLabel27.setForeground(new java.awt.Color(0, 0, 0));
        jLabel27.setText("Contact Number:");
        jPanel5.add(jLabel27);
        jLabel27.setBounds(100, 440, 100, 16);

        jLabel28.setForeground(new java.awt.Color(0, 0, 0));
        jLabel28.setText("Email Address:");
        jPanel5.add(jLabel28);
        jLabel28.setBounds(530, 90, 90, 16);

        jLabel33.setForeground(new java.awt.Color(0, 0, 0));
        jLabel33.setText("Account Creation Date:");
        jPanel5.add(jLabel33);
        jLabel33.setBounds(530, 160, 140, 16);

        jLabel35.setForeground(new java.awt.Color(0, 0, 0));
        jLabel35.setText("Membership Level:");
        jPanel5.add(jLabel35);
        jLabel35.setBounds(530, 230, 110, 16);

        jLabel36.setForeground(new java.awt.Color(0, 0, 0));
        jLabel36.setText("Number of Movies Rented:");
        jPanel5.add(jLabel36);
        jLabel36.setBounds(530, 300, 160, 16);

        lstCDelete.setForeground(new java.awt.Color(0, 0, 0));
        lstCDelete.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstCDeleteValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(lstCDelete);

        jPanel5.add(jScrollPane3);
        jScrollPane3.setBounds(940, 80, 240, 300);

        btnCDFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnCDFirst.setForeground(new java.awt.Color(0, 0, 0));
        btnCDFirst.setText("FIRST");
        btnCDFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCDFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCDFirstMouseExited(evt);
            }
        });
        btnCDFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCDFirstActionPerformed(evt);
            }
        });
        jPanel5.add(btnCDFirst);
        btnCDFirst.setBounds(940, 400, 110, 32);

        btnCDPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnCDPrevious.setForeground(new java.awt.Color(0, 0, 0));
        btnCDPrevious.setText("PREVIOUS");
        btnCDPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCDPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCDPreviousMouseExited(evt);
            }
        });
        btnCDPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCDPreviousActionPerformed(evt);
            }
        });
        jPanel5.add(btnCDPrevious);
        btnCDPrevious.setBounds(1070, 400, 110, 32);

        btnCDNext.setBackground(new java.awt.Color(255, 255, 255));
        btnCDNext.setForeground(new java.awt.Color(0, 0, 0));
        btnCDNext.setText("NEXT");
        btnCDNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCDNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCDNextMouseExited(evt);
            }
        });
        btnCDNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCDNextActionPerformed(evt);
            }
        });
        jPanel5.add(btnCDNext);
        btnCDNext.setBounds(940, 450, 110, 32);

        btnCDLast.setBackground(new java.awt.Color(255, 255, 255));
        btnCDLast.setForeground(new java.awt.Color(0, 0, 0));
        btnCDLast.setText("LAST");
        btnCDLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCDLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCDLastMouseExited(evt);
            }
        });
        btnCDLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCDLastActionPerformed(evt);
            }
        });
        jPanel5.add(btnCDLast);
        btnCDLast.setBounds(1070, 450, 110, 32);

        btnCDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnCDelete.setForeground(new java.awt.Color(0, 0, 0));
        btnCDelete.setText("DELETE CUSTOMER ACCOUNT");
        btnCDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCDeleteMouseExited(evt);
            }
        });
        btnCDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCDeleteActionPerformed(evt);
            }
        });
        jPanel5.add(btnCDelete);
        btnCDelete.setBounds(590, 510, 240, 32);

        lblCDNoRented.setForeground(new java.awt.Color(0, 0, 0));
        lblCDNoRented.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDNoRented);
        lblCDNoRented.setBounds(700, 299, 180, 18);

        lblCDLevel.setForeground(new java.awt.Color(0, 0, 0));
        lblCDLevel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDLevel);
        lblCDLevel.setBounds(700, 229, 180, 18);

        lblCDCreation.setForeground(new java.awt.Color(0, 0, 0));
        lblCDCreation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDCreation);
        lblCDCreation.setBounds(700, 159, 180, 18);

        lblCDEmail.setForeground(new java.awt.Color(0, 0, 0));
        lblCDEmail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDEmail);
        lblCDEmail.setBounds(700, 89, 180, 18);

        lblCDNumber.setForeground(new java.awt.Color(0, 0, 0));
        lblCDNumber.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDNumber);
        lblCDNumber.setBounds(250, 439, 180, 18);

        lblCDPhysical.setForeground(new java.awt.Color(0, 0, 0));
        lblCDPhysical.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDPhysical);
        lblCDPhysical.setBounds(250, 369, 180, 18);

        lblCDGender.setForeground(new java.awt.Color(0, 0, 0));
        lblCDGender.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDGender);
        lblCDGender.setBounds(250, 299, 180, 18);

        lblCDDOB.setForeground(new java.awt.Color(0, 0, 0));
        lblCDDOB.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDDOB);
        lblCDDOB.setBounds(250, 229, 180, 18);

        lblCDSurname.setForeground(new java.awt.Color(0, 0, 0));
        lblCDSurname.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDSurname);
        lblCDSurname.setBounds(250, 159, 180, 18);

        lblCDName.setForeground(new java.awt.Color(0, 0, 0));
        lblCDName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.add(lblCDName);
        lblCDName.setBounds(250, 89, 180, 18);

        jTabbedPane1.addTab("DELETE", jPanel5);

        jPanel1.add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 200, 1280, 640);

        btnClose.setBackground(new java.awt.Color(255, 255, 255));
        btnClose.setForeground(new java.awt.Color(0, 0, 0));
        btnClose.setText("CLOSE");
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        jPanel1.add(btnClose);
        btnClose.setBounds(980, 900, 240, 32);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1280, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 983, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCRFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRFirstMouseEntered
        btnCRFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCRFirstMouseEntered

    private void btnCRFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRFirstMouseExited
        btnCRFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCRFirstMouseExited

    private void btnCRPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRPreviousMouseEntered
        btnCRPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCRPreviousMouseEntered

    private void btnCRPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRPreviousMouseExited
        btnCRPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCRPreviousMouseExited

    private void btnCRNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRNextMouseEntered
        btnCRNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCRNextMouseEntered

    private void btnCRNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRNextMouseExited
        btnCRNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCRNextMouseExited

    private void btnCRLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRLastMouseEntered
        btnCRLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCRLastMouseEntered

    private void btnCRLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCRLastMouseExited
        btnCRLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCRLastMouseExited

    private void btnCCreateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCCreateMouseEntered
        btnCCreate.setForeground(Color.GREEN);
    }//GEN-LAST:event_btnCCreateMouseEntered

    private void btnCCreateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCCreateMouseExited
        btnCCreate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCCreateMouseExited

    private void btnCUFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUFirstMouseEntered
        btnCUFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCUFirstMouseEntered

    private void btnCUFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUFirstMouseExited
        btnCUFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCUFirstMouseExited

    private void btnCUPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUPreviousMouseEntered
        btnCUPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCUPreviousMouseEntered

    private void btnCUPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUPreviousMouseExited
        btnCUPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCUPreviousMouseExited

    private void btnCUNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUNextMouseEntered
        btnCUNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCUNextMouseEntered

    private void btnCUNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUNextMouseExited
        btnCUNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCUNextMouseExited

    private void btnCULastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCULastMouseEntered
        btnCULast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCULastMouseEntered

    private void btnCULastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCULastMouseExited
        btnCULast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCULastMouseExited

    private void btnCUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUpdateMouseEntered
        btnCUpdate.setForeground(Color.YELLOW);
    }//GEN-LAST:event_btnCUpdateMouseEntered

    private void btnCUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCUpdateMouseExited
        btnCUpdate.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCUpdateMouseExited

    private void btnCDFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDFirstMouseEntered
        btnCDFirst.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCDFirstMouseEntered

    private void btnCDFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDFirstMouseExited
        btnCDFirst.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCDFirstMouseExited

    private void btnCDPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDPreviousMouseEntered
        btnCDPrevious.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCDPreviousMouseEntered

    private void btnCDPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDPreviousMouseExited
        btnCDPrevious.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCDPreviousMouseExited

    private void btnCDNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDNextMouseEntered
        btnCDNext.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCDNextMouseEntered

    private void btnCDNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDNextMouseExited
        btnCDNext.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCDNextMouseExited

    private void btnCDLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDLastMouseEntered
        btnCDLast.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCDLastMouseEntered

    private void btnCDLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDLastMouseExited
        btnCDLast.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCDLastMouseExited

    private void btnCDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDeleteMouseEntered
        btnCDelete.setForeground(Color.RED);
    }//GEN-LAST:event_btnCDeleteMouseEntered

    private void btnCDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCDeleteMouseExited
        btnCDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCDeleteMouseExited

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        btnClose.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        btnClose.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCCreateActionPerformed
        //Extracting data from the components
        String name = txfCCName.getText(),
            surname = txfCCSurname.getText().toLowerCase(),
                DOB = ftxfCCDOB.getText(),
             gender = cmbCCGender.getSelectedItem().toString(),
           physical = txfCCPhysical.getText(),
             number = txfCCNumber.getText(),
              email = ftxfCCEmail.getText();

        /**
         * Call the customer method and pass
         * all the data for error handling
         * and database insertion.
         * If it is successful,
         * the UI will update
         */
        if (f.customer(name, surname, DOB, gender, physical, number, email, 0, 1) == 0) bindCustomerCRUD();
    }//GEN-LAST:event_btnCCreateActionPerformed

    private void btnCUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCUpdateActionPerformed
        //Extracting data from the components
        String name = txfCUName.getText(),
            surname = txfCUSurname.getText().toLowerCase(),
                DOB = ftxfCUDOB.getText(),
             gender = cmbCUGender.getSelectedItem().toString(),
           physical = txfCUPhysical.getText(),
             number = txfCUNumber.getText(),
              email = ftxfCUEmail.getText();
        
        //Get the selected ID from the list
        String customerID = lstCUpdate.getSelectedIndex() >= 0 ? lstCUpdate.getSelectedValue() : null;
        customerID = customerID != null ? customerID.substring(0, customerID.indexOf(" - ")) : null;
        int updateID = customerID != null ? Integer.parseInt(customerID) : -1;

        if (updateID != -1)
        {
            /**
             * Call the customer method and pass
             * all the data for error handling
             * and to update the database.
             * If it is successful,
             * the UI will update
             */
            if (f.customer(name, surname, DOB, gender, physical, number, email, updateID, 2) == 0) bindCustomerCRUD();
        }
    }//GEN-LAST:event_btnCUpdateActionPerformed

    private void btnCDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCDeleteActionPerformed
        //Get the selected ID from the list
        String customerID = lstCDelete.getSelectedIndex() >= 0 ? lstCDelete.getSelectedValue() : null;
        customerID = customerID != null ? customerID.substring(0, customerID.indexOf(" - ")) : null;
        int deleteID = customerID != null ? Integer.parseInt(customerID) : -1;
        
        if (deleteID != -1)
        {
            /**
             * Call the delete method and pass
             * the ID and operation type.
             * If it is successful,
             * the UI will update
             */
            if (f.delete(deleteID, 3) == 0) bindCustomerCRUD();
        }
    }//GEN-LAST:event_btnCDeleteActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
        new Store().setVisible(true);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void lstCReadValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstCReadValueChanged
        //Get the selected ID from the list
        String readID = lstCRead.getSelectedIndex() >= 0 ? lstCRead.getSelectedValue() : null;
        readID = readID != null ? readID.substring(0, readID.indexOf(" - ")) : null;
        int customerID = readID != null ? Integer.parseInt(readID) : -1;
        
        if (customerID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the read labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT c.NAME, c.SURNAME, c.DOB, c.GENDER, c.PHYSICAL_ADDRESS, c.CONTACT_NUMBER, " +
                    "c.EMAIL, c.ACCOUNT_CREATION_DATE, mem.NAME, c.NO_OF_RENTALS " +
                    "FROM customers c, membership_levels mem " +
                    "WHERE CUSTOMER_ID = ? AND mem.LEVEL_ID = c.FK_LEVEL_ID"
                );
                ps.setInt(1, customerID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblCRName.setText((String) rs.getObject(1));
                    lblCRSurname.setText((String) rs.getObject(2));
                    lblCRDOB.setText((String) rs.getObject(3));
                    lblCRGender.setText((String) rs.getObject(4));
                    lblCRPhysical.setText((String) rs.getObject(5));
                    lblCRNumber.setText((String) rs.getObject(6));
                    lblCREmail.setText((String) rs.getObject(7));
                    lblCRCreation.setText((String) rs.getObject(8));
                    lblCRLevel.setText((String) rs.getObject(9));
                    lblCRNoRented.setText(rs.getObject(10) + "");
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstCReadValueChanged

    private void btnCRFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCRFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstCRead.setSelectedIndex(0);
    }//GEN-LAST:event_btnCRFirstActionPerformed

    private void btnCRPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCRPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstCRead.getSelectedIndex();
        if (idx > 0) lstCRead.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnCRPreviousActionPerformed

    private void btnCRNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCRNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstCRead.getSelectedIndex();
        if (idx != lstCRead.getModel().getSize() - 1) lstCRead.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnCRNextActionPerformed

    private void btnCRLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCRLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstCRead.setSelectedIndex(lstCRead.getModel().getSize() - 1);
    }//GEN-LAST:event_btnCRLastActionPerformed

    private void lstCUpdateValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstCUpdateValueChanged
        //Get the selected ID from the list
        String updateID = lstCUpdate.getSelectedIndex() >= 0 ? lstCUpdate.getSelectedValue() : null;
        updateID = updateID != null ? updateID.substring(0, updateID.indexOf(" - ")) : null;
        int customerID = updateID != null ? Integer.parseInt(updateID) : -1;
        
        if (customerID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the update text fields with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL " +
                    "FROM customers " +
                    "WHERE CUSTOMER_ID = ?"
                );
                ps.setInt(1, customerID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    txfCUName.setText((String) rs.getObject(1));
                    txfCUSurname.setText((String) rs.getObject(2));
                    ftxfCUDOB.setText((String) rs.getObject(3));
                    cmbCUGender.setSelectedItem((String) rs.getObject(4));
                    txfCUPhysical.setText((String) rs.getObject(5));
                    txfCUNumber.setText((String) rs.getObject(6));
                    ftxfCUEmail.setText((String) rs.getObject(7));
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstCUpdateValueChanged

    private void btnCUFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCUFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstCUpdate.setSelectedIndex(0);
    }//GEN-LAST:event_btnCUFirstActionPerformed

    private void btnCUPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCUPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstCUpdate.getSelectedIndex();
        if (idx > 0) lstCUpdate.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnCUPreviousActionPerformed

    private void btnCUNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCUNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstCUpdate.getSelectedIndex();
        if (idx != lstCUpdate.getModel().getSize() - 1) lstCUpdate.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnCUNextActionPerformed

    private void btnCULastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCULastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstCUpdate.setSelectedIndex(lstCUpdate.getModel().getSize() - 1);
    }//GEN-LAST:event_btnCULastActionPerformed

    private void lstCDeleteValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstCDeleteValueChanged
        //Get the selected ID from the list
        String deleteID = lstCDelete.getSelectedIndex() >= 0 ? lstCDelete.getSelectedValue() : null;
        deleteID = deleteID != null ? deleteID.substring(0, deleteID.indexOf(" - ")) : null;
        int customerID = deleteID != null ? Integer.parseInt(deleteID) : -1;
        
        if (customerID != -1)
        {
            /**
             * Gets the relevant data from the database
             * and binds the delete labels with the data
             */
            try
            {
                ps = c.prepareStatement
                (
                    "SELECT c.NAME, c.SURNAME, c.DOB, c.GENDER, c.PHYSICAL_ADDRESS, c.CONTACT_NUMBER, " +
                    "c.EMAIL, c.ACCOUNT_CREATION_DATE, mem.NAME, c.NO_OF_RENTALS " +
                    "FROM customers c, membership_levels mem " +
                    "WHERE CUSTOMER_ID = ? AND mem.LEVEL_ID = c.FK_LEVEL_ID"
                );
                ps.setInt(1, customerID);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    lblCDName.setText((String) rs.getObject(1));
                    lblCDSurname.setText((String) rs.getObject(2));
                    lblCDDOB.setText((String) rs.getObject(3));
                    lblCDGender.setText((String) rs.getObject(4));
                    lblCDPhysical.setText((String) rs.getObject(5));
                    lblCDNumber.setText((String) rs.getObject(6));
                    lblCDEmail.setText((String) rs.getObject(7));
                    lblCDCreation.setText((String) rs.getObject(8));
                    lblCDLevel.setText((String) rs.getObject(9));
                    lblCDNoRented.setText(rs.getObject(10) + "");
                }
                
                rs.close();
                ps.close();
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }//GEN-LAST:event_lstCDeleteValueChanged

    private void btnCDFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCDFirstActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the first element
         */
        lstCDelete.setSelectedIndex(0);
    }//GEN-LAST:event_btnCDFirstActionPerformed

    private void btnCDPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCDPreviousActionPerformed
        /**
         * Gets the selected index
         * and reduces it by one
         * only if it's greater
         * than zero for the
         * relevant list
         */
        int idx = lstCDelete.getSelectedIndex();
        if (idx > 0) lstCDelete.setSelectedIndex(--idx);
    }//GEN-LAST:event_btnCDPreviousActionPerformed

    private void btnCDNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCDNextActionPerformed
        /**
         * Gets the selected index
         * and increments it by one
         * only if it's less
         * than the size of the
         * relevant list
         */
        int idx = lstCDelete.getSelectedIndex();
        if (idx != lstCDelete.getModel().getSize() - 1) lstCDelete.setSelectedIndex(++idx);
    }//GEN-LAST:event_btnCDNextActionPerformed

    private void btnCDLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCDLastActionPerformed
        /**
         * Sets the selected index of the relevant
         * list to the last element
         */
        lstCDelete.setSelectedIndex(lstCDelete.getModel().getSize() - 1);
    }//GEN-LAST:event_btnCDLastActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        new Store().setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomerCRUD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomerCRUD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomerCRUD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomerCRUD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomerCRUD().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCCreate;
    private javax.swing.JButton btnCDFirst;
    private javax.swing.JButton btnCDLast;
    private javax.swing.JButton btnCDNext;
    private javax.swing.JButton btnCDPrevious;
    private javax.swing.JButton btnCDelete;
    private javax.swing.JButton btnCRFirst;
    private javax.swing.JButton btnCRLast;
    private javax.swing.JButton btnCRNext;
    private javax.swing.JButton btnCRPrevious;
    private javax.swing.JButton btnCUFirst;
    private javax.swing.JButton btnCULast;
    private javax.swing.JButton btnCUNext;
    private javax.swing.JButton btnCUPrevious;
    private javax.swing.JButton btnCUpdate;
    private javax.swing.JButton btnClose;
    private javax.swing.JComboBox<String> cmbCCGender;
    private javax.swing.JComboBox<String> cmbCUGender;
    private javax.swing.JFormattedTextField ftxfCCDOB;
    private javax.swing.JFormattedTextField ftxfCCEmail;
    private javax.swing.JFormattedTextField ftxfCUDOB;
    private javax.swing.JFormattedTextField ftxfCUEmail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblCDCreation;
    private javax.swing.JLabel lblCDDOB;
    private javax.swing.JLabel lblCDEmail;
    private javax.swing.JLabel lblCDGender;
    private javax.swing.JLabel lblCDLevel;
    private javax.swing.JLabel lblCDName;
    private javax.swing.JLabel lblCDNoRented;
    private javax.swing.JLabel lblCDNumber;
    private javax.swing.JLabel lblCDPhysical;
    private javax.swing.JLabel lblCDSurname;
    private javax.swing.JLabel lblCRCreation;
    private javax.swing.JLabel lblCRDOB;
    private javax.swing.JLabel lblCREmail;
    private javax.swing.JLabel lblCRGender;
    private javax.swing.JLabel lblCRLevel;
    private javax.swing.JLabel lblCRName;
    private javax.swing.JLabel lblCRNoRented;
    private javax.swing.JLabel lblCRNumber;
    private javax.swing.JLabel lblCRPhysical;
    private javax.swing.JLabel lblCRSurname;
    private javax.swing.JList<String> lstCDelete;
    private javax.swing.JList<String> lstCRead;
    private javax.swing.JList<String> lstCUpdate;
    private javax.swing.JTextField txfCCName;
    private javax.swing.JTextField txfCCNumber;
    private javax.swing.JTextField txfCCPhysical;
    private javax.swing.JTextField txfCCSurname;
    private javax.swing.JTextField txfCUName;
    private javax.swing.JTextField txfCUNumber;
    private javax.swing.JTextField txfCUPhysical;
    private javax.swing.JTextField txfCUSurname;
    // End of variables declaration//GEN-END:variables
}
