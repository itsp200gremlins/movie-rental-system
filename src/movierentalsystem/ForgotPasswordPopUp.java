package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Forgot Password Pop-up Screen</h1>
 * This screen allows staff members to reset
 * their passwords with the email address
 * they used to create their account
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ForgotPasswordPopUp
{
    
    public ForgotPasswordPopUp()
    {
        //Creates a new frame
        JFrame f = new JFrame("Movie Shack");
        f.setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage()); //Sets the icon for frame
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 640); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we) { new Login(); }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("RESET PASSWORD"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for email
        JLabel lblEmail = new JLabel("Email:");
        lblEmail.setSize(lblEmail.getPreferredSize());
        
        //Textfield for email to be inputted
        JTextField txfEmail = new JTextField(15);
        txfEmail.setSize(txfEmail.getPreferredSize());
        
        //Label for password
        JLabel lblPassword = new JLabel("Enter New Password:");
        lblPassword.setSize(lblPassword.getPreferredSize());
        
        //Password field for a new password to be inputted
        JPasswordField pwfPassword = new JPasswordField(15);
        pwfPassword.setSize(pwfPassword.getPreferredSize());
        
        //Label for confirm password
        JLabel lblConfirm = new JLabel("Confirm New Password:");
        lblConfirm.setSize(lblConfirm.getPreferredSize());
        
        //Password field for a new password to be confirmed
        JPasswordField pwfConfirm = new JPasswordField(15);
        pwfConfirm.setSize(pwfConfirm.getPreferredSize());
        
        //Button to reset password
        JButton btnReset = new JButton("RESET PASSWORD");
        btnReset.setSize(btnReset.getPreferredSize());
        btnReset.setBackground(Color.WHITE);
        btnReset.setForeground(Color.BLACK);
        btnReset.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String email = txfEmail.getText(),
                    password = "",
                     confirm = "";
                
                char passwordArr[] = pwfPassword.getPassword(),
                      confirmArr[] = pwfConfirm.getPassword();
                
                //Iterating through the char arrays to represent the passwords as Strings
                for (char c : passwordArr) password += c;
                for (char c : confirmArr) confirm += c;
                
                Functions f1 = new Functions();
                
                if (f1.resetPassword(email, password, confirm) == 0) f.dispose();
                else
                {
                    txfEmail.setText("");
                    pwfPassword.setText("");
                    pwfConfirm.setText("");
                }
            }

            @Override
            public void mouseEntered(MouseEvent me) { btnReset.setForeground(Color.GREEN); }

            @Override
            public void mouseExited(MouseEvent me) { btnReset.setForeground(Color.BLACK); }
        });
        
        //Button to cancel operation
        JButton btnCancel = new JButton("CANCEL");
        btnCancel.setSize(btnReset.getPreferredSize());
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new Login();
            }

            @Override
            public void mouseEntered(MouseEvent me) { btnCancel.setForeground(Color.RED); }

            @Override
            public void mouseExited(MouseEvent me) { btnCancel.setForeground(Color.BLACK); }
        });
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 80);
        lblEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 220);
        txfEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 240);
        lblPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 280);
        pwfPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 300);
        lblConfirm.setLocation((f.getWidth()-pwfConfirm.getWidth())/2, 340);
        pwfConfirm.setLocation((f.getWidth()-pwfConfirm.getWidth())/2, 360);
        btnReset.setLocation(((f.getWidth()-btnReset.getWidth())/2) - (btnReset.getWidth()/2) - 10, 480);
        btnCancel.setLocation(((f.getWidth()-btnCancel.getWidth())/2) + (btnCancel.getWidth()/2) + 10, 480);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblEmail);
        f.getContentPane().add(txfEmail);
        f.getContentPane().add(lblPassword);
        f.getContentPane().add(pwfPassword);
        f.getContentPane().add(lblConfirm);
        f.getContentPane().add(pwfConfirm);
        f.getContentPane().add(btnReset);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
    }
    
}
