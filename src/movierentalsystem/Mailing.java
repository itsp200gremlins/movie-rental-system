package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Mailing Class</h1>
 * This class is responsible for
 * mailing customers the following:
 * 1. A warm welcome when they create an account
 * 2. Their receipts when they rent movies
 * 3. Reminders to return movies on the due date
 * 4. A thank you message when they return movies
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.JOptionPane;

public class Mailing
{
    
    public Mailing() { setProperties(); }
    
    //Email account information
    private final String USERNAME = "movieshackstore",
                         PASSWORD = "movieshack123",
                             HOST = "smtp.gmail.com";
    
    //Variables used to send emails
    private final Properties p = System.getProperties();
    private final Session s = Session.getDefaultInstance(p);
    private MimeMessage mm = new MimeMessage(s);
    private Transport t = null;

    /**
     * This method adds the required properties
     * to the properties variable [p] to allow
     * emails to be set
     */
    private void setProperties()
    {
        p.put("mail.smtp.starttls.enable", "true"); //Enable Transport Layer Security
        p.put("mail.smtp.host", HOST); //The service used to send emails [GMail]
        p.put("mail.smtp.user", USERNAME); //Sets the username for the email account [movieshackstore]
        p.put("mail.smtp.password", PASSWORD); //Sets the password to be used for the email account
        p.put("mail.smtp.port", "587"); //Sets the official Simple Mail Transfer Protocol [SMTP] port number
        p.put("mail.smtp.auth", "true"); //Enables authentication
    }
    
    /**
     * This method is used to set the MimeMessage
     * and the Transport for an email and then
     * sends it
     */
    private void sendEmail(String recipient, String subject, String body)
    {
        try
        {
            mm.setFrom(new InternetAddress(USERNAME)); //movieshackstore@gmail.com
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient)); //Customer's email address
            mm.setSubject(subject);
            mm.setText(body);
            
            t = s.getTransport("smtp"); //Sets the transport to SMTP
            t.connect(HOST, USERNAME, PASSWORD); //Connect GMail with the credentials of the store
            t.sendMessage(mm, mm.getAllRecipients()); //Sends the email
            t.close(); //Closes the transport
            
            mm = new MimeMessage(s); //Refreshes mm to avoid accumulating recipients
        }
        catch (AddressException ae) { JOptionPane.showMessageDialog(null, ae.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        catch (MessagingException me) { JOptionPane.showMessageDialog(null, me.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }
    
    //Sends a warm welcome message to a new customer
    void emailNewCustomerWelcome(String recipient)
    {
        String subject = "Welcome to Movie Shack",
                  body = "Thank you for becoming a customer of Movie Shack";
        
        sendEmail(recipient, subject, body);
    }
    
    //Sends the receipt to the customer upon a successful transaction
    void emailReceipt(String recipient, String receipt)
    {
        String subject = "Movie Shack Receipt",
                  body = receipt;
        
        sendEmail(recipient, subject, body);
    }
    
    //Sends a thank you message to the customer when they return movies
    void emailThankYouForReturning(String recipient, String message)
    {
        String subject = "Movie Shack Returns Successful",
                  body = message;
        
        sendEmail(recipient, subject, body);
    }
    
    //Sends a reminder to the customer that movies are due
    void emailReminder(String recipient, String message)
    {
        String subject = "Movie Shack: Return Date Reached",
                  body = message;
        
        sendEmail(recipient, subject, body);
    }
    
}
