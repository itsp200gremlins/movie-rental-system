package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Returns Screen</h1>
 * This screen is used to process returns
 * from customers using the relevant
 * transaction ID from their receipt
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Returns
{
    
    Functions f1 = new Functions();
    Connection c = Functions.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    private DefaultTableModel dtm; //The model for tblMovieList
    
    //Column names for tblMovieList
    private final Object tblMovieListHeaders[] =
    {
        "RETURNING ?",
        "RENTAL ID",
        "MOVIE ID",
        "TITLE"
    };
    
    public Returns()
    {
        JFrame f = new JFrame("Movie Shack"); //Creates a new frame
        f.setIconImage(new ImageIcon("src\\icons\\Movie Shack Icon.png").getImage()); //Sets the icon for frame
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 640); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we) { new Store().setVisible(true); }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("RETURNS"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for select transaction ID
        JLabel lblSelect = new JLabel("Select Transaction ID:");
        lblSelect.setSize(lblSelect.getPreferredSize());
        
        setDTM(); //Sets [or resets] the model to be used by the table
        
        //JTable that will hold the unreturned movies
        JTable tblMovieList = new JTable(dtm);
        tblMovieList.setForeground(Color.BLACK);
        tblMovieList.setBackground(Color.WHITE);
        tblMovieList.setFillsViewportHeight(true);
        
        //The viewport for tblMovieList
        JScrollPane sp = new JScrollPane(tblMovieList);
        sp.setSize(f.getWidth() - 80, 220);
        
        //Combo box that will hold all the transaction IDs that have unreturned movies
        JComboBox cmbSelect = new JComboBox(bindcmbSelect());
        cmbSelect.setSelectedIndex(-1);
        cmbSelect.setForeground(Color.BLACK);
        cmbSelect.setBackground(Color.WHITE);
        cmbSelect.setSize(f.getWidth() - 80, cmbSelect.getPreferredSize().height);
        cmbSelect.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent ie)
            {
                if (cmbSelect.getSelectedIndex() >= 0)
                {
                    setDTM(); //Reset the table model
                    
                    int transactionID = (int) cmbSelect.getSelectedItem(); //Get the ID
                    
                    /**
                     * Gets all the rentals from rentals_per_transaction
                     * where there is no return date and has its
                     * transaction ID foreign key equal to
                     * the selected transaction ID.
                     * Once the result set is formed,
                     * its rows are added to the table model [dtm]
                     * and then the model for tblMovieList
                     * is set to dtm
                     */
                    try
                    {
                        ps = c.prepareStatement
                        (
                            "SELECT r.RENTAL_ID, m.MOVIE_ID, m.TITLE " +
                            "FROM rentals_per_transaction r, movies m " +
                            "WHERE r.RETURN_DATE IS NULL AND r.FK_TRANSACTION_ID = ? AND m.MOVIE_ID = r.FK_MOVIE_ID"
                        );
                        ps.setInt(1, transactionID);

                        rs = ps.executeQuery();

                        while (rs.next())
                        {
                            Object row[] =
                            {
                                false,
                                rs.getObject(1),
                                rs.getObject(2),
                                rs.getObject(3)
                            };

                            dtm.addRow(row);
                        }

                        rs.close();
                        ps.close();

                        tblMovieList.setModel(dtm);
                    }
                    catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE); }
                }
            }
        });
        
        //Button to process returns
        JButton btnProcessReturn = new JButton("PROCESS RETURN");
        btnProcessReturn.setForeground(Color.BLACK);
        btnProcessReturn.setBackground(Color.WHITE);
        btnProcessReturn.setSize(btnProcessReturn.getPreferredSize());
        btnProcessReturn.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                if (cmbSelect.getSelectedIndex() >= 0)
                {
                    int transactionID = (int) cmbSelect.getSelectedItem(); //Get the ID

                    /**
                     * Create ArrayLists to hold all the rentalIDs, movieID and titles
                     * that will be used to form an email to be sent to the customer
                     */
                    ArrayList returns = new ArrayList(),
                               movies = new ArrayList(),
                          movieTitles = new ArrayList();

                    /**
                     * Go through all the rows to determine if movies
                     * are being returns and add the information
                     * from that row to the relevant ArrayList
                     */
                    for (int row = 0; row < tblMovieList.getRowCount(); row++)
                    {
                        if ((boolean) tblMovieList.getValueAt(row, 0))
                        {
                            returns.add(tblMovieList.getValueAt(row, 1));
                            movies.add(tblMovieList.getValueAt(row, 2));
                            movieTitles.add(tblMovieList.getValueAt(row, 3));
                        }
                    }

                    /**
                     * Only processes a return if there
                     * are elements in the ArrayLists
                     */
                    if (returns.size() > 0)
                    {
                        if (f1.processReturn(transactionID, returns, movies, movieTitles) == 0)
                        {
                            f.dispose();
                            new Store().setVisible(true);
                        }
                    }
                }
            }
            
            @Override
            public void mouseEntered(MouseEvent me) { btnProcessReturn.setForeground(Color.GREEN); }
            
            @Override
            public void mouseExited(MouseEvent me) { btnProcessReturn.setForeground(Color.BLACK); }
        });
        
        //Button to cancel and go back to the store
        JButton btnCancel = new JButton("CANCEL");
        btnCancel.setSize(btnProcessReturn.getPreferredSize());
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new Store().setVisible(true);
            }

            @Override
            public void mouseEntered(MouseEvent me) { btnCancel.setForeground(Color.RED); }

            @Override
            public void mouseExited(MouseEvent me) { btnCancel.setForeground(Color.BLACK); }
        });
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth()) / 2, 80);
        lblSelect.setLocation((f.getWidth() - cmbSelect.getWidth()) / 2, 200);
        cmbSelect.setLocation((f.getWidth() - cmbSelect.getWidth()) / 2, lblSelect.getLocation().y + 20);
        sp.setLocation((f.getWidth() - sp.getWidth()) / 2, cmbSelect.getLocation().y + 40);
        btnProcessReturn.setLocation(((f.getWidth() - btnProcessReturn.getWidth()) / 2) - (btnProcessReturn.getWidth() / 2) - 10, sp.getLocation().y + 260);
        btnCancel.setLocation(((f.getWidth() - btnCancel.getWidth()) / 2) + (btnCancel.getWidth() / 2) + 10, btnProcessReturn.getLocation().y);
                
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblSelect);
        f.getContentPane().add(cmbSelect);
        f.getContentPane().add(sp);
        f.getContentPane().add(btnProcessReturn);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
    }
    
    /**
     * This method sets the table model
     * with overridden methods that makes
     * the first column of the table boolean
     * which will show check boxes
     * and only allows the first column to
     * be editable
     */
    private void setDTM()
    {
        dtm = new DefaultTableModel(tblMovieListHeaders, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return columnIndex == 0 ? Boolean.class : Object.class;
            }
            
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column == 0;
            }
        };
    }
    
    /**
     * This method returns an object array
     * that contains the transaction IDs
     * that do not have return dates
     */
    private Object[] bindcmbSelect()
    {
        ArrayList tIDs = new ArrayList();
        
        try
        {
            ps = c.prepareStatement
            (
                "SELECT DISTINCT FK_TRANSACTION_ID " +
                "FROM rentals_per_transaction " +
                "WHERE RETURN_DATE IS NULL"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next()) { tIDs.add(rs.getObject(1)); }
            
            rs.close();
            ps.close();
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE); }
        
        Object transactionIDs[] = new Object[tIDs.size()];
        
        for (int i = 0; i < tIDs.size(); i++) transactionIDs[i] = tIDs.get(i);
        
        return transactionIDs;
    }
    
}
