package movierentalsystem;

/*
 * Copyright (C) 2018 Gremlins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * <h1>Functions Class</h1>
 * This class is the backbone of
 * the project. It does all major
 * processes of the system
 * 
 * @author Shaylen Reddy <shaylenreddy42@gmail.com>
 * @version 1.0.2
 * @since Release
 */

import java.io.*;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import java.sql.*;
import java.text.*;
import java.util.*;

public class Functions
{
    
    Mailing  m = new Mailing();
    
    //Patterns for performing error handling on mostly staff and customers
    private final Pattern forNames = Pattern.compile("\\A[A-Z]{2,20}\\z", Pattern.CASE_INSENSITIVE),
                          forDates = Pattern.compile("\\A\\d{2}?/\\d{2}?/\\d{4}?\\z"),
                      forAddresses = Pattern.compile("\\A[A-Z0-9,\\s]{5,50}\\z", Pattern.CASE_INSENSITIVE),
                        forNumbers = Pattern.compile("\\A\\d{10}\\z"),
                         forEmails = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE),
                      forPasswords = Pattern.compile("\\A.{8,}\\z"),
    //Patterns for performing error handling on movies
                    forMovieTitles = Pattern.compile("\\A[A-Z0-9,'\"\\s-\\.]{2,50}\\z", Pattern.CASE_INSENSITIVE),
                       forSynopsis = Pattern.compile("\\A[A-Z,'\"-\\.\\s]{10,250}\\z", Pattern.CASE_INSENSITIVE),
                       forRuntimes = Pattern.compile("\\A\\d{2,3}\\z"),
                     forMovieNames = Pattern.compile("\\A[A-Z\\s]{5,40}+\\z", Pattern.CASE_INSENSITIVE),
                       forReleases = Pattern.compile("\\A\\d{4}\\z"),
                         forPrices = Pattern.compile("\\A\\d{1,2}\\.\\d{2}\\z"),
                     forNoOfCopies = Pattern.compile("\\A\\d{1,2}\\z");
    
    //Initializing error messages to be used during error handling
    private final String initialErrMsg = "ERROR: SAMPLE [VALID RANGE]\r\n\r\n";
    private String finalErrMsg = null;
    
    //Initializing constants for checking operation type
    private final int STAFFDELETE = -3,
                      STAFFUPDATE = -2,
                      STAFFINSERT = -1,
                   CUSTOMERINSERT =  1,
                   CUSTOMERUPDATE =  2,
                   CUSTOMERDELETE =  3,
                      MOVIEINSERT = 11,
                      MOVIEUPDATE = 12,
                      MOVIEDELETE = 13;
    
    /**
     * Creating a calendar variable that is going to be used to get
     * the current date and be used to calculate the return date for movies
     * and also create the recommended formats for date and time
     */
    private final Calendar now = Calendar.getInstance();
    private final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy"),
                                   sdfTime = new SimpleDateFormat("HH:mm:ss");
    
    //Variables to hold login details
    private static String currentUser,
                        currentUserID,
                          sessionDate,
                     sessionStartTime;
    
    //Database constants
    private static final String DATABASE_URL = "jdbc:derby:C:/Users/" +
                                                System.getProperty("user.name") +
                                                "/Documents/Movie Shack",
                               DATABASE_USER = "admin",
                           DATABASE_PASSWORD = "1234";
    
    //SQL variables used to interact with the database
    private static Connection dbConnection = null;
    private static PreparedStatement ps = null;
    private static ResultSet rs = null;
    
    /**
     * Method checks if the database is connected
     * which will be used by the login screen
     * to determine database status
     */
    boolean isDatabaseConnected(Connection c) { return c != null; }
    
    //Returns a database connection
    static Connection getConnection()
    {
        try { dbConnection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD); }
        catch (SQLException ex) { dbConnection = createDatabase(); }
        
        return dbConnection;
    }
    
    /**
     * Closes the database connection
     * which will be called when the
     * application is closed
     */
    void closeConnection()
    {
        if (dbConnection != null)
        {
            try { dbConnection.close(); }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }
    
    //Creates the database and returns database connection
    private static Connection createDatabase()
    {
        try
        {
            dbConnection = DriverManager.getConnection(DATABASE_URL + ";create=true", DATABASE_USER, DATABASE_PASSWORD);
            
            //Creates staff table
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE staff" +
                "( " +
                "STAFF_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "NAME VARCHAR(20) NOT NULL, " +
                "SURNAME VARCHAR(20) NOT NULL, " +
                "DOB VARCHAR(10) NOT NULL, " +
                "GENDER VARCHAR(6) NOT NULL, " +
                "PHYSICAL_ADDRESS VARCHAR(250) NOT NULL, " +
                "CONTACT_NUMBER VARCHAR(10) UNIQUE NOT NULL, " +
                "EMAIL VARCHAR(100) UNIQUE NOT NULL, " +
                "PASSWORD VARCHAR(50) NOT NULL, " +
                "START_DATE VARCHAR(10) NOT NULL " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            //Creates sessions table used to record user login sessions
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE sessions" +
                "( " +
                "SESSION_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "SESSION_DATE VARCHAR(10) NOT NULL, " +
                "START_TIME VARCHAR(8) NOT NULL, " +
                "END_TIME VARCHAR(8) NOT NULL, " +
                "FK_STAFF_ID INT REFERENCES staff(STAFF_ID) " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            /**
             * Creates membership_levels table used to determine
             * a customer's membership level and discount
             */
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE membership_levels" +
                "( " +
                "LEVEL_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "NAME VARCHAR(10) NOT NULL, " +
                "DISCOUNT_PERCENT REAL NOT NULL " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            /**
             * Insert default membership levels into
             * table membership_levels and their
             * relevant discounts
             */
            ps = dbConnection.prepareStatement
            (
                "INSERT INTO membership_levels " +
                "(NAME, DISCOUNT_PERCENT) " +
                "VALUES (?, ?), (?, ?), (?, ?), (?, ?)"
            );
            ps.setString(1, "Bronze");
            ps.setDouble(2, 0.0);
            ps.setString(3, "Silver");
            ps.setDouble(4, 0.1);
            ps.setString(5, "Gold");
            ps.setDouble(6, 0.15);
            ps.setString(7, "Platinum");
            ps.setDouble(8, 0.25);
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            //Creates table for customers
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE customers" +
                "( " +
                "CUSTOMER_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "NAME VARCHAR(20) NOT NULL, " +
                "SURNAME VARCHAR(20) NOT NULL, " +
                "DOB VARCHAR(10) NOT NULL, " +
                "GENDER VARCHAR(6) NOT NULL, " +
                "PHYSICAL_ADDRESS VARCHAR(250) NOT NULL, " +
                "CONTACT_NUMBER VARCHAR(10) UNIQUE NOT NULL, " +
                "EMAIL VARCHAR(100) UNIQUE NOT NULL, " +
                "ACCOUNT_CREATION_DATE VARCHAR(12) NOT NULL, " +
                "FK_LEVEL_ID INT REFERENCES membership_levels(LEVEL_ID) DEFAULT 1, " +
                "NO_OF_RENTALS INT DEFAULT 0 NOT NULL " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            //Creates table for movies
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE movies" +
                "( " +
                "MOVIE_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "TITLE VARCHAR(50) NOT NULL, " +
                "SYNOPSIS VARCHAR(250) NOT NULL, " +
                "RUNTIME INT NOT NULL, " +
                "RATED VARCHAR(50) NOT NULL, " +
                "PRODUCER VARCHAR(40) NOT NULL, " +
                "DIRECTOR VARCHAR(40) NOT NULL, " +
                "RELEASE_DATE INT NOT NULL, " +
                "RENTAL_PRICE REAL NOT NULL, " +
                "NO_OF_COPIES INT NOT NULL " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            //Create transactions table to keep a record of all transactions
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE transactions" +
                "( " +
                "TRANSACTION_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "DATE VARCHAR(10) NOT NULL, " +
                "TIME VARCHAR(8) NOT NULL, " +
                "TOTAL_RENTALS INT NOT NULL, " +
                "TOTAL_PRICE REAL NOT NULL, " +
                "FK_STAFF_ID INT REFERENCES staff(STAFF_ID), " +
                "FK_CUSTOMER_ID INT REFERENCES customers(CUSTOMER_ID) " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            /**
             * Create rentals per transaction tables used to track
             * the movies rented in a specific transaction
             */
            ps = dbConnection.prepareStatement
            (
                "CREATE TABLE rentals_per_transaction" +
                "( " +
                "RENTAL_ID INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY NOT NULL, " +
                "DUE_DATE VARCHAR(10) NOT NULL, " +
                "RETURN_DATE VARCHAR(10) DEFAULT NULL, " +
                "FK_MOVIE_ID INT REFERENCES movies(MOVIE_ID), " +
                "FK_TRANSACTION_ID INT REFERENCES transactions(TRANSACTION_ID) " +
                ")"
            );
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            //Closes the database connection and reopens it
            dbConnection.close();
            dbConnection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD);
        }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        return dbConnection;
    }
    
    //Sets the login details to be passed to the store
    void setStaffLoginInformation(String staffFullName, String staffID, String date, String startTime)
    {
        sessionStartTime = startTime;
             sessionDate = date;
             currentUser = staffFullName;
           currentUserID = staffID;
    }
    
    //Retrieves the login details
    String getStaffFullName() { return currentUser; }
    String getStaffID() { return currentUserID; }
    
    //Method for handling the login process
    int login(String email, String password)
    {
        finalErrMsg = initialErrMsg;
        int error = 0;
        
        if (forEmails.matcher(email).find() == false)
        {
            finalErrMsg += "Email: sample@email.com";
            error = 1;
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so, the error message will be shown
         * else a check will be made to see if it's the admin account
         * and if not, checks if the user is in the database
         * and only then an error message appears, telling the user
         * that the account does not exist if the check does not
         * form a result set with one row
         */
        if (error == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        else if (email.equalsIgnoreCase("admin@movieshack.com") && password.isEmpty())
        {
            setStaffLoginInformation("Admin", "N/A", sdfDate.format(now.getTime()), sdfTime.format(now.getTime()));
            new Admin().setVisible(true);
        }
        else
        {
            /**
             * Extracts the name, surname and staffID
             * from the database to be used to set
             * the staff login information
             */
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT NAME, SURNAME, STAFF_ID " +
                    "FROM staff " +
                    "WHERE EMAIL = ? AND PASSWORD = ?"
                );
                ps.setString(1, email);
                ps.setString(2, password);
                
                rs = ps.executeQuery(); //Creates a result set
                
                if (rs.next())
                {
                    setStaffLoginInformation(rs.getObject(1) + " " + rs.getObject(2), rs.getObject(3) + "", sdfDate.format(now.getTime()), sdfTime.format(now.getTime()));
                    new Store().setVisible(true);
                }
                else error = 2;
                
                rs.close(); //Closes result set rs
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
            
        if (error == 2) JOptionPane.showMessageDialog(null, "Account does not exist", "Error", JOptionPane.ERROR_MESSAGE);
        
        return error;
    }
    
    //Method for handling the log out process
    int logout(boolean exit)
    {
        int dialogResult = -1; //Variable used to determine success
        String option = exit ? "exit ?" : "log out ?"; //Used for a more specific JOptionPane message
        
        /**
         * Shows the JOptionPane with the final message to the user
         * which asks them to confirm their action. If it is confirmed,
         * the diaglogResult varible changes to 0 [which means success]
         * and will then record the session into the database for
         * that specific user and reset login information.
         * The database connection is then closed and the action,
         * whether to log out or exit, determines what the system will do
         */
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to " + option, "Confirm", JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION)
        {
            dialogResult = JOptionPane.YES_OPTION;
            
            if (!getStaffFullName().equals("Admin"))
            {
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "INSERT INTO sessions " +
                        "(SESSION_DATE, START_TIME, END_TIME, FK_STAFF_ID) " +
                        "VALUES (?, ?, ?, ?)"
                    );
                    ps.setString(1, sessionDate);
                    ps.setString(2, sessionStartTime);
                    ps.setString(3, sdfTime.format(now.getTime()));
                    ps.setInt(4, Integer.parseInt(currentUserID));

                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
            }
            
            setStaffLoginInformation(null, null, null, null);
            closeConnection();
            if (exit) System.exit(0);
            else new Login();
        }
        
        return dialogResult;
    }
    
    //Method to reset a staff member's password
    int resetPassword(String email, String password, String confirm)
    {
        finalErrMsg = initialErrMsg;
        int error = 0;
        
        //A 2D array containing {(String)Error: Sample, (boolean)The Check}
        Object errorChecking[][] =
        {
            {"Email: sample@email.com",                     forEmails.matcher(email).find()},
            {"Password must be at least 8 characters long", forPasswords.matcher(password).find()},
            {"Passwords do not match!",                     confirm.equals(password)}
        };
        
        /**
         * Looping through the errorChecking array
         * to find the checks that are false
         * in order to build the error message
         */
        for (int i = 0; i < errorChecking.length; i++)
        {
            if ((boolean) errorChecking[i][1] == false)
            {
                finalErrMsg += (String) errorChecking[i][0] + "\r\n";
                error = 1;
            }
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so then it will show the error message
         */
        if (error == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        else
        {
            /**
             * Check if the account exists in the database
             * and update the account if it does or
             * change the error to 1 if not
             * and show an error message
             */
            
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT STAFF_ID " +
                    "FROM staff " +
                    "WHERE EMAIL = ?"
                );
                ps.setString(1, email);
                
                rs = ps.executeQuery();
                
                if (rs.next())
                {
                    ps = dbConnection.prepareStatement
                    (
                        "UPDATE staff " +
                        "SET PASSWORD = ? " +
                        "WHERE STAFF_ID = ?"
                    );
                    ps.setString(1, password);
                    ps.setInt(2, (int) rs.getObject(1));
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                }
                else error = 2;
                
                rs.close(); //Closes result set rs
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
        
        if (error == 2) JOptionPane.showMessageDialog(null, "Account does not exist", "Error", JOptionPane.ERROR_MESSAGE);
        
        return error;
    }
    
    /**
     * Method used to determine if a date is valid
     * which is called in the relevant error handling method
     */
    private boolean isValidDate(String DOB)
    {
        /**
         * First checks if the DOB matches the pattern.
         * Only if it does, the method moves forward
         * to verify if the DOB is valid by parsing
         * it with sdfDate [a variable initialized earlier].
         * If the parsing fails and the error is caught,
         * the DOB is invalid and will set isValidDate
         * to false
         */
        
        boolean isValidDate = forDates.matcher(DOB).find();
        
        if (isValidDate)
        {
            try
            {
                sdfDate.setLenient(false);
                sdfDate.parse(DOB);
            }
            catch (ParseException pe) { isValidDate = false; }
        }
        
        return isValidDate;
    }
    
    //Method for performing error handling for staff and customers
    private int staffAndCustomerErrorHandling(String name,     String surname,
                                              String DOB,      String physical,
                                              String number,   String email,
                                              String password, String confirm,
                                              int checks)
    {
        //A 2D array containing {(String)Error: Sample [Valid Range], (boolean)The Check}
        Object errorChecking[][] =
        {
            {"Name: Name [2-20]",                           forNames.matcher(name).find()},
            {"Surname: Surname [2-20]",                     forNames.matcher(surname).find()},
            {"Date of Birth: 01/01/2011 (dd/MM/yyyy)",      isValidDate(DOB)},
            {"Physical Address: 1D Umhlanga Blvd [5-50]",   forAddresses.matcher(physical).find()},
            {"Contact Number: 0123456789 [10]",             forNumbers.matcher(number).find()},
            {"Email: example@email.com",                    forEmails.matcher(email).find()},
            {"Password must be at least 8 characters long", forPasswords.matcher(password).find()},
            {"Passwords do not match!",                     confirm.equals(password)}
        };
        
        //Initiating variables for error checking
        int errMsgChanged = 0;
        finalErrMsg = initialErrMsg;
        
        /**
         * Looping through the errorChecking array
         * to find the checks that are false
         * in order to build the error message
         */
        for (int i = 0; i < checks; i++)
        {
            if ((boolean) errorChecking[i][1] == false)
            {
                finalErrMsg += (String) errorChecking[i][0] + "\r\n";
                errMsgChanged = 1;
            }
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so then it will show the error message
         */
        if (errMsgChanged == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        
        return errMsgChanged;
    }
    
    //Method to insert or update staff
    int staff(String name,     String surname,
              String DOB,      String gender,
              String physical, String number,
              String email,    String password,
              String confirm,  int updateID,
              int type)
    {
        int error = 0;
        
        //Checks the type to determine the route to take with the operation
        if (type == STAFFINSERT && staffAndCustomerErrorHandling(name, surname, DOB, physical, number, email, password, confirm, 8) == 0)
        {
            //Ensures a neat format for these data ["Name" instead of "nAmE"]
            String sName = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase(),
                sSurname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
            
            //Inserts the new staff member into the database
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "INSERT INTO staff " +
                    "(NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL, PASSWORD, START_DATE) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
                );
                ps.setString(1, sName);
                ps.setString(2, sSurname);
                ps.setString(3, DOB);
                ps.setString(4, gender);
                ps.setString(5, physical);
                ps.setString(6, number);
                ps.setString(7, email);
                ps.setString(8, password);
                ps.setString(9, sdfDate.format(now.getTime()));
                
                ps.executeUpdate(); //Executes prepared statement ps
                dbConnection.commit(); //Updates database to reflect the new changes
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else if (type == STAFFUPDATE && staffAndCustomerErrorHandling(name, surname, DOB, physical, number, email, password, confirm, 6) == 0)
        {
            //Ensures a neat format for these data ["Name" instead of "nAmE"]
            String sName = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase(),
                sSurname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
            
            //Updates staff member in the database
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "UPDATE staff " +
                    "SET NAME = ?, " +
                    "SURNAME = ?, " +
                    "DOB = ?, " +
                    "GENDER = ?, " +
                    "PHYSICAL_ADDRESS = ?, " +
                    "CONTACT_NUMBER = ?, " +
                    "EMAIL = ? " +
                    "WHERE STAFF_ID = ?"
                );
                ps.setString(1, sName);
                ps.setString(2, sSurname);
                ps.setString(3, DOB);
                ps.setString(4, gender);
                ps.setString(5, physical);
                ps.setString(6, number);
                ps.setString(7, email);
                ps.setInt(8, updateID);
                
                ps.executeUpdate(); //Executes prepared statement ps
                dbConnection.commit(); //Updates database to reflect the new changes
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else error = 1;
        
        return error;
    }
    
    //Method to insert or update movie
    int movie(String title,       String synopsis,
              String runtime,     String rated,
              String producer,    String director,
              String releaseDate, String rentalPrice,
              String noOfCopies,  int updateID,
              int type)
    {
        int error = 0;
        
        //A 2D array containing {(String)Error: Sample [Valid Range], (boolean)The Check}
        Object movieErrorChecking[][] =
        {
            {"Title: A Good Movie [2-50]",             forMovieTitles.matcher(title).find()},
            {"Synopsis: This movie is great [10-250]", forSynopsis.matcher(synopsis).find()},
            {"Runtime: 120 [2-3]",                     forRuntimes.matcher(runtime).find()},
            {"Producer: Sample Producer [5-40]",       forMovieNames.matcher(producer).find()},
            {"Director: Sample Director [5-40]",       forMovieNames.matcher(director).find()},
            {"Release Date: 2018 [4]",                 forReleases.matcher(releaseDate).find()},
            {"Rental Price: 29.99 [0.00 - 99.99]",     forPrices.matcher(rentalPrice).find()},
            {"No of Copies: 15 [1-2]",                 forNoOfCopies.matcher(noOfCopies).find()}
        };
        
        //Initiating variables for error checking
        int errMsgChanged = 0;
        finalErrMsg = initialErrMsg;
        
        /**
         * Looping through the movieErrorChecking array
         * to find the checks that are false
         * in order to build the error message
         */
        for (int i = 0; i < movieErrorChecking.length; i++)
        {
            if ((boolean) movieErrorChecking[i][1] == false)
            {
                finalErrMsg += (String) movieErrorChecking[i][0] + "\r\n";
                errMsgChanged = 1;
            }
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so then it will show the error message
         */
        if (errMsgChanged == 1)
        {
            JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }
        else
        {
            //Converting data to its required data types
            int mRuntime = Integer.parseInt(runtime),
            mReleaseDate = Integer.parseInt(releaseDate),
             mNoOfCopies = Integer.parseInt(noOfCopies);
            
            double mPrice = Double.parseDouble(rentalPrice);
            
            //Checks the type to determine the route to take with the operation
            if (type == MOVIEINSERT)
            {
                //Inserts the new movie into the database
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "INSERT INTO movies " +
                        "(TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE, RENTAL_PRICE, NO_OF_COPIES) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
                    );
                    ps.setString(1, title);
                    ps.setString(2, synopsis);
                    ps.setInt(3, mRuntime);
                    ps.setString(4, rated);
                    ps.setString(5, producer);
                    ps.setString(6, director);
                    ps.setInt(7, mReleaseDate);
                    ps.setDouble(8, mPrice);
                    ps.setInt(9, mNoOfCopies);
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    error = 1;
                }
            }
            else if (type == MOVIEUPDATE)
            {
                //Updates the movie in the database
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "UPDATE movies " +
                        "SET TITLE = ?, " +
                        "SYNOPSIS = ?, " +
                        "RUNTIME = ?, " +
                        "RATED = ?, " +
                        "PRODUCER = ?, " +
                        "DIRECTOR = ?, " +
                        "RELEASE_DATE = ?, " +
                        "RENTAL_PRICE = ?, " +
                        "NO_OF_COPIES = ? " +
                        "WHERE MOVIE_ID = ?"
                    );
                    ps.setString(1, title);
                    ps.setString(2, synopsis);
                    ps.setInt(3, mRuntime);
                    ps.setString(4, rated);
                    ps.setString(5, producer);
                    ps.setString(6, director);
                    ps.setInt(7, mReleaseDate);
                    ps.setDouble(8, mPrice);
                    ps.setInt(9, mNoOfCopies);
                    ps.setInt(10, updateID);
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    error = 1;
                }
            }
        }
        
        return error;
    }
    
    //Method to insert or update customer
    int customer(String name,     String surname,
                 String DOB,      String gender,
                 String physical, String number,
                 String email,    int updateID,
                 int type)
    {
        int error = 0;
        
        if (staffAndCustomerErrorHandling(name, surname, DOB, physical, number, email, "", "", 6) == 0)
        {
            //Ensures a neat format for these data ["Name" instead of "nAmE"]
            String cName = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase(),
                cSurname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
            
            //Checks the type to determine the route to take with the operation
            if (type == CUSTOMERINSERT)
            {
                /**
                 * Inserts the new customer into the database
                 * and sends a welcome email
                 */
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "INSERT INTO customers " +
                        "(NAME, SURNAME, DOB, GENDER, PHYSICAL_ADDRESS, CONTACT_NUMBER, EMAIL, ACCOUNT_CREATION_DATE) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
                    );
                    ps.setString(1, cName);
                    ps.setString(2, cSurname);
                    ps.setString(3, DOB);
                    ps.setString(4, gender);
                    ps.setString(5, physical);
                    ps.setString(6, number);
                    ps.setString(7, email);
                    ps.setString(8, sdfDate.format(now.getTime()));
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                    
                    m.emailNewCustomerWelcome(email);
                }
                catch (SQLException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    error = 1;
                }
            }
            else if (type == CUSTOMERUPDATE)
            {
                //Updates the customer in the database
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "UPDATE customers " +
                        "SET NAME = ?, " +
                        "SURNAME = ?, " +
                        "DOB = ?, " +
                        "GENDER = ?, " +
                        "PHYSICAL_ADDRESS = ?, " +
                        "CONTACT_NUMBER = ?, " +
                        "EMAIL = ? " +
                        "WHERE CUSTOMER_ID = ?"
                    );
                    ps.setString(1, cName);
                    ps.setString(2, cSurname);
                    ps.setString(3, DOB);
                    ps.setString(4, gender);
                    ps.setString(5, physical);
                    ps.setString(6, number);
                    ps.setString(7, email);
                    ps.setInt(8, updateID);
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    error = 1;
                }
            }
        }
        else error = 1;
        
        return error;
    }
    
    /**
     * Method to delete staff, movie or customer from the database.
     * This method will only work if the record to be deleted
     * is not linked to any other record in the database.
     * e.g. Staff in Transaction
     *      Movies in Rentals Per Transaction
     *      Customers in Transaction
     *
     * If they are, then an error message will popup
     * informing the user of this and the record
     * will not be deleted
     */
    int delete(int deleteID, int type)
    {
        int error = 0;
        
        /**
         * Determines which table and ID to use
         * in the prepared statement to delete
         * a record
         */
        String table = type == STAFFDELETE ? "staff" :
                       type == MOVIEDELETE ? "movies" :
                       type == CUSTOMERDELETE ? "customers" :
                       null,
              column = type == STAFFDELETE ? "STAFF_ID" :
                       type == MOVIEDELETE ? "MOVIE_ID" :
                       type == CUSTOMERDELETE ? "CUSTOMER_ID" :
                       null;
        
        //Tries to delete the record
        try
        {
            ps = dbConnection.prepareStatement
            (
                "DELETE FROM " + table +
                " WHERE " + column + " = ?"
            );
            ps.setInt(1, deleteID);
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }
        
        return error;
    }
    
    //Method for processing transactions
    int processTransaction(ArrayList movies, double totalPrice,
                           String date,      String time,
                           int customerID,   String customerEmail,
                           int staffID)
    {
        int noOfRentals = movies.size(),
          transactionID = 0,
                  error = 0;
        
        /**
         * Creating a new calendar based on now [a variable initialized earlier]
         * to calculate the due date for the movies in the transaction
         */
        Calendar c = now;
        
        try { c.setTime(sdfDate.parse(date)); }
        catch (ParseException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        c.add(Calendar.DATE, 15);
        
        /**
         * Sets the due date from the calculated date [c]
         * and begins to build the receipt
         */
        String dueDate = sdfDate.format(c.getTime()),
               receipt = "";
        
        receipt += "------------------------------------------------------------";
        receipt += "------------------------------------------------------------";
        receipt += "\r\n";
        receipt += "Date: " + date + "\r\n";
        receipt += "Time: " + time + "\r\n\r\n";
        
        /**
         * First, the transaction is inserted into the database,
         * then the customer table is updated to add
         * the number of rentals based on noOfRentals
         * [a variable created earlier in this method]
         * determined by the size of an ArrayList passed
         * to this method. Afterwards, the levelID
         * and the number of rentals are extracted
         * from the database to update the level
         * and update it in the database.
         * Soon after, the transactionID is then
         * extracted to continue to build the receipt
         * and be used to insert records into the
         * rentals_per_transaction table. For each
         * movie in the ArrayList movies, a new record
         * is created in the rentals_per_transaction table
         * and the movies table is updated to reduce
         * the number of copies for that specific movie
         * by one. The movie title and rental price
         * is then extracted to continue to build
         * the receipt
         */
        try
        {
            ps = dbConnection.prepareStatement
            (
                "INSERT INTO transactions " +
                "(DATE, TIME, TOTAL_RENTALS, TOTAL_PRICE, FK_STAFF_ID, FK_CUSTOMER_ID) " +
                "VALUES (?, ?, ?, ?, ?, ?)"
            );
            ps.setString(1, date);
            ps.setString(2, time);
            ps.setInt(3, noOfRentals);
            ps.setDouble(4, totalPrice);
            ps.setInt(5, staffID);
            ps.setInt(6, customerID);
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            ps = dbConnection.prepareStatement
            (
                "UPDATE customers " +
                "SET NO_OF_RENTALS = NO_OF_RENTALS + ? " +
                "WHERE CUSTOMER_ID = ?"
            );
            ps.setInt(1, noOfRentals);
            ps.setInt(2, customerID);
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            int noRentals = 0,
                  levelID = 0;
            
            ps = dbConnection.prepareStatement
            (
                "SELECT NO_OF_RENTALS, FK_LEVEL_ID " +
                "FROM customers " +
                "WHERE CUSTOMER_ID = ?"
            );
            ps.setInt(1, customerID);
            
            rs = ps.executeQuery(); //Create result set
            
            if (rs.next())
            {
                noRentals = rs.getInt(1);
                levelID = rs.getInt(2);
            }
            
            rs.close(); //Closes result set rs
            ps.close(); //Closes prepared statement ps
            
            levelID = noRentals >= 10 && levelID == 1 ? 2 :
                      noRentals >= 25 && levelID == 2 ? 3 :
                      noRentals >= 50 && levelID == 3 ? 4 :
                      levelID;
            
            ps = dbConnection.prepareStatement
            (
                "UPDATE customers " +
                "SET FK_LEVEL_ID = ? " +
                "WHERE CUSTOMER_ID = ?"
            );
            ps.setInt(1, levelID);
            ps.setInt(2, customerID);
            
            ps.executeUpdate(); //Executes prepared statement ps
            dbConnection.commit(); //Updates database to reflect the new changes
            ps.close(); //Closes prepared statement ps
            
            ps = dbConnection.prepareStatement
            (
                "SELECT TRANSACTION_ID " +
                "FROM transactions " +
                "WHERE DATE = ? AND TIME = ? " +
                "AND FK_CUSTOMER_ID = ?"
            );
            ps.setString(1, date);
            ps.setString(2, time);
            ps.setInt(3, customerID);
            
            rs = ps.executeQuery(); //Creates result set
            
            if (rs.next()) transactionID = (int) rs.getObject(1);
            
            rs.close(); //Closes result set rs
            ps.close(); //Closes prepared statement ps
            
            receipt += "Transaction ID: " + transactionID + " [USE THIS WHEN YOU RETURN THESE MOVIES]\r\n";
            receipt += "Processed By: " + getStaffFullName() + "\r\n\r\n";
            receipt += "Movie: Price\r\n\r\n";
            
            for (Object obj : movies)
            {
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "INSERT INTO rentals_per_transaction " +
                        "(DUE_DATE, FK_MOVIE_ID, FK_TRANSACTION_ID) " +
                        "VALUES (?, ?, ?)"
                    );
                    ps.setString(1, dueDate);
                    ps.setInt(2, (int) obj);
                    ps.setInt(3, transactionID);
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                    
                    ps = dbConnection.prepareStatement
                    (
                        "UPDATE movies " +
                        "SET NO_OF_COPIES = NO_OF_COPIES - 1 " +
                        "WHERE MOVIE_ID = ?"
                    );
                    ps.setInt(1, (int) obj);
                    
                    ps.executeUpdate(); //Executes prepared statement ps
                    dbConnection.commit(); //Updates database to reflect the new changes
                    ps.close(); //Closes prepared statement ps
                    
                    ps = dbConnection.prepareStatement
                    (
                        "SELECT TITLE, RENTAL_PRICE " +
                        "FROM movies " +
                        "WHERE MOVIE_ID = ?"
                    );
                    ps.setInt(1, (int) obj);
                    
                    rs = ps.executeQuery(); //Creates result set
                    
                    if (rs.next()) receipt += rs.getObject(1) + ": R" + rs.getObject(2) + "\r\n";
                    
                    rs.close(); //Closes result set rs
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
            }
            
            /**
             * The building of the receipt is concluded and is then passed
             * to the createReceipt method in the call to emailReceipt.
             * The createReceipt method just pads extra information
             * to be sent to the customer
             */
            receipt += "\r\n";
            receipt += "Total Number of Rentals: " + noOfRentals + "\r\n";
            receipt += "Total Price: R" + totalPrice + "\r\n";
            receipt += "Return By: " + dueDate + "\r\n";
            receipt += "------------------------------------------------------------";
            receipt += "------------------------------------------------------------";
            
            //Send the receipt to the customer
            m.emailReceipt(customerEmail, createReceipt(receipt, movies));
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }
        
        return error;
    }
    
    /**
     * Pads the receipt which a common protocol companies should follow
     * and adds more information about the movies rented
     */
    private String createReceipt(String receiptOnly, ArrayList movies)
    {
        String receipt  = "";
               receipt += "Thank you for renting movies from us !\r\n\r\n";
               receipt += "Here is your receipt\r\n\r\n" + receiptOnly + "\r\n\r\n";
               receipt += "Here is some information about the movies you just rented\r\n\r\n";
               
        for (Object obj : movies)
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT TITLE, SYNOPSIS, RUNTIME, RATED, PRODUCER, DIRECTOR, RELEASE_DATE " +
                    "FROM movies " + 
                    "WHERE MOVIE_ID = ?"
                );
                ps.setInt(1,(int) obj);
                
                rs = ps.executeQuery(); //Creates result set
                
                if(rs.next())
                {
                    receipt += rs.getObject(1) + "\r\n";
                    receipt += "Synopsis: " + rs.getObject(2) + "\r\n";
                    receipt += "Runtime: " + rs.getObject(3) + "\r\n";
                    receipt += "Rated: " + rs.getObject(4) + "\r\n";
                    receipt += "Producer: " + rs.getObject(5) + "\r\n";
                    receipt += "Director: " + rs.getObject(6) + "\r\n";
                    receipt += "Release Date: " + rs.getObject(7) + "\r\n\r\n";
                }
                
                rs.close(); //Closes result set rs
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
               
        return receipt;
    }
    
    /**
     * Method used for processing returns from customers
     * and on completion, sends a little thank you email
     */
    int processReturn(int transactionID, ArrayList returns,
                      ArrayList movies,  ArrayList movieTitles)
    {
        String recipient = "",
                 message = "";
        
        int error = 0;
        
        
        /**
         * First, the email address is extracted
         * which will be the recipient. Then,
         * the message to the customer is build
         * with a little thank you and the list
         * of movies the customer is returning.
         * During the process, the movies table
         * is updated to add one to the number
         * of copies for that specific movie
         * and updates the return date for that
         * movie in the rentals_per_transaction
         * table using the current date
         */
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT c.EMAIL, t.TRANSACTION_ID " + 
                "FROM customers c, transactions t " +
                "WHERE t.TRANSACTION_ID = ? AND c.CUSTOMER_ID = t.FK_CUSTOMER_ID"
            );
            ps.setInt(1, transactionID);
            
            rs = ps.executeQuery(); //Creates result set
            
            if (rs.next()) recipient = (String) rs.getObject(1);
            
            rs.close(); //Closes result set rs
            ps.close(); //Closes prepared statement ps
            
            message += "Thank you for returning the following movies today:\r\n\r\n";
            
            for (int i = 0; i < returns.size(); i++)
            {
                ps = dbConnection.prepareStatement
                (
                    "UPDATE rentals_per_transaction " +
                    "SET RETURN_DATE = ? " +
                    "WHERE RENTAL_ID = ? AND FK_TRANSACTION_ID = ?"
                );
                ps.setString(1, sdfDate.format(now.getTime()));
                ps.setInt(2, (int) returns.get(i));
                ps.setInt(3, transactionID);
                
                ps.executeUpdate(); //Executes prepared statement ps
                dbConnection.commit(); //Updates database to reflect the new changes
                ps.close(); //Closes prepared statement ps
                
                ps = dbConnection.prepareStatement
                (
                    "UPDATE movies " +
                    "SET NO_OF_COPIES = NO_OF_COPIES + 1 " +
                    "WHERE MOVIE_ID = ?"
                );
                ps.setInt(1, (int) movies.get(i));
                
                ps.executeUpdate(); //Executes prepared statement ps
                dbConnection.commit(); //Updates database to reflect the new changes
                ps.close(); //Closes prepared statement ps
                
                message += movieTitles.get(i) + "\r\n";
            }
            
            m.emailThankYouForReturning(recipient, message);
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }
        
        return error;
    }
    
    /**
     * This method is used to get all the movies
     * that are due on the current day that
     * have not been returned and sends a
     * reminder to the customer via their
     * email to return those movies
     * with the relevant transaction ID
     */
    void sendReminders()
    {
        File f = new File("Last Reminders Sent.txt");
        
        String lastDate = ""; //To track the last date written to the file
        
        /**
         * If the file does not exist,
         * create the file with text "new"
         * on the first and only line.
         * If the file does exist, read
         * in the line which is the last
         * date the reminders were sent
         */
        if (!f.exists())
        {
            try
            {
                PrintWriter pw = new PrintWriter(f);
                pw.print("new");
                pw.close();
            }
            catch (IOException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
        else
        {
            try
            {
                Scanner sc = new Scanner(f);
                while (sc.hasNext()) { lastDate = sc.nextLine(); }
                sc.close();
            }
            catch (FileNotFoundException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
        
        /**
         * A check is done to see if the date
         * in the file is not equal to the
         * current day. Only then, the file
         * is updated and the reminders are
         * sent out for the day
         */
        if (!lastDate.equals(sdfDate.format(now.getTime())))
        {
            try
            {
                PrintWriter pw = new PrintWriter(f);
                pw.print(sdfDate.format(now.getTime()));
                pw.close();
            }
            catch (IOException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

            /**
             * First, distinct transaction IDs are extracted
             * from the database where there is no return
             * date and the due date is the current day.
             * Those transaction IDs form an ArrayList
             * that will be used to get the recipient
             * of the email and gets the movies where
             * the due date is the current day and 
             * there is no return date for every
             * transaction ID in the ArrayList.
             * The remainder is then built and
             * sent to the customer per element
             * of the ArrayList
             */
            try
            {
                ArrayList transactionIDs = new ArrayList();

                ps = dbConnection.prepareStatement
                (
                    "SELECT DISTINCT FK_TRANSACTION_ID " +
                    "FROM rentals_per_transaction " +
                    "WHERE RETURN_DATE IS NULL AND DUE_DATE = ?"
                );
                ps.setString(1, sdfDate.format(now.getTime()));

                rs = ps.executeQuery(); //Creates result set

                while (rs.next()) { transactionIDs.add(rs.getObject(1)); }

                rs.close(); //Closes result set rs
                ps.close(); //Closes prepared statement ps
                
                for (Object transaction : transactionIDs)
                {
                    String recipient = "";
                    
                    ps = dbConnection.prepareStatement
                    (
                        "SELECT c.EMAIL, t.TRANSACTION_ID " +
                        "FROM customers c, transactions t " +
                        "WHERE TRANSACTION_ID = ? AND c.CUSTOMER_ID = t.FK_CUSTOMER_ID"
                    );
                    ps.setInt(1, (int) transaction);
                    
                    rs = ps.executeQuery(); //Creates result set
                    
                    if (rs.next()) recipient = (String) rs.getObject(1);
                    
                    rs.close(); //Closes result set rs
                    ps.close(); //Closes prepared statement ps
                    
                    //Used to hold all the movies that have to be returned
                    ArrayList movies = new ArrayList();
                    
                    ps = dbConnection.prepareStatement
                    (
                        "SELECT m.TITLE, r.FK_TRANSACTION_ID " +
                        "FROM movies m, rentals_per_transaction r " +
                        "WHERE r.RETURN_DATE IS NULL AND r.DUE_DATE = ? " +
                        "AND r.FK_TRANSACTION_ID = ? AND m.MOVIE_ID = r.FK_MOVIE_ID"
                    );
                    ps.setString(1, sdfDate.format(now.getTime()));
                    ps.setInt(2, (int) transaction);
                    
                    rs = ps.executeQuery(); //Creates result set
                    
                    while (rs.next()) { movies.add(rs.getObject(1)); }
                    
                    rs.close(); //Closes result set rs
                    ps.close(); //Closes prepared statement ps
                    
                    String reminder = "Dear Customer\r\n\r\nYou have the following movies from Transaction [" + transaction + "] due for return today:\r\n\r\n";
                    
                    for (Object movie : movies) { reminder += movie + "\r\n"; }
                    
                    //Sends the reminder to the customer
                    m.emailReminder(recipient, reminder);
                }
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }
    
}
